/**
 * Created by Daniel on 16/12/15.
 */
function makeid(length, charArray) {
	charArray = charArray || "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	var text = "";
	for( var i=0; i < length; i++ )
		text += charArray.charAt(Math.floor(Math.random() * charArray.length));

	return text;
}