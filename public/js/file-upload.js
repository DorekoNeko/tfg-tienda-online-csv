/**
 * Created by Daniel on 04/11/2015.
 */
$(function () {
	var modal_drop, save_button;

	// Appending the modal
	$.get("/ajax/templates/panel/file_upload", function(data) {
		$("body").append(data);

		modal_drop = $("#modal-drop");
		save_button = $("#file-upload-modal #upload-save-changes");
	});

	// On modal open
	$(document).on("show.bs.modal", "#file-upload-modal", function(modal) {
		// Cleaning the preview first
		var jModal = $(modal.currentTarget);
		jModal.find(".info-wrapper").show();
		jModal.find(".preview").hide();

		if (modal.relatedTarget != null && modal.relatedTarget != undefined) {
			var targetFile = $($(modal.relatedTarget).attr("data-file"));
			var previewBox = $($(modal.relatedTarget).attr("data-preview"));
			if (targetFile != undefined) {
				modal_drop.bind("click.upload-modal", {
					targetFile: targetFile,
					previewWrapper: previewBox
				}, handleFile);
			}
		}
	});

	// On modal close clear the events
	$(document).on("hide.bs.modal", "#file-upload-modal", function(modal) {
		modal_drop.unbind("click.upload-modal");
		save_button.unbind("click.upload-save");
	});

	/**
	 * Functions
	 */

	/**
	 * Handles the file selection.
	 * @param e
	 */
	function handleFile(e) {
		var targetFile = e.data.targetFile;
		var previewBox = e.data.previewWrapper;

		if (targetFile != null || targetFile != 'undefined') {
			var file_name = targetFile.attr("name");
			targetFile.attr("data-upload-name", file_name);
			targetFile.removeAttr("name");
			targetFile.click();

			targetFile.on("change", function() {
				var input = this;
				modal_drop.children(".info-wrapper").fadeOut(400);

				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onloadend = function(e, file) {
						var bin = this.result;
						// Setting the preview in the modal
						modal_drop.children(".preview").attr("src", bin).fadeIn(400);

						// On save button clicked
						save_button.unbind("click.upload-save");
						save_button.bind("click.upload-save", {
							file: targetFile,
							link: bin,
							previewWrapper: previewBox
						}, uploadSave);
					};

					// Read only the first file.
					reader.readAsDataURL(input.files[0]);
				}
			});
		}
	}

	/**
	 * Handle the "save" process and change the preview box if any.
	 * @param e
	 */
	function uploadSave(e) {
		var file_name = e.data.file.attr("data-upload-name");
		e.data.file.attr("name", file_name);

		if (e.data.previewWrapper != 'undefined') {
			if ($(e.data.previewWrapper).is("img")) {
				e.data.previewWrapper.attr("src", e.data.link);
			} else {
				e.data.previewWrapper.css({
					"background-image": "url('" + e.data.link + "')"
				});
			}
		}

		$('#file-upload-modal').modal('toggle');
	}
});