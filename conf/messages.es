# Panel
panel.titles.admin_chat=Panel | Chat en vivo de administrador

panel.messages.id_doesnt_exists=La ID no existe.

panel.upload_image=Subir Imagen

panel.list.id=Id.
panel.list.sort=Ordenar
panel.list.name=Nombre
panel.list.actions=Acciones
panel.list.actions.edit=Editar
panel.list.actions.delete=Eliminar

vats.errors.vat_does_not_exists=El tipo de IVA no existe.
vats.errors.vat_already_exists=Ya existe un tipo de IVA con este nombre.
vats.messages.add_success=El tipo de IVA fue creado correctamente.
vats.messages.delete_success=El tipo de IVA fue eliminado correctamente.

panel.sections.toolbar.sort=Ordenar
panel.sections.alerts.sort_info=Arrastra y suelta los objetos para moverlos.
panel.sections.messages.add_success=La sección ha sido creada correctamente.
panel.sections.messages.sort_saved=El orden ha sido guardado.
panel.sections.messages.edit_saved=Los cambios han sido guardados.
panel.sections.messages.delete_success=La sección se ha eliminado correctamente.
panel.sections.messages.delete_not_exists=La ID de sección no existe.
panel.list.sections.in_header=Cabecera

# General
home=Inicio
sign_in=Acceder
sign_up=Registrarse
sign_out=Salir
list=Lista
add=Añadir
save=Guardar
update=Actualizar
change=Cambiar
create=Crear
cancel=Cancelar
delete=Eliminar
cart=Carrito
add_to_cart=Añadir a la cesta
sections=Secciones
categories=Categorías

lists.actions=Acciones
lists.actions.view=Ver
lists.actions.edit=Editar
lists.actions.delete=Eliminar

# Errors
categories.category_does_not_exists=La categoría no existe.
categories.category_no_products=Esta categoría aún no ha sido asignada a ningún producto.

# Auth
auth.unauthorized_section=No tienes acceso a este lugar. Porfavor considera <a href="/sign-in">Acceder</a> o <a href="/sign-up">Registrarte</a>.

# Modals
modal.delete_confirmation_title=Confirmación de eliminación
modal.delete_confirmation=Cuidado! Estás a punto de eliminar algo...

modal.shop_generator_title=¿Quieres una tienda como esta?
modal.shop_generator_message=Si quieres una tienda como esta totalmente gratis rellena el formulario <a href="http://apps.siberos.net:4000/">aquí</a> y tendrás la tuya en un par de minutos!

# Categories
category.submenus=Submenúes

# Products
products.recently_added=Nuevos Productos
products.in_offer=En Oferta
products.product_ref_does_not_exists=No existe ningún producto con esta referencia.
products.details.tax_included=impuestos incluídos.
products.details.price_without_taxes=sin impuestos
products.stock.message_0_percent=Agotado!
products.stock.message_20_percent=Quedan unos pocos!
products.stock.message_60_percent=Quedan bastantes!
products.stock.message_100_percent=Lleno!
products.details.quantity=Cantidad
products.details.about_this_item=Sobre este producto
products.details.customer_reviews=Opiniones de los clientes
products.details.customer_reviews.stars=Estrellas
products.details.customer_reviews.message=Mensaje
products.details.customer_reviews.add_review=Añadir Opinión
products.details.customer_reviews.message_too_short=La opinión es muy corta.
products.details.customer_reviews.sign_in_to_comment=<a href="/sign-in">Accede</a> o <a href="/sign-up">Regístrate</a> para enviar una opinión.
products.details.customer_reviews.there_are_no_reviews=No hay opiniones. Se el primero!
products.details.customer_reviews.review_length_min=mín.

# Login
login.username=Email o Usuario
login.password=Contraseña
login.bad_credentials=Credenciales incorrectos
login.forgot_password=Olvidé la contraseña
login.create_account=Crear cuenta

# Sign up
sign_up.name=Nombre
sign_up.last_name=Apellidos
sign_up.dni=DNI
sign_up.phone_number=Número de Teléfono
sign_up.username=Usuario
sign_up.mail=Email
sign_up.password=Contraseña
sign_up.repassword=Repetir Contraseña
sign_up.errors.username_taken=El usuario ya está en uso

register.password.short=La contraseña debe contener 8 caracteres o más
register.password.donotmatch=Las contraseñas no coinciden
register.password.security=La contraseña debe contener letras y números

# Account
account.settings=Configuración de la cuenta
account.address_management=Administración de direcciones
account.conversations=Conversaciones
account.orders=Pedidos

account.name=Name
account.last_name=Apellidos
account.dni=DNI
account.phone_number=Número de Teléfono
account.username=Usuario
account.mail=Email
account.password=Contraseña

account.settings.change_password=Cambiar Contraseña
account.settings.old_password=Antigüa Contraseña
account.settings.new_password=Nueva Contraseña
account.settings.new_password_repeat=Repetir Nueva Contraseña
account.settings.picture_change=Cambiar
account.settings.picture_remove=Eliminar
account.settings.save_success=La configuración ha sido guardada!
account.settings.password_change_success=La contraseña ha sido actualizada!
account.settings.password_change_bad_auth=La antigüa contraseña no es correcta.

account.address_management.list_addresses=Listar Direcciones
account.address_management.add_address=Añadir Dirección
account.address_management.empty_list=Aún no has añadido una dirección.
account.address_management.address_name=Alias
account.address_management.address=Dirección
account.address_management.population=Población
account.address_management.province=Provincia
account.address_management.postal_code=Código Postal
account.address_management.add_success=La dirección se ha creado correctamente!
account.address_management.edit_success=La dirección se ha guardado correctamente!
account.address_management.delete_success=La dirección se ha eliminado correctamente!
account.address_management.address_already_exists=Ya existe una dirección con el alias {0}.
account.address_management.address_does_not_exists=La dirección requerida no existe.
account.address_management.edit_address=Editar Dirección

account.conversations.list=Listar Conversaciones
account.conversations.new=Nueva Conversación
account.conversations.title=Título
account.conversations.created_on=Creada El
account.conversations.last_activity_on=Última Actividad El
account.conversations.empty_list=No hay conversaciones.
account.conversations.message=Mensaje
account.conversations.creation_success=La conversación se ha creado correctamente.

# Chat
chat.live_chat=Chat En Vivo
chat.connect=Conectar
chat.disconnect=Desconectar
chat.admin.chats=Chats
chat.send_box.placeholder=Escribe aquí tu mensaje...
chat.send=Enviar
chat.admin_offline=Actualmente no podemos ayudarte. Inténtalo de nuevo más tarde.