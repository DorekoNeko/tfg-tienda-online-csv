@import dao.{ComplexCategory, ComplexProduct}
@import settings.GeneralSettings
@(product: ComplexProduct, category_map: Seq[ComplexCategory])(implicit messages: Messages, flash: Flash, settings: GeneralSettings)

    <!-- Breadcrumbs -->
<div class="row">
    <div class="col-sm-12">
        <ol class="breadcrumb">
            @for(category <- category_map.take(category_map.length-1)) {
                <li><a href='#/category/show/@category.normalized_url'>@category.name</a></li>
            }
            <li class="active">@category_map.last.name</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <h1 class="page-header">@product.name</h1>
    </div>
</div>

<div class="row product-details">
    <div class="col-sm-12">
        <div class="col-sm-7">
            @if(product.gallery.nonEmpty) {
                @defining(product.gallery.filterNot(_.is_slider)) { slides =>
                    <div id="gallery-carousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                        <ol class="carousel-indicators">
                            @if(slides.exists(_.is_default)) {
                                @for(slide <- slides.sortBy(!_.is_default)) {
                                    @if(slides.sortBy(!_.is_default).indexOf(slide) == 0) {
                                        <li data-target="#gallery-carousel" data-slide-to="0" class="active"></li>
                                    } else {
                                        <li data-target="#gallery-carousel" data-slide-to="@slides.sortBy(!_.is_default).indexOf(slide)"></li>
                                    }
                                }
                            } else {
                                @for(slide <- slides) {
                                    @if(slides.indexOf(slide) == 0) {
                                        <li data-target="#gallery-carousel" data-slide-to="0" class="active"></li>
                                    } else {
                                        <li data-target="#gallery-carousel" data-slide-to="@slides.indexOf(slide)"></li>
                                    }
                                }
                            }
                        </ol>

                            <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox" product-gallery>
                            @if(slides.exists(_.is_default)) {
                                @for(slide <- slides.sortBy(!_.is_default)) {
                                    @if(slides.sortBy(!_.is_default).indexOf(slide) == 0) {
                                        <a href="@routes.Resources.get(slide.fileuuid, "products")" class="item active">
                                            <img src="@routes.Resources.resize(slide.fileuuid, 512, 512, "products")" alt="@slide.fileuuid" />
                                            <div class="magnifier-overlay">
                                                <span class="glyphicon glyphicon-resize-full magnifier-icon"></span>
                                            </div>
                                        </a>
                                    } else {
                                        <a href="@routes.Resources.get(slide.fileuuid, "products")" class="item">
                                            <img src="@routes.Resources.resize(slide.fileuuid, 512, 512, "products")" alt="@slide.fileuuid" />
                                            <div class="magnifier-overlay">
                                                <span class="glyphicon glyphicon-resize-full magnifier-icon"></span>
                                            </div>
                                        </a>
                                    }
                                }
                            } else {
                                @for(slide <- slides) {
                                    @if(slides.indexOf(slide) == 0) {
                                        <a href="@routes.Resources.get(slide.fileuuid, "products")" class="item active">
                                            <img src="@routes.Resources.resize(slide.fileuuid, 512, 512, "products")" alt="@slide.fileuuid" />
                                            <div class="magnifier-overlay">
                                                <span class="glyphicon glyphicon-resize-full magnifier-icon"></span>
                                            </div>
                                        </a>
                                    } else {
                                        <a href="@routes.Resources.get(slide.fileuuid, "products")" class="item">
                                            <img src="@routes.Resources.resize(slide.fileuuid, 512, 512, "products")" alt="@slide.fileuuid" />
                                            <div class="magnifier-overlay">
                                                <span class="glyphicon glyphicon-resize-full magnifier-icon"></span>
                                            </div>
                                        </a>
                                    }
                                }
                            }
                        </div>

                            <!-- Controls -->
                        <a class="left carousel-control" href="!" role="button">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="!" role="button">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                }
            }
        </div>
        <div class="col-sm-5">
            <div class="row">
                <div class="col-sm-12">
                    @defining((product.stock / 100.0 * 100.0).toInt) { stock_percent =>
                        @if(stock_percent == 0) {
                            <div class="progress">
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="@stock_percent" aria-valuemin="0" aria-valuemax="100" style="min-width: 40%; width: 40%;">
                                    <strong>@stock_percent %</strong> <small>- @messages("products.stock.message_0_percent")</small>
                                </div>
                            </div>
                        } else { @if(stock_percent <= 20) {
                            <div class="progress">
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="@stock_percent" aria-valuemin="0" aria-valuemax="100" style="min-width: 40%; width: 45%;">
                                    <strong>@stock_percent %</strong> <small>- @messages("products.stock.message_20_percent")</small>
                                </div>
                            </div>
                        } else { @if(stock_percent <= 99) {
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="@stock_percent" aria-valuemin="0" aria-valuemax="100" style="min-width: 40%; width: 60%;">
                                    <strong>@stock_percent %</strong> <small>- @messages("products.stock.message_60_percent")</small>
                                </div>
                            </div>
                        } else {
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="@stock_percent" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                    <strong>@stock_percent %</strong> <small>- @messages("products.stock.message_100_percent")</small>
                                </div>
                            </div>
                        }
                        }}
                    }
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="product-toolbar">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="prices">
                                            @if(product.in_offer) {
                                                <span class="original-price">{{@product.priceWithVAT | currency: ""}} &euro;</span>
                                                <span class="price">{{@product.calculatedPrice | currency: ""}} &euro;</span>
                                                <div class="price-no-vat">
                                                    @messages("products.details.tax_included") ({{@product.priceWithDiscountsNoVAT | currency: ""}} &euro; @messages("products.details.price_without_taxes"))
                                                </div>
                                            } else {
                                                <span class="price">{{@product.priceWithVAT | currency: ""}} &euro;</span>
                                                <div class="price-no-vat">
                                                    @messages("products.details.tax_included") ({{@product.price | currency: ""}} &euro; @messages("products.details.price_without_taxes"))
                                                </div>
                                            }
                                        </div>
                                    </div>
                                    <div class="col-sm-4 btn-wrapper">
                                        <button type="button" class="btn btn-success btn-block btn-cart" ng-click="store.addToCart('@product.reference')">
                                            <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                            @messages("add_to_cart")
                                        </button>
                                        <input type="number" class="form-control input-quantity" min="1" ng-model="store.add_to_cart_quantity" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                        <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-justified" role="tablist">
                        <li role="presentation" class="active">
                            <a href="!" data-target="#about-this-item" aria-controls="about-this-item" role="tab" data-toggle="tab">
                            @messages("products.details.about_this_item")
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="!" data-target="#customer-reviews" aria-controls="customer-reviews" role="tab" data-toggle="tab">
                            @messages("products.details.customer_reviews")
                            </a>
                        </li>
                    </ul>
                        <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="about-this-item">
                        @Html(product.description.toString)
                        </div>
                        <div role="tabpanel" class="tab-pane" id="customer-reviews">
                            <customer-reviews product-ref="@product.reference"></customer-reviews>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>