/**
 * Created by Daniel on 3/05/16.
 */
$(function() {

	var current_url = "";

	$(document).on("click", "a.do-confirmation", function(e) {
		e.preventDefault();
		e.stopPropagation();

		current_url = $(this).attr("href");
		$("#bt-confirm-modal").modal("show");

		return false;
	});

	$(document).on("click", "#bt-confirm-modal-proceed", function(e) {
		e.preventDefault();
		e.stopPropagation();

		window.location = current_url;

		return false;
	});

});