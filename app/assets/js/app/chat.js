/**
 * Created by Daniel on 22/05/16.
 */
(function() {
	var chat = angular.module("ClientChat", []);

	chat.factory("alerts", ["$rootScope", "$compile", "$timeout", function($rootScope, $compile, $timeout) {
		var instance = {};

		instance.element = undefined;

		instance.init = function(element) {
			element.append("<div class='alerts' style='position: fixed; bottom: 0; right: 30px; width: 480px;'></div>");
			instance.element = element.find(".alerts");
		};

		instance.addAlert = function(text, type, dismisable) {
			type = typeof type !== "undefined" ? type : "danger";
			dismisable = typeof dismisable !== "undefined" ? dismisable : true;

			var alert;
			if (dismisable) {
				alert = $("<div class='alert alert-dismissible alert-" + type + "' role='alert'>" +
					"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + text +
					"</div>");
			} else {
				alert = $("<div class='alert alert-" + type + "' role='alert'>" + text + "</div>");
			}

			alert.css({
				"position": "relative",
				"top": "999px"
			});

			instance.element.prepend(alert);

			alert.animate({
				"top": 0
			}, 350, function() {
				$timeout(function() {
					alert.fadeOut(350, function() {
						alert.remove();
					});
				}, 6000);
			});
		};

		return instance;
	}]);

	/**
	 * Handles the live chat button
	 */
	chat.directive("buttonChatController", function() {
		return {
			restrict: "A",
			controller: ["$scope", "$element", "alerts", function($scope, $element, alerts) {
				$scope.socket = null;
				$scope.connected = false;

				// Creating the tone
				$scope.tone = {
					audio: new Audio("/assets/sounds/success_tone.mp3"),
					play: function() {
						if (this.audio !== null) {
							this.audio.play();
						}
					}
				};

				// Initializing alerts service
				alerts.init($element);

				// Initializing the WebSocket
				if (window.WebSocket) {
					if ($scope.socket === null) {
						var loc = window.location, uri;
						if (loc.protocol === "https:") {
							uri = "wss:";
						} else {
							uri = "ws:";
						}

						$scope.socket = new WebSocket(uri + "//" + loc.hostname + ":" + loc.port + "/chat/socket");

						$scope.socket.onopen = function(e) {
							$scope.$apply(function() {
								$scope.connected = true;
							});

							console.log("Connected.");
							$scope.checkForAdmin();
						};

						$scope.socket.onmessage = function(e) {
							console.log("message");
							var msg = JSON.parse(e.data);

							switch(msg.type) {
								case "notify-admin-online":
									$scope.status_icon.toggleClass("on");
									$scope.tone.play();
									break;
								case "notify-admin-offline":
									$scope.status_icon.toggleClass("on");
									break;
								case "check-admin":
									if (msg.result) {
										$scope.status_icon.addClass("on");
										$scope.tone.play();
									} else {
										$scope.status_icon.removeClass("on");
									}
									break;
							}
						};

						$scope.socket.onclose = function(e) {
							$scope.$apply(function() {
								$scope.connected = false;
							});

							if (e.code == 1006) {
								alerts.addAlert("Error " + e.code + ": The connection to the WebSocket was closed abnormally.");
							}
						};

						$scope.checkForAdmin = function() {
							var request = {
								"type": "check-admin"
							};

							$scope.socket.send(JSON.stringify(request));
						};
					}
				} else {
					alerts.addAlert("WebSockets aren't supported by your browser. Please try using another browser.");
				}

				$scope.popupWindow = null;
				$scope.showPopup = function() {
					if ($scope.popupWindow === null) {
						var w = 580;
						var h = 540;
						var leftPos = (screen.width) ? (screen.width-w)/2 : 0;
						var topPos = (screen.height) ? (screen.height-h)/2 : 0;
						var settings = 'resizable=0,height=' + h + ',width=' + w + ',top=' + topPos + ', left=' + leftPos + ',scrollbars=yes';
						$scope.popupWindow = window.open("/chat", "User Live Chat", settings);
						$scope.popupWindow.onbeforeunload = function() {
							$scope.popupWindow = null;
						};
					}
				};
			}],
			link: function(scope, element) {
				scope.status_icon = element.find(".icon");
			}
		};
	});

	/**
	 * Main chat controller
	 */
	chat.controller("ChatController", ["$scope", "$element", "alerts", function($scope, $element, alerts) {
		$scope.socket = null;
		$scope.session_started = false;

		// Creating the tone
		$scope.tone = {
			audio: new Audio("/assets/sounds/success_tone.mp3"),
			play: function() {
				if (this.audio !== null) {
					this.audio.play();
				}
			}
		};

		// Initializing alerts service
		alerts.init($element);

		$scope.startChatSession = function() {
			var request = {
				"type": "chat-start"
			};

			$scope.socket.send(JSON.stringify(request));
		};

		$scope.chat = {};
		$scope.addChat = function(hash, username) {
			$scope.chat = {
				hash: hash,
				username: username,
				messages: [],
				message: "",
				sendMessage: $scope.fnChat.sendMessage,
				addMessage: $scope.fnChat.addMessage,
				addStatus: $scope.fnChat.addStatus
			};
		};
		$scope.fnChat = {
			sendMessage: function(message) {
				if ($scope.socket !== null && this.message.length > 0) {
					var request = {
						type: "send-message",
						message: this.message,
						hash: this.hash
					};

					$scope.socket.send(JSON.stringify(request));
					this.addMessage(this.message, this.username);
					this.message = "";
				}
			},
			addMessage: function(message, username) {
				var date = new Date();
				this.messages.push({
					is_status: false,
					body: message,
					timestamp: date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds(),
					username: username
				});

				var container = $("." + this.hash).find(".messages");
				container.animate({
					scrollTop: container[0].scrollHeight
				}, 300);
			},
			addStatus: function(message) {
				var date = new Date();
				this.messages.push({
					is_status: true,
					body: message,
					timestamp: date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds(),
					username: ""
				});

				var container = $("." + this.hash).find(".messages");
				container.animate({
					scrollTop: container[0].scrollHeight
				}, 300);
			}
		};

		// Starting the socket
		if (window.WebSocket) {
			if ($scope.socket === null) {
				var loc = window.location, uri;
				if (loc.protocol === "https:") {
					uri = "wss:";
				} else {
					uri = "ws:";
				}

				$scope.socket = new WebSocket(uri + "//" + loc.hostname + ":" + loc.port + "/chat/socket");

				$scope.socket.onopen = function(e) {
					$scope.startChatSession();
				};

				$scope.socket.onmessage = function(e) {
					var message = JSON.parse(e.data);

					switch (message.type) {
						case "chat-start":
							$scope.$apply(function() {
								$scope.addChat(message.result.conversation.hash, message.result.username);
							});
							$scope.$apply(function() {
								$scope.chat.addStatus("Connected.");
							});
							break;
						case "chat-end":
							$scope.$apply(function() {
								$scope.session_started = false;
								$scope.socket.close(1000);
							});
							break;
						case "message-received":
							$scope.$apply(function() {
								$scope.chat.addMessage(message.message, message.username);
								$scope.tone.play();
							});
							break;
						case "notify-admin-offline":
							$scope.$apply(function() {
								$scope.session_started = false;
								$scope.socket.close(1000);
							});
							break;
						default:
							console.log(message.result);
							break;
					}
				};

				$scope.socket.onclose = function(e) {
					if (e.code == 1000) {
						$scope.$apply(function() {
							$scope.chat.addStatus("The conversation has been ended.");
						});
					} else if (e.code == 1006) {
						alerts.addAlert("Error " + e.code + ": The connection was closed abnormally. You sure you are signed in?");
					}
				};
			}
		} else {
			alerts.addAlert("WebSockets aren't supported by your browser. Please try using another browser.");
		}
	}]);

	/**
	 * Handling the message input
	 */
	chat.directive("messageHandler", function() {
		return {
			restrict: "A",
			link: function(scope, element) {
				element.bind("keypress", function(event) {
					scope.$apply(function() {
						if (scope.chat !== undefined) {
							if (event.keyCode == 13) {
								scope.chat.sendMessage();
							} else {
								scope.chat.message += String.fromCharCode(event.keyCode);
							}
						}
					});

					return false;
				});
			}
		};
	});
})();