/**
 * Created by Daniel on 28/04/16.
 * AngularJS - Store module
 */
(function() {
	var store = angular.module("Store", ["ngRoute", "storeCtrl", "ngAnimate", "ClientChat"]);

	store.factory("alerts", ["$rootScope", "$timeout", function($rootScope, $timeout) {
		var instance = {};

		instance.element = undefined;

		instance.init = function(element) {
			element.append("<div class='alerts' style='position: fixed; bottom: 0; right: 30px; width: 480px;'></div>");
			instance.element = element.find(".alerts");
		};

		instance.addAlert = function(text, type, dismisable) {
			type = typeof type !== "undefined" ? type : "danger";
			dismisable = typeof dismisable !== "undefined" ? dismisable : true;

			var alert;
			if (dismisable) {
				alert = $("<div class='alert alert-dismissible alert-" + type + "' role='alert'>" +
					"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + text +
					"</div>");
			} else {
				alert = $("<div class='alert alert-" + type + "' role='alert'>" + text + "</div>");
			}

			alert.css({
				"position": "relative",
				"top": "999px"
			});

			instance.element.prepend(alert);

			alert.animate({
				"top": 0
			}, 350, function() {
				$timeout(function() {
					alert.fadeOut(350, function() {
						alert.remove();
					});
				}, 6000);
			});
		};

		return instance;
	}]);

	/**
	 * Routes
	 */
	store.config(function($routeProvider) {
		$routeProvider.
		when("/index", {
			templateUrl: "ajax/index",
			controller: "uncachedRoute"
		}).
		when("/section/:name", {
			templateUrl: function($routeParams) { return "ajax/section/" + $routeParams.name; },
			controller: "uncachedRoute"
		}).
		when("/category/show/:name/:id/:order", {
			templateUrl: function($routeParams) {
				return "ajax/category/show/" + $routeParams.name + "/" + $routeParams.id + "/" + $routeParams.order;
			},
			controller: "uncachedRoute"
		}).
		when("/category/show/:name", {
			templateUrl: function($routeParams) { return "ajax/category/show/" + $routeParams.name; },
			controller: "uncachedRoute"
		}).
		when("/product/:ref", {
			templateUrl: function($routeParams) { return "ajax/product/" + $routeParams.ref; },
			controller: "uncachedRoute"
		}).
		when("/account/settings", {
			templateUrl: "ajax/account/settings",
			controller: "uncachedRoute"
		}).
		when("/account/addresses", {
			templateUrl: "ajax/account/addresses",
			controller: "uncachedRoute"
		}).
		when("/account/addresses/add", {
			templateUrl: "ajax/account/addresses/add",
			controller: "uncachedRoute"
		}).
		when("/account/addresses/edit/:id", {
			templateUrl: function($routeParams) { return "ajax/account/addresses/edit/" + $routeParams.id; },
			controller: "uncachedRoute"
		}).
		when("/account/addresses/delete/:id", {
			templateUrl: function($routeParams) { return "ajax/account/addresses/delete/" + $routeParams.id; },
			controller: "uncachedRoute"
		}).
		when("/account/conversations", {
			templateUrl: "ajax/account/conversations",
			controller: "uncachedRoute"
		}).
		when("/account/conversations/new", {
			templateUrl: "ajax/account/conversations/new",
			controller: "uncachedRoute"
		}).
		when("/account/conversations/view/:hash", {
			templateUrl: function($routeParams) { return "ajax/account/conversations/view/" + $routeParams.hash; },
			controller: "uncachedRoute"
		}).
		when("/account/orders", {
			templateUrl: "ajax/account/orders",
			controller: "uncachedRoute"
		}).
		when("/account/orders/:hash/cancel", {
			templateUrl: function($routeParams) { return "ajax/account/orders/" + $routeParams.hash + "/cancel"; },
			controller: "uncachedRoute"
		}).
		when("/checkout", {
			templateUrl: "ajax/cart/checkout",
			controller: "uncachedRoute"
		}).
		when("/checkout/step/:step", {
			templateUrl: function($routeParams) { return "ajax/cart/checkout/step/" + $routeParams.step; },
			controller: "uncachedRoute"
		}).
		otherwise({
			redirectTo: "/index"
		});
	});

	store.controller("uncachedRoute", function($scope, $templateCache) {
		$templateCache.removeAll();
	});

	/**
	 * Store Controller
	 */
	store.controller("StoreController", ["$scope", "$http", "alerts", function($scope, $http, alerts) {
		$scope.store = {
			signed_in: false,
			add_to_cart_quantity: 1,
			addToCart: function(hash) {
				var quantity = this.add_to_cart_quantity > 0 ? this.add_to_cart_quantity : 1;

				if ($scope.store.signed_in) {
					$http.post("/cart/add/" + hash + "/" + quantity).then(
						function success(response) {
							$scope.cart.updateProducts();
							alerts.addAlert(quantity + " product(s) added to your cart.", "success");
						}, function error(response) {
							console.log("$http error =>");
							console.log(response);
						}
					);
				} else {
					alerts.addAlert("You need to sign in to buy products!");
				}
			}
		};

		alerts.init($("body"));
	}]);
})();