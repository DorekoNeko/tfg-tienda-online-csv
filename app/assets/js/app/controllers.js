/**
 * Created by Daniel on 14/05/16.
 * AngularJS - Store controller
 */
(function() {
	var storeCtrl = angular.module("storeCtrl", ["userCtrl", "headerCtrl", "productCtrl"]);

	/**
	 * Handles the async form submission for all the forms within the element with the attribute
	 */
	storeCtrl.directive("asyncSubmission", function($http, $compile) {
		return {
			restrict: "A",
			controller: function($scope) {
				$scope.bindForms = function(element) {
					var forms = element.find("form");

					forms.each(function(i, el) {
						$(el).bind("submit", function(e) {
							e.preventDefault();
							e.stopPropagation();

							var btnSubmit = $(el).find("[type='submit']");
							// Set submit button to its loading state
							btnSubmit.html('<i class="fa fa-cog fa-spin fa-lg"></i>');
							btnSubmit.prop("disabled", true);

							// Making the form data before send
							var formData = new FormData(el);

							$http({
								method: $(el).attr("method"),
								url: $(el).attr("action"),
								data: formData,
								transformRequest: angular.identity,
								headers : {
									'Content-Type': undefined
								}  // Correcting the boundary error
							}).then(function success(response) {
								// Fire the $userChanged event to notify the
								// header controllers to update info if needed.
								if ($(el).attr("submit-user-changed") !== undefined) {
									$scope.$emit("$userChanged");
								}

								// Update the view
								element.html(response.data);
								$compile(element.contents())($scope);
								console.log("Form view updated.");
								// Re-bind the possible forms submission
								$scope.bindForms(element);

								// Scroll top if alerts
								if(element.find(".alert").length) {
									$('html, body').animate({
										scrollTop: $("[ng-view]").offset().top
									}, '500', 'swing');
								}
							}, function error(response) {
								console.log("Some error =>");
								console.log(response);
							});

							return false;
						});
					});
				};
			},
			link: function(scope, element, attrs) {
				scope.bindForms(element);
			}
		};
	});

	/**
	 * Handles the hidden menu animation
	 */
	storeCtrl.directive("hiddenMenu", function() {
		return {
			restrict: "A",
			link: function(scope, element) {
				var menu = element.find("ul.hidden-menu");

				if (menu) {
					element.bind("mouseenter", function(e) {
						menu.addClass("show");
					});
					element.bind("mouseleave", function(e) {
						menu.removeClass("show");
					});
				}
			}
		};
	});

	storeCtrl.directive("carouselOffers", function() {
		return {
			restrict: "A",
			link: function(scope, element) {
				element.find(".carousel-control").each(function(i, el) {
					if ($(el).hasClass("left")) {
						$(el).bind("click", function(e) {
							e.preventDefault();
							e.stopPropagation();

							element.carousel("prev");

							return false;
						});
					} else if ($(el).hasClass("right")) {
						$(el).bind("click", function(e) {
							e.preventDefault();
							e.stopPropagation();

							element.carousel("next");

							return false;
						});
					}
				});
			}
		};
	});

	/**
	 * Checkout
	 */
	storeCtrl.directive("checkout", function() {
		return {
			restrict: "A",
			controller: ["$scope", "$route", function($scope, $route) {
				$scope.checkout = {
					removeFromList: function(hash) {
						$scope.cart.removeFromCart(hash, function() {
							$route.reload();
						});
					},
					emptyCart: function() {
						$scope.cart.empty(function() {
							$route.reload();
						});
					}
				};
			}]
		};
	});
})();