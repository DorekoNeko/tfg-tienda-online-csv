/**
 * Created by Daniel on 15/05/16.
 */

(function () {
	var user = angular.module("userCtrl", []);

	/**
	 * Handles the profile picture management
	 */
	user.directive("profilePictureManagement", function() {
		return {
			restrict: "A",
			link: function(scope, element) {
				var pic_preview = element.find("img.pic-preview");
				var links = element.find("a");
				var input = $(document.createElement("input")).attr({
					"type": "file",
					"name": "profile-picture"
				});
				var deleteRequestHidden = $(document.createElement("input")).attr({
					"type": "hidden",
					"name": "remove-picture"
				});

				// Appending input
				element.append(input);

				// Binding events
				links.each(function(i, el) {
					if ($(el).hasClass("pic-change")) {
						$(el).bind("click", function(e) {
							e.preventDefault();

							input.click();

							return false;
						});
					} else if ($(el).hasClass("pic-remove")) {
						$(el).bind("click", function(e) {
							e.preventDefault();

							// Reset the input
							input.wrap('<form>').closest('form').get(0).reset();
							input.unwrap();
							// Add hidden input
							element.append(deleteRequestHidden.clone());
							// Changing the preview
							pic_preview.attr("src", "/assets/images/default-avatar.png");

							return false;
						});
					}
				});

				// Handling the change event of the image file
				input.on("change", function() {
					if (this.files.length > 0) {
						var reader = new FileReader();
						reader.onloadend = function(e, file) {
							var bin = this.result;
							// Changing the preview
							pic_preview.attr("src", bin);
						};

						// Read only the first file.
						reader.readAsDataURL(this.files[0]);

						// Clean the remove hidden input if exists
						if (element.find("input[name='remove-picture']").length) {
							element.find("input[name='remove-picture']").remove();
						}
					}
				});
			}
		};
	});

	user.directive("confirmation", function() {
		return {
			restrict: "A",
			controller: function($scope) {
				$scope.forward_url = "";
			},
			link: function(scope, element) {
				// Prevent default
				element.bind("click", function(e) {
					e.preventDefault();
					e.stopPropagation();

					scope.forward_url = element.attr("href");
					$("#bt-confirm-modal").modal("show");

					return false;
				});

				// Follow link when confirmation succeed
				element.parents("body").find("#bt-confirm-modal-proceed").bind("click", function(e) {
					e.preventDefault();
					e.stopPropagation();

					window.location = scope.forward_url;
					$("#bt-confirm-modal").modal("hide");

					return false;
				});
			}
		};
	});
})();