/**
 * Created by Daniel on 20/05/16.
 */
(function() {
	var chat = angular.module("Chat", []);

	chat.factory("alerts", ["$rootScope", "$compile", "$timeout", function($rootScope, $compile, $timeout) {
		var instance = {};

		instance.element = undefined;

		instance.init = function(element) {
			element.append("<div class='alerts' style='position: fixed; bottom: 0; right: 30px; width: 480px;'></div>");
			instance.element = element.find(".alerts");
		};

		instance.addAlert = function(text, type, dismisable) {
			type = typeof type !== "undefined" ? type : "danger";
			dismisable = typeof dismisable !== "undefined" ? dismisable : true;

			var alert;
			if (dismisable) {
				alert = $("<div class='alert alert-dismissible alert-" + type + "' role='alert'>" +
						"<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + text +
					"</div>");
			} else {
				alert = $("<div class='alert alert-" + type + "' role='alert'>" + text + "</div>");
			}

			alert.css({
				"position": "relative",
				"top": "999px"
			});

			instance.element.prepend(alert);

			alert.animate({
				"top": 0
			}, 350, function() {
				$timeout(function() {
					alert.fadeOut(350, function() {
						alert.remove();
					});
				}, 6000);
			});
		};

		return instance;
	}]);

	/**
	 * Main controller
	 */
	chat.controller("ChatController", ["$scope", "$element", "alerts", function($scope, $element, alerts) {
		$scope.socket = null;
		$scope.connected = false;

		// Creating the tone
		$scope.tone = {
			audio: new Audio("/assets/sounds/success_tone.mp3"),
			play: function() {
				if (this.audio !== null) {
					this.audio.play();
				}
			}
		};

		// Initializing alerts service
		alerts.init($element);

		$scope.connectOrDisconnect = function() {
			if (window.WebSocket) {
				if ($scope.socket === null) {
					var loc = window.location, uri;
					if (loc.protocol === "https:") {
						uri = "wss:";
					} else {
						uri = "ws:";
					}

					$scope.socket = new WebSocket(uri + "//" + loc.hostname + ":" + loc.port + "/chat/socket");

					$scope.socket.onopen = function(e) {
						$scope.$apply(function() {
							$scope.connected = true;
						});

						alerts.addAlert("Connected!", "success");
					};

					$scope.socket.onmessage = function(e) {
						console.log("message");
						var msg = JSON.parse(e.data);

						switch(msg.type) {
							case "notify-chat-start":
								$scope.$apply(function() {
									if ($scope.chat_list.chats.length === 0) {
										$scope.chat_list.add(msg.conversation.hash, msg.username, true);
									} else {
										$scope.chat_list.add(msg.conversation.hash, msg.username);
									}

									$scope.tone.play();

									var chat = $scope.chat_list.get(msg.conversation.hash);
									chat.addStatus("Connected with " + chat.username);
								});
								break;
							case "notify-chat-end":
								$scope.$apply(function() {
									$scope.chat_list.setState(msg.conversation.hash, false);
									var chat = $scope.chat_list.get(msg.conversation.hash);
									chat.addStatus(chat.username + " is now offline.");
								});
								break;
							case "message-received":
								$scope.$apply(function() {
									var chat = $scope.chat_list.get(msg.conversation.hash);
									chat.addMessage(msg.message, msg.username);
									$scope.tone.play();
								});
								break;
							default:
								console.log(msg);
								break;
						}
					};

					$scope.socket.onclose = function(e) {
						$scope.$apply(function() {
							$scope.connected = false;
						});

						if (e.code == 1000) {
							alerts.addAlert("Disconnected!", "success");
						} else if (e.code == 1006) {
							alerts.addAlert("Error " + e.code + ": The connection was closed abnormally. You sure you are signed in as admin?");
						}
					};
				} else {
					$scope.disconnect();
				}
			} else {
				alerts.addAlert("WebSockets aren't supported by your browser. Please try using another browser.");
			}
		};

		$scope.disconnect = function() {
			if ($scope.socket !== null) {
				$scope.connected = false;
				$scope.socket.close(1000);
				$scope.socket = null;
				$scope.chat_list.chats = [];
			}
		};
	}]);

	/**
	 * Gets the username of the admin
	 */
	chat.directive("userName", function() {
		return {
			restrict: "A",
			link: function(scope, element, attr) {
				scope.username = attr.userName;
			}
		};
	});

	/**
	 * Handles the chat list
	 */
	chat.directive("chatList", function() {
		return {
			restrict: "E",
			controller: ["$scope", function($scope) {
				$scope.chat_list = {
					chats: [],
					add: function(hash, username, active) {
						active = typeof active !== "undefined" ? active : false;
						this.chats.push({
							is_active: active,
							hash: hash,
							username: username,
							online: true,
							messages: [],
							message: "",
							sendMessage: $scope.chat_list.fn.sendMessage,
							addMessage: $scope.chat_list.fn.addMessage,
							addStatus: $scope.chat_list.fn.addStatus,
							show: $scope.chat_list.fn.show
						});
					},
					get: function(hash) {
						for (var i = 0; i < this.chats.length; i++) {
							if (this.chats[i].hash == hash) {
								return this.chats[i];
							}
						}
					},
					setState: function(hash, state) {
						for (var i = 0; i < this.chats.length; i++) {
							if (this.chats[i].hash == hash) {
								this.chats[i].online = state;
							}
						}
					},
					remove: function(hash) {
						var chat = this.get(hash);
						var request = {
							type: "chat-end",
							hash: hash
						};

						$scope.socket.send(JSON.stringify(request));
						this.chats.splice(this.chats.indexOf(chat), 1);
					},
					fn: {
						sendMessage: function(message) {
							if ($scope.socket !== null && this.message.length > 0) {
								var request = {
									type: "send-message",
									message: this.message,
									hash: this.hash
								};

								$scope.socket.send(JSON.stringify(request));
								this.addMessage(this.message, $scope.username);
								this.message = "";
							}
						},
						addMessage: function(message, username) {
							var date = new Date();
							this.messages.push({
								is_status: false,
								body: message,
								timestamp: date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds(),
								username: username
							});

							var container = $("." + this.hash).find(".messages");
							container.animate({
								scrollTop: container[0].scrollHeight
							}, 300);
						},
						addStatus: function(message) {
							var date = new Date();
							this.messages.push({
								is_status: true,
								body: message,
								timestamp: date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds(),
								username: ""
							});

							var container = $("." + this.hash).find(".messages");
							container.animate({
								scrollTop: container[0].scrollHeight
							}, 300);
						},
						show: function() {
							for (var i = 0; i < $scope.chat_list.chats.length; i++) {
								var chat = $scope.chat_list.chats[i];
								if (chat == this) {
									chat.is_active = true;
								} else {
									chat.is_active = false;
								}
							}
						}
					}
				};
			}]
		};
	});

	/**
	 * Handling the message input
	 */
	chat.directive("messageHandler", function() {
		return {
			restrict: "A",
			scope: {
				chat: "="
			},
			link: function(scope, element) {
				element.bind("keypress", function(event) {
					scope.$apply(function() {
						if (scope.chat.online) {
							if (event.keyCode == 13) {
								scope.chat.sendMessage();
							} else {
								scope.chat.message += String.fromCharCode(event.keyCode);
							}
						}
					});

					return false;
				});
			}
		};
	});
})();