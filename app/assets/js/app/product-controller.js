/**
 * Created by Daniel on 18/05/16.
 */
(function(){
	var product = angular.module("productCtrl", []);

	/**
	 * Handles the product gallery initialization of
	 * the LightGallery plugin and fixing the carousel buttons.
	 */
	product.directive("productGallery", function() {
		return {
			restrict: "A",
			link: function(scope, element) {
				// Initializing LightGallery plugin
				element.lightGallery({
					thumbnail: true
				});

				element.parent().find(".carousel-control").each(function(i, el) {
					if ($(el).hasClass("left")) {
						$(el).bind("click", function(e) {
							e.preventDefault();
							e.stopPropagation();

							element.parent().carousel("prev");

							return false;
						});
					} else if ($(el).hasClass("right")) {
						$(el).bind("click", function(e) {
							e.preventDefault();
							e.stopPropagation();

							element.parent().carousel("next");

							return false;
						});
					}
				});
			}
		};
	});

	/**
	 * Handles the customer reviews view
	 */
	product.directive("customerReviews", function($http, $compile) {
		return {
			restrict: "E",
			template: "<div ng-include='reviews_url'></div>",
			controller: function($scope) {
				$scope.ref = "";
				$scope.reviews_url = "";

				$scope.reloadReviews = function(element) {
					$http.get($scope.reviews_url).then(
						function success(response) {
							console.log("Review reloaded!");
							element.html(response.data);
							$compile(element.contents())($scope);
						}, function error(response) {
							console.log("Error during reviews reload =>");
							console.log(response);
						}
					);
				};
			},
			link: function(scope, element, attrs) {
				scope.ref = attrs.productRef;
				scope.reviews_url = "/ajax/product/" + scope.ref + "/reviews";

				scope.$on("$reviewsAdded", function() {
					scope.reloadReviews(element);
				});
			}
		};
	});

	/**
	 * Handles the form submission
	 */
	product.directive("addReviewForm", function($http) {
		return {
			restrict: "A",
			controller: function($scope) {
				$scope.review = {
					url: "/ajax/product/" + $scope.ref + "/reviews/add",
					data: {
						valoration: "1",
						body: ""
					},
					errors: false,
					body_min: 80
				};

				$scope.submitReview = function() {
					if ($scope.review.data.body.length >= $scope.review.body_min) {
						$http.post($scope.review.url, $scope.review.data).then(
							function success(response) {
								console.log("Review added!");
								$scope.$emit("$reviewsAdded");
							}, function error(response) {
								console.log("Error adding review =>");
								console.log(response);
							}
						);
					} else {
						$scope.review.errors = true;
					}
				};
			}
		};
	});
})();