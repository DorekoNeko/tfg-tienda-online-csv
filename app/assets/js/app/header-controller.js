/**
 * Created by Daniel on 16/05/16.
 */
(function () {
	var header = angular.module("headerCtrl", []);

	/**
	 * Handles the scrolling bar
	 */
	header.directive("scrolling", function() {
		return {
			restrict: "A",
			link: function(scope, element) {
				$(window).bind("scroll", function(ev) {
					var fromTop = $(window).scrollTop();
					var headerHeight = $("header").height();
					$("body").toggleClass("down", (fromTop > headerHeight));
				});
			}
		};
	});

	header.directive("search", ["$http", function($http) {
		return {
			restrict: "A",
			controller: ["$scope", function($scope) {
				$scope.search_results = [];
				$scope.search_no_results = false;

				$scope.search = {
					query: function(query) {
						$http.post("/ajax/search/" + query).then(
							function success(response) {
								if (response.data.length === 0) {
									$scope.search_no_results = true;
									$scope.search_results = [];
								} else {
									$scope.search_no_results = false;
									$scope.search_results = response.data;
								}
							},
							function error(response) {
								console.log("$http error =>");
								console.log(response);
							}
						);
					},
					defaultPicture: function(item) {
						var gallery = item.gallery;
						for (var i = 0; i < gallery.length; i++) {
							var image = gallery[i];
							if (image.is_default) {
								return image.fileuuid;
							}
						}
					},
					price: function(item) {
						var withVAT = function(price) { return price + (price * (item.vat.percentage / 100.0)); };
						//console.log(item.product.price + " => (" + item.product.vat.percentage+ "%) " + withVAT(item.product.price));

						if (item.in_offer) {
							var discount_price = 0;
							if (item.use_date) {
								if (item.offer_date_until > Date.now()) {
									if (item.use_percentage) {
										discount_price = (item.price - (item.discount * item.price / 100.0) > 0) ? item.price - (item.discount * item.price / 100.0) : 0;
									} else {
										discount_price = (item.price - item.discount > 0) ? item.price - item.discount : 0;
									}
								} else {
									discount_price = vat_price;
								}
							} else if (item.use_percentage) {
								discount_price = (item.price - (item.discount * item.price / 100.0) > 0) ? item.price - (item.discount * item.price / 100.0) : 0;
							} else {
								discount_price = (item.price - item.discount > 0) ? item.price - item.discount : 0;
							}

							return withVAT(discount_price);
						} else {
							return withVAT(item.price);
						}
					}
				};
			}],
			link: function(scope, element) {
				var backdrop = $("body").find(".search-backdrop");
				var close = backdrop.find(".close");
				var input = backdrop.find("#search-input");
				element.bind("click", function(e) {
					scope.$apply(function() {
						scope.search_no_results = false;
					});
					backdrop.fadeIn(400);
					input.focus();
				});
				close.bind("click", function(e) {
					backdrop.fadeOut(400);
				});
				input.bind("keyup", function(e) {
					if (input.val().length > 3) {
						scope.$apply(function() {
							scope.search.query(input.val());
						});
					} else {
						scope.$apply(function() {
							scope.search_results = [];
							scope.search_no_results = false;
						});
					}
				});
				$(document).on("click", ".search-backdrop .link", function(e) {
					scope.$apply(function() {
						backdrop.fadeOut(400);
						input.val("");
						scope.search_no_results = false;
						scope.search_results = [];
					});
				});
			}
		};
	}]);

	/**
	 * Handles the user menu
	 */
	header.directive("userMenu", function($http, $compile) {
		return {
			restrict: "A",
			templateUrl: "/ajax/account/header-menu",
			controller: function($scope) {
				$scope.updateView = function(element) {
					$http.get("/ajax/account/header-menu").then(
						function success(response) {
							console.log("User menu updated.");
							element.html(response.data);
							$compile(element.contents())($scope);
						}, function error(response) {
							console.log("Error updating the user menu =>");
							console.log(response);
						}
					);
				};
			},
			link: function(scope, element) {
				// Register to the $userChanged event to update the user menu
				scope.$on("$userChanged", function() {
					scope.updateView(element);
				});
			}
		};
	});

	/**
	 * Handles the user cart
	 */
	header.directive("userCart", function($http, $compile) {
		return {
			restrict: "A",
			templateUrl: "/ajax/account/header-cart",
			controller: ["$scope", "alerts", function($scope, alerts) {
				$scope.cart = {
					products: [],
					total_products: 0,
					updateView: function(element) {
						$http.get("/ajax/account/header-cart").then(
							function success(response) {
								console.log("User cart updated.");
								element.html(response.data);
								$compile(element.contents())($scope);
							}, function error(response) {
								console.log("Error updating the cart menu =>");
								console.log(response);
							}
						);
					},
					updateProducts: function() {
						$http.get("/cart.json").then(
							function success(response) {
								$scope.cart.products = response.data;
								$scope.cart.updateTotalProducts();
							}, function error(response) {
								console.log("$http error =>");
								console.log(response);
							}
						);
					},
					updateTotalProducts: function() {
						var total = 0;
						for (var i = 0; i < this.products.length; i++) {
							var product = this.products[i];
							total += product.quantity;
						}
						this.total_products = total;
					},
					removeFromCart: function(hash, callback) {
						if ($scope.store.signed_in) {
							$http.post("/cart/remove/" + hash).then(
								function success(response) {
									$scope.cart.updateProducts();

									if (callback !== "undefined") {
										callback();
									}
								}, function error(response) {
									console.log("$http error =>");
									console.log(response);
								}
							);
						} else {
							alerts.addAlert("Please sign in.");
						}
					},
					empty: function(callback) {
						if ($scope.store.signed_in) {
							$http.post("/cart/empty").then(
								function success(response) {
									$scope.cart.updateProducts();

									if (callback !== "undefined") {
										callback();
									}
								}, function error(response) {
									console.log("$http error =>");
									console.log(response);
								}
							);
						} else {
							alerts.addAlert("Please sign in.");
						}
					},
					defaultPicture: function(item) {
						var gallery = item.product.gallery;
						for (var i = 0; i < gallery.length; i++) {
							var image = gallery[i];
							if (image.is_default) {
								return image.fileuuid;
							}
						}
					},
					price: function(item) {
						var withVAT = function(price) { return price + (price * (item.product.vat.percentage / 100.0)); };
						//console.log(item.product.price + " => (" + item.product.vat.percentage+ "%) " + withVAT(item.product.price));

						if (item.product.in_offer) {
							var discount_price = 0;
							if (item.product.use_date) {
								if (item.product.offer_date_until > Date.now()) {
									if (item.product.use_percentage) {
										discount_price = (item.product.price - (item.product.discount * item.product.price / 100.0) > 0) ? item.product.price - (item.product.discount * item.product.price / 100.0) : 0;
									} else {
										discount_price = (item.product.price - item.product.discount > 0) ? item.product.price - item.product.discount : 0;
									}
								} else {
									discount_price = vat_price;
								}
							} else if (item.product.use_percentage) {
								discount_price = (item.product.price - (item.product.discount * item.product.price / 100.0) > 0) ? item.product.price - (item.product.discount * item.product.price / 100.0) : 0;
							} else {
								discount_price = (item.product.price - item.product.discount > 0) ? item.product.price - item.product.discount : 0;
							}

							return withVAT(discount_price);
						} else {
							return withVAT(item.product.price);
						}
					},
					totalPrice: function() {
						var total = 0;

						for (var i = 0; i < this.products.length; i++) {
							var price = this.price(this.products[i]);
							total += price * this.products[i].quantity;
						}

						return total;
					}
				};

				// Get cart products
				$scope.cart.updateProducts();
			}],
			link: function(scope, element) {
				// Register to the $userCartChanged event to update the user cart
				scope.$on("$userCartChanged", function() {
					scope.cart.updateView(element);
				});

				scope.store.signed_in = true;
			}
		};
	});

	/**
	 * Handles the category menu
	 */
	header.directive("categoryMenu", function($http, $compile) {
		return {
			restrict: "A",
			templateUrl: "/ajax/category/base-menu",
			controller: function($scope) {
				$scope.bindLinks = function(element) {
					var category_links = element.find("*[data-category]");

					// Searching for category top navigation links
					element.find(".breadcrumb > li > a.category-base").each(function(i, el) {
						$(el).bind("click", function(e) {
							e.preventDefault();
							e.stopPropagation();

							$http.get("/ajax/category/base-menu").then(
								function success(response) {
									console.log("Category menu changed.");
									element.html(response.data);
									$compile(element.contents())($scope);
									$scope.bindLinks(element);
								}, function error(response) {
									console.log("Error changing category menu =>");
									console.log(response);
								}
							);

							return false;
						});
					});

					// Binding category links
					category_links.each(function(i, el) {
						$(el).bind("click", function(e) {
							e.preventDefault();
							e.stopPropagation();

							if ($(el).attr("data-category") !== undefined) {
								var normalized = $(el).attr("data-category");
								$http.get("/ajax/category/sub-menu/" + normalized).then(
									function success(response) {
										console.log("Category menu changed.");
										element.html(response.data);
										$compile(element.contents())($scope);
										$scope.bindLinks(element);
									}, function error(response) {
										console.log("Error changing category menu =>");
										console.log(response);
									}
								);
							}

							return false;
						});
					});
				};
			},
			link: function(scope, element) {
				scope.bindLinks(element);
			}
		};
	});
})();