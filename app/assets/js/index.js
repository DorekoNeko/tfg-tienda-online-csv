/**
 * Created by Daniel on 1/03/16.
 */

/**
 * JQuery Initialization
 */
$(function () {
	/**
	 * Override default behaviour of empty anchors
	 */
	$(document).on("click", "a[href='!']", function(e) {
		return false;
	});

	// Bootstrap tooltips and popovers fixed for ajax content
	$('body').tooltip({
		selector: '.has-tooltip'
	}).popover({
		container: 'body',
		selector: '.has-popover'
	});

	// Shop Generator Ad
	if (getCookie("display-generator-ad") === null) {
		// Show the ad every hour
		setCookie("display-generator-ad", "true", 24*60*60*1000);
		$("#shop-generator-ad").modal();
	}

	/**
	 * Sets a cookie
	 * @param cname
	 * @param cvalue
	 * @param expiry_time
	 */
	function setCookie(cname, cvalue, expiry_time) {
		var d = new Date();
		d.setTime(d.getTime() + expiry_time);
		var expires = "expires="+ d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	/**
	 * Gets a cookie
	 * @param cname
	 * @returns {*}
	 */
	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) === 0) {
				return c.substring(name.length,c.length);
			}
		}
		return null;
	}
});