package actions.builders

import actions.builders.requests.AuthenticatedUserSession
import dao.User
import play.api.Play
import play.api.Play.current
import play.api.mvc.Security.{AuthenticatedBuilder, AuthenticatedRequest}
import play.api.mvc._
import util.UserSession
import util.UserSession.Session
import util.Validation._

import scala.concurrent.Future

/**
	* Created by Daniel on 15/05/16.
	*/
object AuthenticatedWithAdmin extends ActionBuilder[AuthenticatedUserSession] {
	def invokeBlock[A](request: Request[A], block: (AuthenticatedUserSession[A]) => Future[Result]) = {
		AuthenticatedBuilder(
			userInfoReq => {
				// If we are in dev mode we create a dummy user instance if there is no session
				if (Play.configuration.getBoolean("developer_mode").getOrElse(false)) {
					if (UserSession.validateWithSession(userInfoReq.session) == Valid) {
						val user_instance = UserSession.get(userInfoReq.session.get("token").get).get.get("user").get.asInstanceOf[User]
						if (user_instance.is_admin) {
							UserSession.get(userInfoReq.session.get("token").get)
						} else {
							None
						}
					} else {
						Some(UserSession.startDummySession)
					}
				} else {
					if (UserSession.validateWithSession(userInfoReq.session) == Valid) {
						val user_instance = UserSession.get(userInfoReq.session.get("token").get).get.get("user").get.asInstanceOf[User]
						if (user_instance.is_admin) {
							UserSession.get(userInfoReq.session.get("token").get)
						} else {
							None
						}
					} else {
						None
					}
				}
			},
			unauthorizedReq => {
				if (unauthorizedReq.path.startsWith("/ajax")) {
					Results.Redirect(controllers.ajax.routes.Application.unauthorized())
				} else {
					Results.Redirect(controllers.routes.Application.unauthorized())
				}
			}
		).authenticate(request, { authRequest: AuthenticatedRequest[A, Session] =>
			block(new AuthenticatedUserSession[A](authRequest.user, request))
		})
	}
}