package actions.builders.requests

import play.api.mvc.{Request, WrappedRequest}
import util.UserSession.Session

/**
	* Created by Daniel on 25/05/16.
	*/
class AuthenticatedUserSession[A](val user_session: Session, request: Request[A]) extends WrappedRequest[A](request)