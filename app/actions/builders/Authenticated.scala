package actions.builders

import actions.builders.requests.AuthenticatedUserSession
import play.api.mvc.Security.{AuthenticatedBuilder, AuthenticatedRequest}
import play.api.mvc._
import util.UserSession
import util.UserSession.Session
import util.Validation._

import scala.concurrent.Future

/**
	* Created by Daniel on 15/05/16.
	*/
object Authenticated extends ActionBuilder[AuthenticatedUserSession] {
	def invokeBlock[A](request: Request[A], block: (AuthenticatedUserSession[A]) => Future[Result]) = {
		AuthenticatedBuilder(
			userInfoReq => {
				if (UserSession.validateWithSession(userInfoReq.session) == Valid) {
					UserSession.get(userInfoReq.session.get("token").get)
				} else {
					None
				}
			},
			unauthorizedReq => {
				if (unauthorizedReq.path.startsWith("/ajax")) {
					Results.Redirect(controllers.ajax.routes.Application.unauthorized())
				} else {
					Results.Redirect(controllers.routes.Application.unauthorized())
				}
			}
		).authenticate(request, { authRequest: AuthenticatedRequest[A, Session] =>
			block(new AuthenticatedUserSession[A](authRequest.user, request))
		})
	}
}