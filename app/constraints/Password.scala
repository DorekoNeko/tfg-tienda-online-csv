package constraints

import play.api.Play.current
import play.api.data.validation.{Constraint, ValidationError}
import play.api.i18n.Messages
import play.api.i18n.Messages.Implicits._

/**
	* Created by Daniel on 14/05/16.
	*/
object Password {
	val passwordCheckConstraint: Constraint[String] = Constraint("constraints.passwordcheck")({
		plainText =>
			val allNumbers = """\d*""".r
			val allLetters = """[A-Za-z]*""".r
			val lengthCheck = """^[A-Za-z0-9]{0,7}$""".r
			val errors = plainText match {
				case allNumbers() => Seq(ValidationError(Messages("register.password.security")))
				case allLetters() => Seq(ValidationError(Messages("register.password.security")))
				case lengthCheck() => Seq(ValidationError(Messages("register.password.short")))
				case _ => Nil
			}

			if (errors.isEmpty) {
				play.api.data.validation.Valid
			} else {
				play.api.data.validation.Invalid(errors)
			}
	})
}
