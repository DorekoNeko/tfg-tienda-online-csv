package dao

import javax.inject._

import com.google.inject.ImplementedBy
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, JsPath, Reads, Writes}
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
	* Created by Daniel on 18/04/16.
	*/
@ImplementedBy(classOf[PopulationDAO])
trait IPopulationDAO {
	def all: Future[Seq[Population]]
	def get(id: Int): Future[Population]
	def exists(name: String): Future[Boolean]
	def exists(id: Int): Future[Boolean]
}

case class Population(id: Int, province_id: Int, name: String, name_seo: String, postal_code: Int, lat: Double, long: Double)

object Population {
	implicit lazy val populationReads: Reads[Population] = (
		(JsPath \ "id").read[Int] and
			(JsPath \ "province_id").read[Int] and
			(JsPath \ "name").read[String] and
			(JsPath \ "name_seo").read[String] and
			(JsPath \ "postal_code").read[Int] and
			(JsPath \ "lat").read[Double] and
			(JsPath \ "long").read[Double]
		)(Population.apply _)

	implicit lazy val populationWrites: Writes[Population] = (
		(JsPath \ "id").write[Int] and
			(JsPath \ "province_id").write[Int] and
			(JsPath \ "name").write[String] and
			(JsPath \ "name_seo").write[String] and
			(JsPath \ "postal_code").write[Int] and
			(JsPath \ "lat").write[Double] and
			(JsPath \ "long").write[Double]
		)(unlift(Population.unapply))

	implicit val populationFormat: Format[Population] = Format(populationReads, populationWrites)
}

class PopulationDAO @Inject()(
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with IPopulationDAO {
	import driver.api._

	val Populations = TableQuery[PopulationsTable]

	/**
		* Returns all the populations
		* @return
		*/
	def all: Future[Seq[Population]] = db.run(Populations.result)

	/**
		* Gets the population with the given id
		* @param id
		* @return
		*/
	def get(id: Int): Future[Population] = db.run(Populations.filter(_.id === id).result.head)

	/**
		* Checks if a population with the given name exists
		* @param name
		* @return
		*/
	def exists(name: String): Future[Boolean] = {
		db.run(Populations.filter(_.name === name).result).map { case s => s.nonEmpty }
	}

	/**
		* Checks if a population with the given id exists
		* @param id
		* @return
		*/
	def exists(id: Int): Future[Boolean] = {
		db.run(Populations.filter(_.id === id).result).map { case s => s.nonEmpty }
	}

	// Table
	class PopulationsTable(tag: Tag) extends Table[Population](tag, "poblacion") {

		def id = column[Int]("idpoblacion", O.PrimaryKey, O.AutoInc)

		def province_id = column[Int]("idprovincia")

		def name = column[String]("poblacion")

		def name_seo = column[String]("poblacionseo")

		def postal_code = column[Int]("postal")

		def lat = column[Double]("latitud")

		def long = column[Double]("longitud")

		def * = (id, province_id, name, name_seo, postal_code, lat, long) <> ((Population.apply _).tupled, Population.unapply _)
	}
}
