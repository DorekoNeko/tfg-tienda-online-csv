package dao

import javax.inject._

import com.google.inject.ImplementedBy
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.json.{Format, Json}
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
	* Created by Daniel on 18/04/16.
	*/
@ImplementedBy(classOf[ProductGalleryDAO])
trait IProductGalleryDAO {
	def getGallery(product_id: Int): Future[Seq[GalleryItem]]
	def insert(item: GalleryItem): Future[Unit]
	def update(item: GalleryItem): Future[Unit]
	def delete(fileuuid: String): Future[Unit]
	def deleteGallery(product_id: Int): Future[Unit]
}

case class GalleryItem(id: Option[Int], product_id: Int, fileuuid: String, var is_default: Boolean = false, var is_slider: Boolean = false)

object GalleryItem {
	implicit val galleryItemFormat: Format[GalleryItem] = Json.format[GalleryItem]
}

class ProductGalleryDAO @Inject()(
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with IProductGalleryDAO {
	import driver.api._

	val Galleries = TableQuery[ProductGalleryTable]

	/**
		* Returns the gallery of a product
		* @param product_id
		* @return
		*/
	def getGallery(product_id: Int): Future[Seq[GalleryItem]] = db.run(Galleries.filter(_.product_id === product_id).result)

	/**
		* Adds a new gallery item
		* @param item
		* @return
		*/
	def insert(item: GalleryItem): Future[Unit] = db.run(Galleries += item).map(_ => ())

	/**
		* Updates the given item
		* @param item
		* @return
		*/
	def update(item: GalleryItem): Future[Unit] = db.run(Galleries.filter(_.id === item.id.get).update(item)).map( _ => () )

	/**
		* Deletes the given item
		* @param fileuuid
		* @return
		*/
	def delete(fileuuid: String): Future[Unit] = db.run(Galleries.filter(_.imageuuid === fileuuid).delete).map(_ => ())

	/**
		* Deletes the given product gallery
		* @param product_id
		* @return
		*/
	def deleteGallery(product_id: Int): Future[Unit] = db.run(Galleries.filter(_.product_id === product_id).delete).map(_ => ())

	class ProductGalleryTable(tag: Tag) extends Table[GalleryItem](tag, "product_gallery") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def product_id = column[Int]("product_id")

		def imageuuid = column[String]("imageuuid")

		def is_default = column[Boolean]("is_default")

		def is_slider = column[Boolean]("is_slider")

		def * = (id.?, product_id, imageuuid, is_default, is_slider) <> ((GalleryItem.apply _).tupled, GalleryItem.unapply)
	}
}
