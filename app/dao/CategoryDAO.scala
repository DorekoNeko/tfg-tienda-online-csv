package dao

import javax.inject._

import com.google.inject.ImplementedBy
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.functional.syntax._
import play.api.libs.json._
import slick.driver.JdbcProfile

import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Future, Promise}

/**
	* Created by Daniel on 18/04/16.
	*/
@ImplementedBy(classOf[CategoryDAO])
trait ICategoryDAO {
	def all: Future[Seq[ComplexCategory]]
	def allBase: Future[Seq[ComplexCategory]]
	def get(category_id: Int): Future[ComplexCategory]
	def get(normalized_name: String): Future[ComplexCategory]
	def getChildren(category_id: Int): Future[Seq[Category]]
	def getComplexChildren(category_id: Int): Future[Seq[ComplexCategory]]
	def getParents(category_id: Int): Future[Seq[ComplexCategory]]
	def exists(category_id: Int): Future[Boolean]
	def exists(normalized_name: String): Future[Boolean]
	def update(category: Category): Future[Unit]
	def delete(category_id: Int): Future[Unit]
	def insert(category: Category): Future[Unit]
}

case class Category(id: Option[Int] = None, lang_name_id: String, parent_id: Int, order: Int, normalized_url: String)
case class ComplexCategory(id: Option[Int] = None, name: Lang, order: Int, children: Seq[Category], parent: Option[ComplexCategory], normalized_url: String)

object Category {
	implicit val categoryFormat: Format[Category] = Json.format[Category]
}

object ComplexCategory {
	implicit lazy val complexCategoryReads: Reads[ComplexCategory] = (
		(JsPath \ "id").readNullable[Int] and
			(JsPath \ "name").read[Lang] and
			(JsPath \ "order").read[Int] and
			(JsPath \ "children").read[Seq[Category]] and
			(JsPath \ "parent").lazyReadNullable(Reads.of[ComplexCategory]) and
			(JsPath \ "normalized_url").read[String]
		)(ComplexCategory.apply _)

	implicit lazy val complexCategoryWrites: Writes[ComplexCategory] = (
		(JsPath \ "id").writeNullable[Int] and
			(JsPath \ "name").write[Lang] and
			(JsPath \ "order").write[Int] and
			(JsPath \ "children").write[Seq[Category]] and
			(JsPath \ "parent").lazyWriteNullable(Writes.of[ComplexCategory]) and
			(JsPath \ "normalized_url").write[String]
		)(unlift(ComplexCategory.unapply))

	implicit val complexCategoryFormat: Format[ComplexCategory] = Format(complexCategoryReads, complexCategoryWrites)
}

class CategoryDAO @Inject()(
	protected val dbConfigProvider: DatabaseConfigProvider,
	protected val langItemDAO: ILangItemDAO
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with ICategoryDAO {
	import driver.api._

	val Categories = TableQuery[CategoriesTable]

	/**
		* Returns all the categories.
		* @return
		*/
	def all: Future[Seq[ComplexCategory]] = {
		val p = Promise[Seq[ComplexCategory]]
		val f = p.future

		db.run(Categories.result).map { results => iterate(0, results, Seq()) }

		def iterate(index: Int = 0, raws: Seq[Category], f: Seq[ComplexCategory]): Unit = {
			if (raws.isEmpty) {
				p success f
			} else {
				val actual = raws(index)
				get(actual.id.get).map { c =>
					if (index == raws.length-1) {
						p success f :+ c
					} else {
						iterate(index+1, raws, f :+ c)
					}
				}
			}
		}

		f
	}

	def allBase: Future[Seq[ComplexCategory]] = {
		val p = Promise[Seq[ComplexCategory]]
		val f = p.future

		db.run(Categories.filter(_.parent_id === 0).result).map { results => iterate(0, results, Seq()) }

		def iterate(index: Int = 0, raws: Seq[Category], f: Seq[ComplexCategory]): Unit = {
			if (raws.isEmpty) {
				p success f
			} else {
				val actual = raws(index)
				get(actual.id.get).map { c =>
					if (index == raws.length-1) {
						p success f :+ c
					} else {
						iterate(index+1, raws, f :+ c)
					}
				}
			}
		}

		f
	}

	/**
		* Gets the category with the given id.
		* @param category_id
		* @return
		*/
	def get(category_id: Int): Future[ComplexCategory] = {
		val p = Promise[ComplexCategory]
		val f = p.future

		db.run(Categories.filter(_.id === category_id).result.head).map { raw =>
			langItemDAO.get(raw.lang_name_id).map { lang =>
				getChildren(category_id).map { children =>
					if (raw.parent_id != 0) {
						get(raw.parent_id).map { parent =>
							p success new ComplexCategory(raw.id, lang, raw.order, children, Some(parent), raw.normalized_url)
						}
					} else {
						p success new ComplexCategory(raw.id, lang, raw.order, children, None, raw.normalized_url)
					}
				}
			}
		}

		f
	}

	/**
		* Gets the category with the given normalized name.
		* @param normalized_name
		* @return
		*/
	def get(normalized_name: String): Future[ComplexCategory] = {
		val p = Promise[ComplexCategory]
		val f = p.future

		db.run(Categories.filter(_.normalized_url === normalized_name).result.head).map { raw =>
			langItemDAO.get(raw.lang_name_id).map { lang =>
				getChildren(raw.id.get).map { children =>
					if (raw.parent_id != 0) {
						get(raw.parent_id).map { parent =>
							p success new ComplexCategory(raw.id, lang, raw.order, children, Some(parent), raw.normalized_url)
						}
					} else {
						p success new ComplexCategory(raw.id, lang, raw.order, children, None, raw.normalized_url)
					}
				}
			}
		}

		f
	}

	/**
		* Gets the children of the given category in form of Category objects
		* @param category_id
		* @return
		*/
	def getChildren(category_id: Int): Future[Seq[Category]] = {
		val generalPromise = Promise[Seq[Category]]
		val generalFuture = generalPromise.future

		// Lo importante siempre son los hijos!
		db.run(Categories.filter(_.parent_id === category_id).result).map { rawChildren =>
			generalPromise success rawChildren
		}

		generalFuture
	}

	/**
		* Gets the children of the given category in form of ComplexCategory objects
		* @param category_id
		* @return
		*/
	def getComplexChildren(category_id: Int): Future[Seq[ComplexCategory]] = {
		val p = Promise[Seq[ComplexCategory]]()

		db.run(Categories.filter(_.parent_id === category_id).map(_.id).result).map { ids =>
			val children: ListBuffer[ComplexCategory] = ListBuffer()
			val serialized = {
				var accum = Future{()}

				for (id <- ids) {
					accum = accum flatMap { _ =>
						get(id).map { category =>
							children += category
						}
					}
				}

				accum
			}

			// Completing the promise
			serialized.map { _ =>
				p success children
			}
		}

		p.future
	}

	/**
		* Returns the parents map from the given category
		* @param category_id
		* @return
		*/
	def getParents(category_id: Int): Future[Seq[ComplexCategory]] = {
		val p = Promise[Seq[ComplexCategory]]()

		def parent(id: Int, list: Seq[ComplexCategory] = Seq()): Unit = {
			get(id).map { category =>
				if (category.parent.nonEmpty) {
					parent(category.parent.get.id.get, list :+ category)
				} else {
					p success list :+ category
				}
			}
		}

		parent(category_id)

		p.future
	}

	/**
		* Updates a category.
		* @param category
		* @return
		*/
	def update(category: Category): Future[Unit] = {
		db.run(
			Categories.filter(_.id === category.id).update(category)
		).map { _ => () }
	}

	/**
		* Deletes a category.
		* @param category_id
		* @return
		*/
	def delete(category_id: Int): Future[Unit] = {
		db.run(
			Categories.filter(_.id === category_id).delete
		).map { _ => () }
	}

	/**
		* Checks if a category id exists.
		* @param category_id
		* @return
		*/
	def exists(category_id: Int): Future[Boolean] = {
		db.run(Categories.filter(_.id === category_id).result).map { case s => s.nonEmpty }
	}

	/**
		* Checks if a category with the given normalized name exists.
		* @param normalized_name
		* @return
		*/
	def exists(normalized_name: String): Future[Boolean] = {
		db.run(Categories.filter(_.normalized_url === normalized_name).result).map { case s => s.nonEmpty }
	}

	/**
		* Inserts a new category
		* @param category
		* @return
		*/
	def insert(category: Category): Future[Unit] = db.run(Categories += category).map { _ => () }

	class CategoriesTable(tag: Tag) extends Table[Category](tag, "category") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def lang_name_id = column[String]("lang_name_id")

		def parent_id = column[Int]("parent_id")

		def order = column[Int]("cat_order")

		def normalized_url = column[String]("normalized_url")

		def * = (id.?, lang_name_id, parent_id, order, normalized_url) <> ((Category.apply _).tupled, Category.unapply)
	}
}
