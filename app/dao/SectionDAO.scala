package dao

import javax.inject.Inject

import com.google.inject.ImplementedBy
import play.api.Logger
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.twirl.api.Html
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future, Promise}

/**
	* Created by Daniel on 3/05/16.
	*/
@ImplementedBy(classOf[SectionDAO])
trait ISectionDAO {
	def all: Future[Seq[Section]]
	def get(id: Int): Future[Section]
	def get(name: String): Future[Section]
	def list(length: Option[Int] = Some(10), from: Option[Int] = Some(0), order: String = "asc"): Future[Seq[Section]]
	def count: Future[Int]
	def updateOrder(id: Int, order: Int): Future[Unit]
	def exists(id: Int): Future[Boolean]
	def exists(name: String): Future[Boolean]
	def update(section: Section): Future[Unit]
	def delete(id: Int): Future[Unit]
	def insert(section: Section): Future[Unit]
	def empty: Future[Unit]
	def load_defaults: Future[Unit]
}

case class Section(id: Option[Int], var name: String, var html: Html, var order: Int = 0, var in_header: Boolean = false, is_widget: Boolean = false)

class SectionDAO @Inject()(
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with ISectionDAO {
	import driver.api._

	val logger = Logger(this.getClass)
	val Sections = TableQuery[SectionsTable]

	// Mapping from DateTime to Date
	implicit def string2html  =
		MappedColumnType.base[Html, String](
			{ html => html.toString },
			{ str => Html(str) }
		)

	/**
		* Returns all the sections
		* @return
		*/
	def all: Future[Seq[Section]] = db.run(Sections.result)

	/**
		* Returns the section with the given id
		* @param id
		* @return
		*/
	def get(id: Int): Future[Section] = db.run(Sections.filter(_.id === id).result.head)

	/**
		* Returns the section with the given name
		* @param name
		* @return
		*/
	def get(name: String): Future[Section] = db.run(Sections.filter(_.name === name).result.head)

	/**
		* Gets a list of sections of the given length starting from the given index ordered by asc or desc.
		* @param length
		* @param from
		* @param order
		* @return
		*/
	def list(length: Option[Int] = Some(10), from: Option[Int] = Some(0), order: String = "asc"): Future[Seq[Section]] = {
		db.run(
			Sections
				.drop(from.getOrElse(0))
				.take(length.getOrElse(10))
				.sortBy(p => if (order == "asc") p.id.asc else p.id.desc).result
		).map { sections =>
			sections
		}
	}

	/**
		* Gets the total number of sections.
		*/
	def count: Future[Int] = db.run(Sections.map(_.id).length.result)

	/**
		* Updates the order of the given section id
		* @param id
		* @param order
		* @return
		*/
	def updateOrder(id: Int, order: Int): Future[Unit] = db.run(Sections.filter(_.id === id).map(_.order).update(order)).map( _ => () )

	/**
		* Checks if a section with the given id exists
		* @param id
		* @return
		*/
	def exists(id: Int): Future[Boolean] = {
		db.run(Sections.filter(_.id === id).result).map { case s => s.nonEmpty }
	}

	/**
		* Checks if a section with the given name exists
		* @param name
		* @return
		*/
	def exists(name: String): Future[Boolean] = {
		db.run(Sections.filter(_.name === name).result).map { case s => s.nonEmpty }
	}

	/**
		* Updates the given section
		* @param section
		* @return
		*/
	def update(section: Section): Future[Unit] = db.run(Sections.filter(_.id === section.id.get).update(section)).map( _ => () )

	/**
		* Deletes the section with the given id
		* @param id
		* @return
		*/
	def delete(id: Int): Future[Unit] = db.run(Sections.filter(_.id === id).delete).map( _ => () )

	/**
		* Empty the table.
		* @return
		*/
	def empty: Future[Unit] = db.run(Sections.delete).map( _ => () )

	/**
		* Inserts a new section
		* @param section
		* @return
		*/
	def insert(section: Section): Future[Unit] = db.run(Sections += section).map { _ => () }

	/**
		* Load defaults for the table. Should be runned one time when configuring the application.
		* @return
		*/
	def load_defaults: Future[Unit] = {
		val p = Promise[Unit]()

		count.map { c =>
			if (c == 0) {
				val category_widget = new Section(None, "Categories", Html(""), 0, true, true)
				insert(category_widget).map { _ =>
					logger.info("Loading defaults.")
					p success Unit
				}
			} else {
				p success Unit
			}
		}

		p.future
	}

	/**
		* Table definition
		* @param tag
		*/
	class SectionsTable(tag: Tag) extends Table[Section](tag, "section") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def name = column[String]("name")

		def html = column[Html]("html")

		def order = column[Int]("order")

		def in_header = column[Boolean]("in_header")

		def is_widget = column[Boolean]("is_widget")

		def * = (id.?, name, html, order, in_header, is_widget) <> (Section.tupled, Section.unapply)
	}

}
