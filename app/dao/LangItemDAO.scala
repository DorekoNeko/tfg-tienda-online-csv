package dao

import javax.inject._

import _root_.util.Hashing
import com.google.inject.ImplementedBy
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.functional.syntax._
import play.api.libs.json._
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future, Promise}

/**
	* Created by Daniel on 18/04/16.
	*/
@ImplementedBy(classOf[LangItemDAO])
trait ILangItemDAO {
	def all: Future[Seq[LangItem]]
	def get(lang_id: String): Future[Lang]
	def lastLangIndex: Future[Int]
	def generateHash: Future[String]
	def update(langItem: LangItem): Future[Unit]
	def delete(lang_id: String): Future[Unit]
	def exists(hash: String): Future[Boolean]
	def insert(langItem: LangItem): Future[Unit]
	def insertAndGetHash(langItem: LangItem): Future[String]
}

case class LangItem(id: Option[Int] = None, lang_id: String, lang_acronym: String, var value: String)
case class Lang(lang_id: String, items: Seq[LangItem]) {
	def default: LangItem = {
		val default = "en"
		items.find( i => i.lang_acronym.toLowerCase == default).getOrElse(items.head)
	}

	override def toString = default.value
}

object LangItem {
	implicit val langItemFormat: Format[LangItem] = Json.format[LangItem]
}

object Lang {
	implicit lazy val langReads: Reads[Lang] = (
		(JsPath \ "id").read[String] and
			(JsPath \ "items").read[Seq[LangItem]]
		)(Lang.apply _)

	implicit lazy val langWrites: Writes[Lang] = (
		(JsPath \ "id").write[String] and
			(JsPath \ "items").write[Seq[LangItem]]
		)(unlift(Lang.unapply))

	implicit val langFormat: Format[Lang] = Format(langReads, langWrites)
}

class LangItemDAO @Inject()(
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with ILangItemDAO {
	import driver.api._

	val LangItems = TableQuery[LangItemsTable]

	/**
		* Returns all the lang items.
		* @return
		*/
	def all: Future[Seq[LangItem]] = db.run(LangItems.result)

	/**
		* Gets the Lang object with the given lang id.
		* @param lang_id
		* @return
		*/
	def get(lang_id: String): Future[Lang] = {
		val p = Promise[Lang]
		val f = p.future

		db.run(LangItems.filter(_.lang_id === lang_id).result).map { items =>
			p success new Lang(lang_id, items)
		}

		f
	}

	/**
		* Generates a new unique hash
		* @return
		*/
	def generateHash: Future[String] = {
		val p = Promise[String]()

		def iterate(hash: String): Unit = {
			exists(hash).map { exists =>
				if (exists) {
					iterate(Hashing.randomAlphaNumericString(8))
				} else {
					p success hash
				}
			}
		}

		iterate(Hashing.randomAlphaNumericString(8))

		p.future
	}

	/**
		* Gets the last lang index.
		* @return
		*/
	@Deprecated
	def lastLangIndex: Future[Int] = {
		val p = Promise[Int]
		val f = p.future

		db.run(LangItems.length.result).map { count =>
			// If there are no langs, then return 0
			if (count == 0) {
				p success count
			} else {
				db.run(LangItems.sortBy(_.lang_id.desc).map(_.id).result.head).map { l => p success l }
			}
		}

		f
	}

	/**
		* Updates a lang item.
		* @param langItem
		* @return
		*/
	def update(langItem: LangItem): Future[Unit] = {
		db.run(
			LangItems.filter(_.id === langItem.id).update(langItem)
		).map { _ => () }
	}

	/**
		* Deletes all the lang items with the given lang id.
		* @param lang_id
		* @return
		*/
	def delete(lang_id: String): Future[Unit] = {
		db.run(
			LangItems.filter(_.lang_id === lang_id).delete
		).map { _ => () }
	}

	/**
		* Checks a conversation with the given hash already exists
		* @param hash
		* @return
		*/
	def exists(hash: String): Future[Boolean] = db.run(LangItems.filter(_.lang_id === hash).length.result).map { l => if (l > 0) true else false }

	def insert(langItem: LangItem): Future[Unit] = {
		val p = Promise[Unit]()

		if (langItem.lang_id.isEmpty) {
			generateHash.map { hash =>
				db.run(
					LangItems += new LangItem(None, hash, langItem.lang_acronym, langItem.value)
				).map( _ => p success Unit )
			}
		} else {
			db.run(LangItems += langItem).map { _ => p success Unit }
		}

		p.future
	}

	/**
		* Inserts a new LangItem and returns it hash
		* @param langItem
		* @return
		*/
	def insertAndGetHash(langItem: LangItem): Future[String] = {
		val p = Promise[String]()

		if (langItem.lang_id.isEmpty) {
			generateHash.map { hash =>
				db.run(
					LangItems += new LangItem(None, hash, langItem.lang_acronym, langItem.value)
				).map( _ => p success hash )
			}
		} else {
			db.run(LangItems += langItem).map { _ => p success langItem.lang_id }
		}

		p.future
	}

	class LangItemsTable(tag: Tag) extends Table[LangItem](tag, "lang") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def lang_id = column[String]("lang_id")

		def acronym = column[String]("lang")

		def value = column[String]("value")

		def * = (id.?, lang_id, acronym, value) <> ((LangItem.apply _).tupled, LangItem.unapply)
	}
}
