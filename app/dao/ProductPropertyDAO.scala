package dao

import javax.inject._

import com.google.inject.ImplementedBy
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.json.{Format, Json}
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future, Promise}

/**
	* Created by Daniel on 18/04/16.
	*/
@ImplementedBy(classOf[ProductPropertyDAO])
trait IProductPropertyDAO {
	def insertProperty(propertyRaw: ProductProperty): Future[Unit]
	def insertValue(valueRaw: PropertyValue): Future[Unit]
	def lastIndex: Future[Int]
	def deleteAll(product_id: Int): Future[Unit]
}

case class ProductProperty(id: Option[Int] = None, product_id: Int, lang_name: String, input_method: String, with_images: Boolean)
case class PropertyValue(id: Option[Int] = None, property_id: Int, lang_value_id: String, image_link: String, order: Byte)
case class ComplexProductProperty(id: Option[Int] = None, product_id: Int, name: Lang, input_method: String, with_images: Boolean, values: Seq[PropertyValue])
case class ComplexPropertyValue(id: Option[Int] = None, property_id: Int, value: Lang, image_link: String, order: Byte)

object ProductProperty {
	implicit val productPropertyFormat: Format[ProductProperty] = Json.format[ProductProperty]
}

class ProductPropertyDAO @Inject()(
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with IProductPropertyDAO {
	import driver.api._

	val ProductPropertiesRaw = TableQuery[ProductPropertiesRawTable]
	val PropertyValuesRaw = TableQuery[PropertyValuesTable]

	def insertProperty(propertyRaw: ProductProperty): Future[Unit] = db.run(ProductPropertiesRaw += propertyRaw).map { _ => () }

	def insertValue(valueRaw: PropertyValue): Future[Unit] = db.run(PropertyValuesRaw += valueRaw).map { _ => () }

	/**
		* Gets the last property index.
		* @return
		*/
	def lastIndex: Future[Int] = {
		val p = Promise[Int]
		val f = p.future

		db.run(ProductPropertiesRaw.sortBy(_.id.desc).result.head).map { l => p success l.id.get }

		f
	}

	def deleteAll(product_id: Int): Future[Unit] = {
		val p = Promise[Unit]
		val f = p.future

		db.run(ProductPropertiesRaw.filter(_.product_id === product_id).result).map { properties =>
			val serialized = {
				var accum = Future{()}

				for (property <- properties) {
					accum = accum flatMap { _ =>
						db.run(PropertyValuesRaw.filter(_.property_id === property.id.get).delete).map { _ => () }
					}
				}

				accum
			}

			serialized.map { _ =>
				db.run(ProductPropertiesRaw.filter(_.product_id === product_id).delete).map { _ =>
					p success Unit
				}
			}
		}

		f
	}

	class ProductPropertiesRawTable(tag: Tag) extends Table[ProductProperty](tag, "property") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def product_id = column[Int]("product_id")

		def lang_name_id = column[String]("lang_name")

		def input_method = column[String]("input_method")

		def with_images = column[Boolean]("with_images")

		def * = (id.?, product_id, lang_name_id, input_method, with_images) <> ((ProductProperty.apply _).tupled, ProductProperty.unapply)
	}

	class PropertyValuesTable(tag: Tag) extends Table[PropertyValue](tag, "property_values") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def property_id = column[Int]("property_id")

		def lang_value_id = column[String]("lang_value_id")

		def image_link = column[String]("image_link")

		def order = column[Byte]("order")

		def * = (id.?, property_id, lang_value_id, image_link, order) <> (PropertyValue.tupled, PropertyValue.unapply _)
	}
}
