package dao

import javax.inject._

import com.google.inject.ImplementedBy
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.json.{Format, Json}
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
	* Created by Daniel on 18/04/16.
	*/
@ImplementedBy(classOf[AddressDAO])
trait IAddressDAO {
	def all: Future[Seq[Address]]
	def get(id: Int): Future[Address]
	def getUserAddresses(user_id: Int): Future[Seq[Address]]
	def update(address: Address): Future[Unit]
	def delete(id: Int): Future[Unit]
	def exists(id: Int): Future[Boolean]
	def exists(name: String): Future[Boolean]
	def insert(address: Address): Future[Unit]
}

case class Address(id: Option[Int] = None, user_id: Int, var name: String, var address: String, var postal_code: String, var province: String, var population: String, isDefault: Boolean = false)

object Address {
	implicit val addressFormat: Format[Address] = Json.format[Address]
}

class AddressDAO @Inject()(
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with IAddressDAO {
	import driver.api._

	val Addresses = TableQuery[AddressesTable]

	/**
		* Returns all the addresses
		* @return
		*/
	def all: Future[Seq[Address]] = db.run(Addresses.result)

	/**
		* Gets the user address with the given id
		* @param id
		* @return
		*/
	def get(id: Int): Future[Address] = db.run(Addresses.filter(_.id === id).result.head)

	/**
		* Gets all the user addresses from the given user
		* @param user_id
		* @return
		*/
	def getUserAddresses(user_id: Int): Future[Seq[Address]] = db.run(Addresses.filter(_.user_id === user_id).result)

	/**
		* updates the given address
		* @param address
		* @return
		*/
	def update(address: Address): Future[Unit] = {
		db.run(
			Addresses.filter(_.id === address.id).update(address)
		).map { _ => () }
	}

	/**
		* deletes the given address
		* @param id
		* @return
		*/
	def delete(id: Int): Future[Unit] = {
		db.run(
			Addresses.filter(_.id === id).delete
		).map { _ => () }
	}

	/**
		* Checks if the address name exists
		* @param name
		* @return
		*/
	def exists(name: String): Future[Boolean] = {
		db.run(Addresses.filter(_.name === name).result).map { case s => s.nonEmpty }
	}

	/**
		* Checks if the address exists
		* @param id
		* @return
		*/
	def exists(id: Int): Future[Boolean] = {
		db.run(Addresses.filter(_.id === id).result).map { case s => s.nonEmpty }
	}

	/**
		* Insert a new address
		* @param address
		* @return
		*/
	def insert(address: Address): Future[Unit] = db.run(Addresses += address).map { _ => () }

	// Table
	class AddressesTable(tag: Tag) extends Table[Address](tag, "user_addresses") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def user_id = column[Int]("user_id")

		def name = column[String]("name")

		def address = column[String]("address")

		def postal_code = column[String]("postal_code")

		def province = column[String]("province")

		def population = column[String]("population")

		def default = column[Boolean]("is_default")

		def * = (id.?, user_id, name, address, postal_code, province, population, default) <> ((Address.apply _).tupled, Address.unapply _)
	}
}
