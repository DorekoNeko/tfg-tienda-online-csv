package dao

import javax.inject.Inject

import com.google.inject.ImplementedBy
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, JsPath, Reads, Writes}
import slick.driver.JdbcProfile

import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Future, Promise}

/**
	* Created by Daniel on 24/05/16.
	*/
@ImplementedBy(classOf[CartDAO])
trait ICartDAO {
	def get(user_id: Int): Future[Cart]
	def getItems(user_id: Int): Future[Seq[CartItem]]
	def count(user_id: Int): Future[Int]
	def updateItem(item: CartItem): Future[Unit]
	def deleteUserItem(user_id: Int, product_id: Int): Future[Unit]
	def empty(user_id: Int): Future[Unit]
	def insert(item: CartItem): Future[Unit]
}

case class CartItem(id: Option[Int], user_id: Int, product_id: Int, var quantity: Int)
case class ComplexCartItem(id: Option[Int], user_id: Int, product: ComplexProduct, quantity: Int)
case class Cart(user: User, items: Seq[ComplexCartItem])

object ComplexCartItem {
	implicit lazy val complexCartItemReads: Reads[ComplexCartItem] = (
		(JsPath \ "id").readNullable[Int] and
			(JsPath \ "user_id").read[Int] and
			(JsPath \ "product").read[ComplexProduct] and
			(JsPath \ "quantity").read[Int]
		)(ComplexCartItem.apply _)

	implicit lazy val complexCartItemWrites: Writes[ComplexCartItem] = (
		(JsPath \ "id").writeNullable[Int] and
			(JsPath \ "user_id").write[Int] and
			(JsPath \ "product").write[ComplexProduct] and
			(JsPath \ "quantity").write[Int]
		)(unlift(ComplexCartItem.unapply))

	implicit val complexCartItemFormat: Format[ComplexCartItem] = Format(complexCartItemReads, complexCartItemWrites)
}

object Cart {
	implicit lazy val cartReads: Reads[Cart] = (
		(JsPath \ "user").read[User] and
			(JsPath \ "items").read[Seq[ComplexCartItem]]
		)(Cart.apply _)

	implicit lazy val cartWrites: Writes[Cart] = (
		(JsPath \ "user").write[User] and
			(JsPath \ "items").write[Seq[ComplexCartItem]]
		)(unlift(Cart.unapply))

	implicit val cartFormat: Format[Cart] = Format(cartReads, cartWrites)
}

class CartDAO @Inject()(
	protected val userDAO: IUserDAO,
	protected val productDAO: IProductDAO,
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with ICartDAO {
	import driver.api._

	val CartItems = TableQuery[CartsTable]

	/**
		* Gets the cart for the given user
		* @param user_id
		* @return
		*/
	def get(user_id: Int): Future[Cart] = {
		val p = Promise[Cart]

		db.run(CartItems.filter(_.user_id === user_id).result).map { items =>
			val complexItems: ListBuffer[ComplexCartItem] = ListBuffer()
			val serialized = {
				var accum = Future{()}

				for (item <- items) {
					accum = accum flatMap { _ =>
						productDAO.getComplex(item.product_id).map { product =>
							complexItems += new ComplexCartItem(item.id, item.user_id, product, item.quantity)
						}
					}
				}

				accum
			}

			serialized.map { _ =>
				userDAO.get(user_id).map { user =>
					p success new Cart(user, complexItems.toSeq)
				}
			}
		}

		p.future
	}

	/**
		* Return a list of cart items of this user
		* @param user_id
		* @return
		*/
	def getItems(user_id: Int): Future[Seq[CartItem]] = db.run(CartItems.filter(_.user_id === user_id).result)

	/**
		* Gets the total number of cart items.
		*/
	def count(user_id: Int): Future[Int] = db.run(CartItems.filter(_.user_id === user_id).map(_.id).length.result)

	/**
		* Updates the given item
		* @param item
		* @return
		*/
	def updateItem(item: CartItem): Future[Unit] = db.run(CartItems.filter(_.id === item.id).update(item)).map( _ => () )

	/**
		* Deletes a cart item with the given user id and product id
		* @param user_id
		* @param product_id
		* @return
		*/
	def deleteUserItem(user_id: Int, product_id: Int): Future[Unit] = {
		db.run(
			CartItems
				.filter(i => i.user_id === user_id && i.product_id === product_id)
				.delete
		).map( _ => () )
	}

	/**
		* Empty the given user cart
		* @param user_id
		* @return
		*/
	def empty(user_id: Int): Future[Unit] = db.run(CartItems.filter(_.user_id === user_id).delete).map( _ => () )

	/**
		* Inserts a new cart item
		* @param item
		* @return
		*/
	def insert(item: CartItem): Future[Unit] = db.run(CartItems += item).map( _ => () )

	/**
		* Cart table definition
		* @param tag
		*/
	class CartsTable(tag: Tag) extends Table[CartItem](tag, "cart") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def user_id = column[Int]("user_id")

		def product_id = column[Int]("product_id")

		def quantity = column[Int]("quantity")

		def * = (id.?, user_id, product_id, quantity) <> (CartItem.tupled, CartItem.unapply)
	}
}
