package dao

import javax.inject._

import com.google.inject.ImplementedBy
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.json.{Format, Json}
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
	* Created by Daniel on 18/04/16.
	*/
@ImplementedBy(classOf[LanguageDAO])
trait ILanguageDAO {
	def all: Future[Seq[Language]]
	def get(id: Int): Future[Language]
	def exists(id: Int): Future[Boolean]
}

case class Language(id: Option[Int] = None, name: String, short: String)

object Language {
	implicit val languagesFormat: Format[Language] = Json.format[Language]
}

class LanguageDAO @Inject()(
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with ILanguageDAO {
	import driver.api._

	val Languages = TableQuery[LanguagesTable]

	/**
		* Gets all the languages
		* @return
		*/
	def all: Future[Seq[Language]] = db.run(Languages.result)

	/**
		* Gets the language with the given id
		* @param id
		* @return
		*/
	def get(id: Int): Future[Language] = db.run(Languages.filter(_.id === id).result.head)

	/**
		* Checks if the language with the given id exists
		* @param id
		* @return
		*/
	def exists(id: Int): Future[Boolean] = db.run(Languages.filter(_.id === id).result).map { case s => s.nonEmpty }

	class LanguagesTable(tag: Tag) extends Table[Language](tag, "languages") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def name = column[String]("name")

		def short = column[String]("short")

		def * = (id.?, name, short) <>((Language.apply _).tupled, Language.unapply _)
	}
}
