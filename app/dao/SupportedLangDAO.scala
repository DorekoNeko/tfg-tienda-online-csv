package dao

import javax.inject._

import com.google.inject.ImplementedBy
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
	* Created by Daniel on 18/04/16.
	*/
@ImplementedBy(classOf[SupportedLangDAO])
trait ISupportedLangDAO {
	def all: Future[Seq[SupportedLang]]
	def get(id: Int): Future[SupportedLang]
	def getDefault: Future[SupportedLang]
	def exists(id: Int): Future[Boolean]
	def update(supportedLang: SupportedLang): Future[Unit]
	def delete(id: Int): Future[Unit]
	def insert(supportedLanguage: SupportedLang): Future[Unit]
}

case class SupportedLang(id: Option[Int] = None, language_id: Int, short: String, name: String, is_default: Boolean = false)

class SupportedLangDAO @Inject()(
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with ISupportedLangDAO {
	import driver.api._

	val SupportedLangs = TableQuery[SupportedLangsTable]

	/**
		* Returns all supported languages.
		* @return
		*/
	def all: Future[Seq[SupportedLang]] = db.run(SupportedLangs.result)

	/**
		* Gets the supported lang object with the given id.
		* @param id
		* @return
		*/
	def get(id: Int): Future[SupportedLang] = db.run(SupportedLangs.filter(_.id === id).result.head)

	/**
		* Gets the default supported lang object.
		* @return
		*/
	def getDefault: Future[SupportedLang] = db.run(SupportedLangs.filter(_.default === true).result.head)

	/**
		* Updates a supported lang.
		* @param supportedLang
		* @return
		*/
	def update(supportedLang: SupportedLang): Future[Unit] = {
		db.run(
			SupportedLangs.filter(_.id === supportedLang.id).update(supportedLang)
		).map { _ => () }
	}

	/**
		* Deletes all the supported languages with the given id.
		* @param id
		* @return
		*/
	def delete(id: Int): Future[Unit] = {
		db.run(
			SupportedLangs.filter(_.id === id).delete
		).map { _ => () }
	}

	/**
		* Checks if a supported language exists.
		* @param id
		* @return
		*/
	def exists(id: Int): Future[Boolean] = {
		db.run(SupportedLangs.filter(_.id === id).result).map { case s => s.nonEmpty }
	}

	def insert(supportedLanguage: SupportedLang): Future[Unit] = db.run(SupportedLangs += supportedLanguage).map { _ => () }

	class SupportedLangsTable(tag: Tag) extends Table[SupportedLang](tag, "supported_languages") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def language_id = column[Int]("language_id")

		def short = column[String]("short")

		def name = column[String]("name")

		def default = column[Boolean]("is_default")

		def * = (id.?, language_id, short, name, default) <> (SupportedLang.tupled, SupportedLang.unapply _)
	}
}
