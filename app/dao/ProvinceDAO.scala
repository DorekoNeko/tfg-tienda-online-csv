package dao

import javax.inject._

import com.google.inject.ImplementedBy
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, JsPath, Reads, Writes}
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
	* Created by Daniel on 18/04/16.
	*/
@ImplementedBy(classOf[ProvinceDAO])
trait IProvinceDAO {
	def all: Future[Seq[Province]]
	def get(id: Int): Future[Province]
	def exists(name: String): Future[Boolean]
	def exists(id: Int): Future[Boolean]
}

case class Province(id: Int, name: String, name_seo: String, acronym: String)

object Province {
	implicit lazy val provinceReads: Reads[Province] = (
		(JsPath \ "id").read[Int] and
			(JsPath \ "name").read[String] and
			(JsPath \ "name_seo").read[String] and
			(JsPath \ "acronym").read[String]
		)(Province.apply _)

	implicit lazy val provinceWrites: Writes[Province] = (
		(JsPath \ "id").write[Int] and
			(JsPath \ "name").write[String] and
			(JsPath \ "name_seo").write[String] and
			(JsPath \ "acronym").write[String]
		)(unlift(Province.unapply))

	implicit val provinceFormat: Format[Province] = Format(provinceReads, provinceWrites)
}

class ProvinceDAO @Inject()(
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with IProvinceDAO {
	import driver.api._

	val Provinces = TableQuery[ProvincesTable]

	/**
		* Returns all the provinces
		* @return
		*/
	def all: Future[Seq[Province]] = db.run(Provinces.result)

	/**
		* Gets the province with the given id
		* @param id
		* @return
		*/
	def get(id: Int): Future[Province] = db.run(Provinces.filter(_.id === id).result.head)

	/**
		* Checks if the province with the given name exists
		* @param name
		* @return
		*/
	def exists(name: String): Future[Boolean] = {
		db.run(Provinces.filter(_.name === name).result).map { case s => s.nonEmpty }
	}

	/**
		* Checks if the province with the given id exists
		* @param id
		* @return
		*/
	def exists(id: Int): Future[Boolean] = {
		db.run(Provinces.filter(_.id === id).result).map { case s => s.nonEmpty }
	}

	// Table
	class ProvincesTable(tag: Tag) extends Table[Province](tag, "provincia") {

		def id = column[Int]("idprovincia", O.PrimaryKey, O.AutoInc)

		def name = column[String]("provincia")

		def name_seo = column[String]("provinciaseo")

		def acronym = column[String]("provincia3")

		def * = (id, name, name_seo, acronym) <> ((Province.apply _).tupled, Province.unapply _)
	}
}
