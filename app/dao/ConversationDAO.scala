package dao

import java.sql._
import javax.inject._

import com.google.inject.ImplementedBy
import org.joda.time._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import util.Hashing

import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Future, Promise}

/**
	* Created by Daniel on 22/05/16.
	*/
@ImplementedBy(classOf[ConversationDAO])
trait IConversationDAO {
	def list(length: Option[Int] = Some(10), from: Option[Int] = Some(0), order: String = "asc"): Future[Seq[Conversation]]
	def generateHash: Future[String]
	def getConversation(hash: String): Future[Conversation]
	def getComplexConversation(hash: String): Future[ComplexConversation]
	def getUserConversations(user_id: Int): Future[Seq[Conversation]]
	def getMessages(hash: String): Future[Seq[Message]]
	def getMessagesCount(hash: String): Future[Int]
	def count: Future[Int]
	def insertConversation(conversation: Conversation): Future[Unit]
	def insertMessage(message: Message): Future[Unit]
	def deleteConversation(hash: String): Future[Unit]
	def exists(hash: String): Future[Boolean]
}

case class Conversation(id: Option[Int], title: String, created_on: DateTime = DateTime.now(), last_activity_on: DateTime = DateTime.now(), hash: String = "")
case class ComplexConversation(id: Option[Int], hash: String, title: String, created_on: DateTime, var last_activity_on: DateTime, var messages: Seq[ComplexMessage] = Seq())
case class Message(id: Option[Int], conversation_hash: String, message: String, timestamp: DateTime = DateTime.now(), user_id: Int)
case class ComplexMessage(id: Option[Int], conversation_hash: String, message: String, timestamp: DateTime = DateTime.now(), user: User)

class ConversationDAO @Inject()(
	protected val userDAO: IUserDAO,
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with IConversationDAO {
	import driver.api._

	val Conversations = TableQuery[ConversationsTable]
	val Messages = TableQuery[MessagesTable]

	object Mapper {
		// Mapping from DateTime to Timestamp
		implicit def timestamp2DateTime  =
			MappedColumnType.base[DateTime, Timestamp](
				{ dateTime => new Timestamp(dateTime.getMillis) },
				{ timestamp => new DateTime(timestamp.getTime) }
			)
	}

	import Mapper._

	/**
		* Gets a list of conversations of the given length starting from the given index ordered by asc or desc.
		* @param length
		* @param from
		* @param order
		* @return
		*/
	def list(length: Option[Int] = Some(10), from: Option[Int] = Some(0), order: String = "desc"): Future[Seq[Conversation]] = {
		db.run(
			Conversations
				.sortBy(p => if (order == "asc") p.id.asc else p.id.desc)
				.drop(from.getOrElse(0))
				.take(length.getOrElse(10))
				.result
		)
	}

	/**
		* Generates a new unique hash
		* @return
		*/
	def generateHash: Future[String] = {
		val p = Promise[String]()

		def iterate(hash: String): Unit = {
			exists(hash).map { exists =>
				if (exists) {
					iterate(Hashing.randomAlphaNumericString(32))
				} else {
					p success hash
				}
			}
		}

		iterate(Hashing.randomAlphaNumericString(32))

		p.future
	}

	/**
		* Returns the conversation with the given hash
		* @param hash
		* @return
		*/
	def getConversation(hash: String): Future[Conversation] = db.run(Conversations.filter(_.hash === hash).result.head)

	/**
		* Returns the ComplexConversation with the given hash
		* @param hash
		* @return
		*/
	def getComplexConversation(hash: String): Future[ComplexConversation] = {
		val p = Promise[ComplexConversation]()

		getConversation(hash).map { conversation =>
			val complexConversation = new ComplexConversation(conversation.id, conversation.hash, conversation.title, conversation.created_on, conversation.last_activity_on)
			getMessages(hash).map { messages =>
				val users: ListBuffer[User] = ListBuffer()
				val serial = {
					var accum = Future{()}

					for (message <- messages) {
						accum = accum flatMap { _ =>
							val pi = Promise[Unit]()

							if (!users.exists(_.id.get == message.user_id)) {
								userDAO.get(message.user_id).map { user =>
									users += user
									pi success Unit
								}
							} else {
								pi success Unit
							}

							pi.future
						}
					}

					accum
				}

				serial.map { _ =>
					val complexMessages: ListBuffer[ComplexMessage] = ListBuffer()
					for (message <- messages) {
						complexMessages += new ComplexMessage(message.id, message.conversation_hash, message.message, message.timestamp, users.find(_.id.get == message.user_id).get)
					}

					complexConversation.messages = complexMessages

					p success complexConversation
				}
			}
		}

		p.future
	}

	/**
		* Returns all the conversations of the given user
		* @param user_id
		* @return
		*/
	def getUserConversations(user_id: Int): Future[Seq[Conversation]] = {
		val p = Promise[Seq[Conversation]]()

		db.run(Messages.filter(_.user_id === user_id).map(_.conversation_hash).groupBy(x => x).map(_._1).result).map { hashes =>
			val conversations: ListBuffer[Conversation] = ListBuffer()
			val serialized = {
				var accum = Future{()}

				for (hash <- hashes) {
					accum = accum flatMap { _ =>
						getConversation(hash).map { conversation =>
							conversations += conversation
						}
					}
				}

				accum
			}

			serialized.map { _ =>
				p success conversations.toSeq.sortBy(-_.created_on.getMillis)
			}
		}

		p.future
	}

	/**
		* Returns all messages with the given conversation hash
		* @param hash
		* @return
		*/
	def getMessages(hash: String): Future[Seq[Message]] = db.run(Messages.filter(_.conversation_hash === hash).result)

	/**
		* Returns the messages count in the given conversation
		* @param hash
		* @return
		*/
	def getMessagesCount(hash: String): Future[Int] = db.run(Messages.filter(_.conversation_hash === hash).length.result)

	/**
		* Gets the total number of conversations.
		*/
	def count: Future[Int] = db.run(Conversations.map(_.id).length.result)

	/**
		* Inserts a new conversation
		* @param conversation
		* @return
		*/
	def insertConversation(conversation: Conversation): Future[Unit] = {
		val p = Promise[Unit]()

		if (conversation.hash.isEmpty) {
			generateHash.map { hash =>
				db.run(
					Conversations += new Conversation(None, conversation.title, conversation.created_on, conversation.last_activity_on, hash)
				).map( _ => p success Unit )
			}
		} else {
			db.run(Conversations += conversation).map( _ => p success Unit )
		}

		p.future
	}

	/**
		* Inserts a new message
		* @param message
		* @return
		*/
	def insertMessage(message: Message): Future[Unit] = db.run(Messages += message).map( _ => () )

	/**
		* Checks a conversation with the given hash already exists
		* @param hash
		* @return
		*/
	def exists(hash: String): Future[Boolean] = db.run(Conversations.filter(_.hash === hash).length.result).map { l => if (l > 0) true else false }

	/**
		* Deletes the conversation with the given hash
		* @param hash
		* @return
		*/
	def deleteConversation(hash: String): Future[Unit] = db.run(Conversations.filter(_.hash === hash).delete).map( _ => () )

	/**
		* Conversations table definition
		* @param tag
		*/
	class ConversationsTable(tag: Tag) extends Table[Conversation](tag, "user_conversations") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def title = column[String]("title")

		def created_on = column[DateTime]("created_on")

		def last_activity_on = column[DateTime]("last_activity_on")

		def hash = column[String]("hash")

		def * = (id.?, title, created_on, last_activity_on, hash) <> (Conversation.tupled, Conversation.unapply)
	}

	/**
		* Messages table definition
		* @param tag
		*/
	class MessagesTable(tag: Tag) extends Table[Message](tag, "user_messages") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def conversation_hash = column[String]("conversation_hash")

		def user_id = column[Int]("user_id")

		def message = column[String]("message")

		def timestamp = column[DateTime]("timestamp")

		def * = (id.?, conversation_hash, message, timestamp, user_id) <> (Message.tupled, Message.unapply)
	}
}
