package dao

import javax.inject._

import com.google.inject.ImplementedBy
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future, Promise}

/**
	* Created by Daniel on 18/04/16.
	*/
@ImplementedBy(classOf[ShopSettingDAO])
trait IShopSettingDAO {
	def get(group: String): Future[Seq[Setting]]
	def update(setting: Setting): Future[Unit]
	def update(settings: List[Setting]): Future[Unit]
	def insert(setting: Setting): Future[Unit]
}

case class Setting(id: Option[Int] = None, key: String, var value: String, var value_boolean: Boolean, group: String)

class ShopSettingDAO @Inject()(
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with IShopSettingDAO {
	import driver.api._

	val Settings = TableQuery[SettingsTable]

	def get(group: String): Future[Seq[Setting]] = {
		val p = Promise[Seq[Setting]]
		val f = p.future

		db.run(Settings.filter(_.group === group).result).map { settings =>
			p success settings
		}

		f
	}

	def update(setting: Setting): Future[Unit] = {
		db.run(
			Settings.filter(_.id === setting.id).update(setting)
		).map { _ => () }
	}

	def update(settings: List[Setting]): Future[Unit] = {
		val p = Promise[Unit]
		val f = p.future

		var completed = 0
		for (setting <- settings) {
			update(setting).map { _ =>
				completed = completed + 1

				if (completed == settings.length) p success Unit
			}
		}

		f
	}

	/**
		* Inserts a new setting
		* @param setting
		* @return
		*/
	def insert(setting: Setting): Future[Unit] = db.run(Settings += setting).map { _ => () }

	class SettingsTable(tag: Tag) extends Table[Setting](tag, "settings") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def key = column[String]("key")

		def value = column[String]("value")

		def value_boolean = column[Boolean]("value_boolean")

		def group = column[String]("group")

		def * = (id.?, key, value, value_boolean, group) <> (Setting.tupled, Setting.unapply _)
	}
}
