package dao

import java.sql.Timestamp
import javax.inject.Inject

import com.google.inject.ImplementedBy
import org.joda.time.DateTime
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import util.Hashing

import scala.concurrent.{ExecutionContext, Future, Promise}

/**
	* Created by Daniel on 24/05/16.
	*/
@ImplementedBy(classOf[OrderDAO])
trait IOrderDAO {
	def generateHash: Future[String]
	def list(length: Option[Int] = Some(10), from: Option[Int] = Some(0), order: String = "asc"): Future[Seq[Order]]
	def getOrder(hash: String): Future[Order]
	def getOrders(user_id: Int): Future[Seq[Order]]
	def getItems(hash: String): Future[Seq[OrderItem]]
	def count: Future[Int]
	def updateOrder(order: Order): Future[Unit]
	def insertOrder(order: Order): Future[Unit]
	def insertOrderItem(orderItem: OrderItem): Future[Unit]
	def exists(hash: String): Future[Boolean]
}

case class Order(id: Option[Int], user_id: Int, timestamp: DateTime = DateTime.now(), var state: String = "preparing", hash: String, shipping_costs: Double, total_cost: Double, client_name: String, address: String, postal_code: String, province: String, population: String)
case class OrderItem(id: Option[Int], order_id: Int, quantity: Int, order_hash: String, name: String, price: Double, vat_percentage: Double)

class OrderDAO @Inject()(
	protected val userDAO: IUserDAO,
	protected val productDAO: IProductDAO,
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with IOrderDAO {
	import driver.api._

	val Orders = TableQuery[OrdersTable]
	val OrderItems = TableQuery[OrderItemsTable]

	object Mapper {
		// Mapping from DateTime to Timestamp
		implicit def timestamp2DateTime  =
			MappedColumnType.base[DateTime, Timestamp](
				{ dateTime => new Timestamp(dateTime.getMillis) },
				{ timestamp => new DateTime(timestamp.getTime) }
			)
	}

	import Mapper._

	/**
		* Generates a new unique hash
		* @return
		*/
	def generateHash: Future[String] = {
		val p = Promise[String]()

		def iterate(hash: String): Unit = {
			exists(hash).map { exists =>
				if (exists) {
					iterate(Hashing.randomNumeric(11))
				} else {
					p success hash
				}
			}
		}

		iterate(Hashing.randomNumeric(11))

		p.future
	}

	/**
		* Gets a list of orders of the given length starting from the given index ordered by asc or desc.
		* @param length
		* @param from
		* @param order
		* @return
		*/
	def list(length: Option[Int] = Some(10), from: Option[Int] = Some(0), order: String = "asc"): Future[Seq[Order]] = {
		db.run(
			Orders
				.sortBy(p => if (order == "asc") p.timestamp.asc else p.timestamp.desc)
				.drop(from.getOrElse(0))
				.take(length.getOrElse(10))
				.result
		)
	}

	/**
		* Returns the order with the given hash
		* @param hash
		* @return
		*/
	def getOrder(hash: String): Future[Order] = db.run(Orders.filter(_.hash === hash).result.head)

	/**
		* Returns al the orders of the given user
		* @param user_id
		* @return
		*/
	def getOrders(user_id: Int): Future[Seq[Order]] = db.run(Orders.filter(_.user_id === user_id).result)

	/**
		* Returns the order items of the given order
		* @param hash
		* @return
		*/
	def getItems(hash: String): Future[Seq[OrderItem]] = db.run(OrderItems.filter(_.order_hash === hash).result)

	/**
		* Checks if an order with the given hash already exists
		* @param hash
		* @return
		*/
	def exists(hash: String): Future[Boolean] = {
		db.run(Orders.filter(_.hash === hash).map(_.id).length.result).map { l =>
			if (l > 0) true else false
		}
	}

	/**
		* Gets the total number of orders.
		*/
	def count: Future[Int] = db.run(Orders.map(_.id).length.result)

	/**
		* Updates the given order
		* @param order
		* @return
		*/
	def updateOrder(order: Order): Future[Unit] = db.run(Orders.filter(_.id === order.id).update(order)).map( _ => () )

	/**
		* Inserts an order
		* @param order
		* @return
		*/
	def insertOrder(order: Order): Future[Unit] = db.run(Orders += order).map( _ => () )

	/**
		* Inserts an order item
		* @param orderItem
		* @return
		*/
	def insertOrderItem(orderItem: OrderItem): Future[Unit] = db.run(OrderItems += orderItem).map( _ => () )

	/**
		* Order table definition
		* @param tag
		*/
	class OrdersTable(tag: Tag) extends Table[Order](tag, "order") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def user_id = column[Int]("user_id")

		def timestamp = column[DateTime]("timestamp")

		def state = column[String]("state")

		def hash = column[String]("hash")

		def shipping_costs = column[Double]("shipping_costs")

		def total_cost = column[Double]("total_cost")

		def client_name = column[String]("client_name")

		def address = column[String]("address")

		def postal_code = column[String]("postal_code")

		def province = column[String]("province")

		def population = column[String]("population")

		def * = (id.?, user_id, timestamp, state, hash, shipping_costs, total_cost, client_name, address, postal_code, province, population) <> (Order.tupled, Order.unapply)
	}

	/**
		* Order items table definition
		* @param tag
		*/
	class OrderItemsTable(tag: Tag) extends Table[OrderItem](tag, "order_items") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def order_id = column[Int]("order_id")

		def quantity = column[Int]("quantity")

		def order_hash = column[String]("order_hash")

		def name = column[String]("name")

		def price = column[Double]("price")

		def vat_percentage = column[Double]("vat_percentage")

		def * = (id.?, order_id, quantity, order_hash, name, price, vat_percentage) <> (OrderItem.tupled, OrderItem.unapply)
	}
}
