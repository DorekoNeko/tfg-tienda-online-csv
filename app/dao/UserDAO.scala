package dao

import javax.inject._

import com.google.inject.ImplementedBy
import debug.AsyncTimer
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.functional.syntax._
import play.api.libs.json._
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future, Promise}

/**
	* Created by Daniel on 18/04/16.
	*/
@ImplementedBy(classOf[UserDAO])
trait IUserDAO {
	def all: Future[Seq[User]]
	def get(id: Int): Future[User]
	def get(username: String): Future[User]
	def isUsernameTaken(username: String): Future[Boolean]
	def authenticate(user: String, password: String): Future[Boolean]
	def update(user: User): Future[Unit]
	def delete(id: Int): Future[Unit]
	def exists(id: Int): Future[Boolean]
	def insert(user: User): Future[Unit]
}

case class User(id: Option[Int] = None, username: String, var password: String, var mail: String, var profile_picture: String, var name: String, var last_name: String, var province_id: Int, var population_id: Int, var DNI: String, var phone_number: String, var is_admin: Boolean)
case class ComplexUser(id: Option[Int] = None, username: String, password: String, mail: String, profile_picture: String, name: String, last_name: String, province: Province, population: Population, DNI: String, phone_number: String, addresses: Seq[Address], is_admin: Boolean)

object User {
	implicit val userFormat: Format[User] = Json.format[User]
}

object ComplexUser {
	implicit lazy val complexUserReads: Reads[ComplexUser] = (
		(JsPath \ "id").readNullable[Int] and
			(JsPath \ "username").read[String] and
			(JsPath \ "password").read[String] and
			(JsPath \ "mail").read[String] and
			(JsPath \ "profile_picture").read[String] and
			(JsPath \ "name").read[String] and
			(JsPath \ "last_name").read[String] and
			(JsPath \ "province").read[Province] and
			(JsPath \ "population").read[Population] and
			(JsPath \ "DNI").read[String] and
			(JsPath \ "phone_number").read[String] and
			(JsPath \ "addresses").read[Seq[Address]] and
			(JsPath \ "is_admin").read[Boolean]
		)(ComplexUser.apply _)

	implicit lazy val complexUserWrites: Writes[ComplexUser] = (
		(JsPath \ "id").writeNullable[Int] and
			(JsPath \ "username").write[String] and
			(JsPath \ "password").write[String] and
			(JsPath \ "mail").write[String] and
			(JsPath \ "profile_picture").write[String] and
			(JsPath \ "name").write[String] and
			(JsPath \ "last_name").write[String] and
			(JsPath \ "province").write[Province] and
			(JsPath \ "population").write[Population] and
			(JsPath \ "DNI").write[String] and
			(JsPath \ "phone_number").write[String] and
			(JsPath \ "addresses").write[Seq[Address]] and
			(JsPath \ "is_admin").write[Boolean]
		)(unlift(ComplexUser.unapply))

	implicit val complexUserFormat: Format[ComplexUser] = Format(complexUserReads, complexUserWrites)
}

class UserDAO @Inject()(
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with IUserDAO {
	import driver.api._

	val Users = TableQuery[UsersTable]

	/**
		* Returns all the users
		* @return
		*/
	def all: Future[Seq[User]] = {
		val timer = new AsyncTimer(this)
		db.run(Users.result).map { all =>
			timer.end
			all
		}
	}

	/**
		* Gets the user with the given id
		* @param id
		* @return
		*/
	def get(id: Int): Future[User] = {
		val timer = new AsyncTimer(this)
		db.run(Users.filter(_.id === id).result.head). map { user =>
			timer.end
			user
		}
	}

	/**
		* Gets the user with the given username
		* @param username
		* @return
		*/
	def get(username: String): Future[User] = {
		val p = Promise[User]()

		db.run(Users.filter(_.username === username).length.result).map { c =>
			if (c > 0) {
				db.run(Users.filter(_.username === username).result.head).map { user =>
					p success user
				}
			} else {
				db.run(Users.filter(_.mail === username).result.head).map { user =>
					p success user
				}
			}
		}

		p.future
	}

	/**
		* Checks if the given username is already in use
		* @param username
		* @return
		*/
	def isUsernameTaken(username: String): Future[Boolean] = {
		db.run(Users.filter(_.username === username).result).map { case s => s.nonEmpty }
	}

	/**
		* Checks if the given credentials match by username and/or email.
		* @param user
		* @param password
		* @return
		*/
	def authenticate(user: String, password: String): Future[Boolean] = {
		val p = Promise[Boolean]()

		val check = for {
			c1 <- db.run(Users.filter(r => r.username === user && r.password === password).length.result)
			c2 <- db.run(Users.filter(r => r.mail === user && r.password === password).length.result)
		} yield (c1, c2)

		check.map { t =>
			if (t._1 == 0) {
				// If no username match, return email matching result
				p.success(if (t._2 > 0) true else false)
			} else {
				// if username match, return the result
				p success true
			}
		}

		p.future
	}

	/**
		* Updates the user
		* @param user
		* @return
		*/
	def update(user: User): Future[Unit] = {
		db.run(
			Users.filter(_.id === user.id).update(user)
		).map { _ => () }
	}

	/**
		* Deletes the user with the given id
		* @param id
		* @return
		*/
	def delete(id: Int): Future[Unit] = {
		db.run(
			Users.filter(_.id === id).delete
		).map { _ => () }
	}

	/**
		* Checks if a user with the given id exists
		* @param id
		* @return
		*/
	def exists(id: Int): Future[Boolean] = {
		db.run(Users.filter(_.id === id).result).map { case s => s.nonEmpty }
	}

	/**
		* Inserts a new user
		* @param user
		* @return
		*/
	def insert(user: User): Future[Unit] = db.run(Users += user).map { _ => () }

	// Table
	class UsersTable(tag: Tag) extends Table[User](tag, "user") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def username = column[String]("user")

		def password = column[String]("passwd")

		def mail = column[String]("mail")

		def profile_picture = column[String]("profile_picture")

		def name = column[String]("name")

		def last_name = column[String]("last_name")

		def province_id = column[Int]("province_id")

		def population_id = column[Int]("population_id")

		def DNI = column[String]("dni")

		def phone_number = column[String]("phone_number")

		def is_admin = column[Boolean]("is_admin")

		def * = (id.?, username, password, mail, profile_picture, name, last_name, province_id, population_id, DNI, phone_number, is_admin) <> ((User.apply _).tupled, User.unapply _)
	}
}
