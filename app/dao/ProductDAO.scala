package dao

import java.sql.Date
import javax.inject._

import com.google.inject.ImplementedBy
import debug.AsyncTimer
import org.joda.time.DateTime
import play.api.Logger
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.functional.syntax._
import play.api.libs.json.{Format, JsPath, Reads, Writes}
import settings.IGeneralSettings
import slick.driver.JdbcProfile

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future, Promise}

/**
	* Created by Daniel on 18/04/16.
	*/
@ImplementedBy(classOf[ProductDAO])
trait IProductDAO {
	def all: Future[Seq[Product]]
	def allComplex: Future[Seq[ComplexProduct]]
	def list(length: Option[Int] = None, from: Option[Int] = None, order: String = "asc"): Future[Seq[ComplexProduct]]
	def listFromCategory(category_id: Int, length: Option[Int] = None, from: Option[Int] = None, order: String = "asc"): Future[Seq[ComplexProduct]]
	def newest(length: Int = 10): Future[Seq[ComplexProduct]]
	def search(query: String): Future[Seq[ComplexProduct]]
	def get(id: Int): Future[Product]
	def getComplex(id: Int): Future[ComplexProduct]
	def get(reference: String): Future[Product]
	def getComplex(reference: String): Future[ComplexProduct]
	def getProperties(rawProperties: Seq[ProductProperty]): Future[Seq[ComplexProductProperty]]
	def getProperty(property: ProductProperty): Future[ComplexProductProperty]
	def getPropertyValues(values: Seq[PropertyValue]): Future[Seq[ComplexPropertyValue]]
	def getPropertyValue(propertyValueRaw: PropertyValue): Future[ComplexPropertyValue]
	def getProductReviews(product_id: Int): Future[Seq[ComplexProductReview]]
	def getProductReviews(product_reference: String): Future[Seq[ComplexProductReview]]
	def lastIndex: Future[Int]
	def count: Future[Int]
	def countCategoryProducts(category_id: Int): Future[Int]
	def update(product: Product): Future[Unit]
	def delete(id: Int): Future[Unit]
	def erase: Future[Unit]
	def exists(id: Int): Future[Boolean]
	def exists(reference: String): Future[Boolean]
	def insert(product: Product): Future[Unit]
	def insertReview(review: ProductReview): Future[Unit]
}

case class Product(id: Option[Int] = None, var lang_name_id: String, var lang_description_id: String, var category_id: Int, var vat_id: Int, reference: String, var stock: Int, var in_offer: Boolean = false, var offer_use_percentage: Boolean = false, var discount: Double = 0, var offer_date_until: DateTime = DateTime.now(), var offer_use_date: Boolean = false, var price: Double)
case class ComplexProduct(id: Option[Int] = None, name: Lang, description: Lang, category: ComplexCategory, vat: VAT, reference: String, stock: Int, properties: Seq[ProductProperty], in_offer: Boolean, offer_use_percentage: Boolean, discount: Double, offer_date_until: DateTime, offer_use_date: Boolean, price: Double, gallery: Seq[GalleryItem]) {
	/**
		* Get the price with VAT applied
		* @return
		*/
	def priceWithVAT: Double = {
		price + (price * (vat.percentage / 100.0))
	}

	/**
		* Get the price with discounts applied
		* @return
		*/
	def priceWithDiscountsNoVAT: Double = {
		if (in_offer) {
			if (offer_use_date) {
				if (offer_date_until.isAfterNow) {
					if (offer_use_percentage) {
						if(price - (discount * price / 100.0) > 0) price - (discount * price / 100.0) else 0
					} else {
						if(price - discount > 0) price - discount else 0
					}
				} else {
					price
				}
			} else if (offer_use_percentage) {
				if(price - (discount * price / 100.0) > 0) price - (discount * price / 100.0) else 0
			} else {
				if(price - discount > 0) price - discount else 0
			}
		} else {
			price
		}
	}

	/**
		* Get the price with VAT and discounts applied if any
		* @return
		*/
	def calculatedPrice: Double = {
		if (in_offer) {
			if (offer_use_date) {
				if (offer_date_until.isAfterNow) {
					if (offer_use_percentage) {
						val discount_price = if(price - (discount * price / 100.0) > 0) price - (discount * price / 100.0) else 0
						withVAT(discount_price)
					} else {
						val discount_price = if(price - discount > 0) price - discount else 0
						withVAT(discount_price)
					}
				} else {
					val discount_price = if(price - discount > 0) price - discount else 0
					withVAT(discount_price)
				}
			} else if (offer_use_percentage) {
				val discount_price = if(price - (discount * price / 100.0) > 0) price - (discount * price / 100.0) else 0
				withVAT(discount_price)
			} else {
				val discount_price = if(price - discount > 0) price - discount else 0
				withVAT(discount_price)
			}
		} else {
			priceWithVAT
		}
	}

	/**
		* Gets the given number with the VAT applied
		* @param p
		* @return
		*/
	def withVAT(p: Double) = p + (p * (vat.percentage / 100.0))
}
case class ProductReview(id: Option[Int], product_id: Int, product_reference: String, user_id: Int, review: String, valoration: Int)
case class ComplexProductReview(id: Option[Int], product_id: Int, product_reference: String, user: User, review: String, valoration: Int)

object ComplexProduct {
	implicit lazy val complexProductReads: Reads[ComplexProduct] = (
		(JsPath \ "id").readNullable[Int] and
			(JsPath \ "name").read[Lang] and
			(JsPath \ "description").read[Lang] and
			(JsPath \ "category").read[ComplexCategory] and
			(JsPath \ "vat").read[VAT] and
			(JsPath \ "reference").read[String] and
			(JsPath \ "stock").read[Int] and
			(JsPath \ "properties").read[Seq[ProductProperty]] and
			(JsPath \ "in_offer").read[Boolean] and
			(JsPath \ "use_percentage").read[Boolean] and
			(JsPath \ "discount").read[Double] and
			(JsPath \ "offer_date_until").read[DateTime] and
			(JsPath \ "use_date").read[Boolean] and
			(JsPath \ "price").read[Double] and
			(JsPath \ "gallery").read[Seq[GalleryItem]]
		)(ComplexProduct.apply _)

	implicit lazy val complexProductWrites: Writes[ComplexProduct] = (
		(JsPath \ "id").writeNullable[Int] and
			(JsPath \ "name").write[Lang] and
			(JsPath \ "description").write[Lang] and
			(JsPath \ "category").write[ComplexCategory] and
			(JsPath \ "vat").write[VAT] and
			(JsPath \ "reference").write[String] and
			(JsPath \ "stock").write[Int] and
			(JsPath \ "properties").write[Seq[ProductProperty]] and
			(JsPath \ "in_offer").write[Boolean] and
			(JsPath \ "use_percentage").write[Boolean] and
			(JsPath \ "discount").write[Double] and
			(JsPath \ "offer_date_until").write[DateTime] and
			(JsPath \ "use_date").write[Boolean] and
			(JsPath \ "price").write[Double] and
			(JsPath \ "gallery").write[Seq[GalleryItem]]
		)(unlift(ComplexProduct.unapply))

	implicit val complexProductFormat: Format[ComplexProduct] = Format(complexProductReads, complexProductWrites)
}

class ProductDAO @Inject()(
	protected val dbConfigProvider: DatabaseConfigProvider,
	protected val settings: IGeneralSettings,
	protected val langItemDAO: ILangItemDAO,
	protected val userDAO: IUserDAO,
	protected val categoryDAO: ICategoryDAO,
	protected val vatDAO: IVATDAO,
	protected val productGalleryDAO: IProductGalleryDAO
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with IProductDAO {
	import driver.api._

	val Products = TableQuery[ProductsTable]
	val ProductReviews = TableQuery[ProductReviewsTable]
	val logger = Logger(this.getClass)

	// Mapping from DateTime to Date
	implicit def dateTime2date  =
		MappedColumnType.base[DateTime, Date](
			{ dateTime => new Date(dateTime.getMillis) },
			{ date => new DateTime(date.getTime) }
		)

	// Wait for the ShopSettings component to be available
	Await.ready(settings.initialize, 3.minutes)

	/**
		* Returns a list with all the products
		* @return
		*/
	def all: Future[Seq[Product]] = db.run(Products.result)

	/**
		* Returns a list with all the complex products
		* @return
		*/
	def allComplex: Future[Seq[ComplexProduct]] = {
		val p = Promise[Seq[ComplexProduct]]()
		val timer = new AsyncTimer(this)
		val products: ListBuffer[ComplexProduct] = ListBuffer()

		db.run(Products.map(_.id).result).map { ids =>
			val serialized = {
				var accum = Future {()}

				for (id <- ids) {
					accum = accum flatMap { _ =>
						getComplex(id).map { product =>
							products += product

							()
						}
					}
				}

				accum
			}

			serialized map { _ =>
				timer.end
				p success products.toSeq
			}
		}

		p.future
	}

	/**
		* Gets a list of products of the given length starting from the given index ordered by asc or desc.
		* @param length
		* @param from
		* @param order
		* @return
		*/
	def list(length: Option[Int] = None, from: Option[Int] = None, order: String = "asc"): Future[Seq[ComplexProduct]] = {
		val p = Promise[Seq[ComplexProduct]]
		val f = p.future
		val timer = new AsyncTimer(this.getClass + " - List")

		val products: ListBuffer[ComplexProduct] = ListBuffer()
		var quantity = length.getOrElse(10)

		db.run(
			Products
				.sortBy(p => if (order == "asc") p.id.asc else p.id.desc)
				.drop(from.getOrElse(0))
				.take(quantity)
				.map(p => p.id).result
		).map { ids =>
			if (ids.nonEmpty) {
				for (id <- ids) {
					getComplex(id).map { product =>
						products += product

						if (products.length == ids.length) {
							timer.end
							p success products.toSeq
						}
					}
				}
			} else {
				p success products.toSeq
			}
		}

		f
	}

	/**
		* Gets the n newest products
		* @return
		*/
	def newest(length: Int = 10): Future[Seq[ComplexProduct]] = {
		val p = Promise[Seq[ComplexProduct]]
		val timer = new AsyncTimer(this)
		val products: ListBuffer[ComplexProduct] = ListBuffer()

		db.run(
			Products
				.sortBy(p => p.id.desc)
				.take(length)
				.map(p => p.id).result
		).map { ids =>
			val serialized = {
				var accum = Future {()}

				for (id <- ids) {
					accum = accum flatMap { _ =>
						getComplex(id).map { product =>
							products += product
						}
					}
				}

				accum
			}

			serialized.map { _ =>
				timer.end
				p success products.toSeq
			}
		}

		p.future
	}

	/**
		* Searchs for products based on the query string
		* @param query
		* @return
		*/
	def search(query: String): Future[Seq[ComplexProduct]] = {
		val p = Promise[Seq[ComplexProduct]]()

		allComplex.map { products =>
			val list_score: ListBuffer[(ComplexProduct, Int)] = ListBuffer()
			for (product <- products) {
				val query_r = s"(?i)$query".r
				val name_matches = query_r.findAllIn(product.name.toString).length
				val description_matches = query_r.findAllIn(product.description.toString).length
				if (name_matches + description_matches > 0)
					list_score.+=((product, name_matches + description_matches))
			}

			// Returns a limit of 4 results ordered by match score
			p success list_score.sortBy(_._2).take(4).map(_._1)
		}

		p.future
	}

	/**
		* Gets the total number of products.
		*/
	def count: Future[Int] = db.run(Products.map(_.id).length.result)

	/**
		* Gets the product with the given id
		* @param id
		* @return
		*/
	def get(id: Int): Future[Product] = db.run(Products.filter(_.id === id).result.head)

	/**
		* Gets the ComplexProduct with the given id
		* @param id
		* @return
		*/
	def getComplex(id: Int): Future[ComplexProduct] = {
		val p = Promise[ComplexProduct]
		val f = p.future

		db.run(Products.filter(_.id === id).result.head).map { product =>
			val q = for {
				nameLang <- langItemDAO.get(product.lang_name_id)
				descriptionLang <- langItemDAO.get(product.lang_description_id)
				category <- categoryDAO.get(product.category_id)
				vat <- vatDAO.get(product.vat_id)
				// TODO: Rework product properties
				//rawProperties <- db.run(ProductProperty.ProductPropertiesRaw.filter(_.product_id === id).result)
				//properties <- getProperties(rawProperties)
				gallery <- productGalleryDAO.getGallery(id)
			} yield (nameLang, descriptionLang, category, vat, Seq(), gallery)

			q.map { t =>
				p success new ComplexProduct(product.id, t._1, t._2, t._3, t._4, product.reference, product.stock, t._5, product.in_offer, product.offer_use_percentage, product.discount, product.offer_date_until, product.offer_use_date, product.price, t._6)
			}
		}

		f
	}

	/**
		* Gets the product with the given reference
		* @param reference
		* @return
		*/
	def get(reference: String): Future[Product] = {
		for {
			raw <- db.run(Products.filter(_.reference === reference).result.head)
			product <- get(raw.id.get)
		} yield product
	}

	/**
		* Gets the ComplexProduct with the given reference
		* @param reference
		* @return
		*/
	def getComplex(reference: String): Future[ComplexProduct] = {
		for {
			raw <- db.run(Products.filter(_.reference === reference).result.head)
			product <- getComplex(raw.id.get)
		} yield product
	}

	/**
		* Gets a list of products within the given category of the given length
		* starting from the given index ordered by asc or desc.
		* @param length
		* @param from
		* @param order
		* @return
		*/
	def listFromCategory(category_id: Int, length: Option[Int] = None, from: Option[Int] = None, order: String = "asc"): Future[Seq[ComplexProduct]] = {
		val p = Promise[Seq[ComplexProduct]]
		val f = p.future
		val timer = new AsyncTimer(this.getClass + " - ListFromCategory")

		val products: ListBuffer[ComplexProduct] = ListBuffer()
		var quantity = length.getOrElse(settings.get("products_per_page").get.value.toInt)

		db.run(
			Products
				.filter(_.category_id === category_id)
				.drop(from.getOrElse(0))
				.take(quantity)
				.sortBy(p => if (order == "asc") p.id.asc else p.id.desc)
				.map(p => p.id).result
		).map { ids =>
			val serialized = {
				var accum = Future {()}

				for (id <- ids) {
					accum = accum flatMap { _ =>
						getComplex(id).map { product =>
							products += product
						}
					}
				}

				accum
			}

			serialized.map { _ =>
				timer.end
				p success products.toSeq
			}
		}

		f
	}

	/**
		* Gets the number of products within the given category
		* @param category_id
		* @return
		*/
	def countCategoryProducts(category_id: Int): Future[Int] = db.run(Products.filter(_.category_id === category_id).map(_.id).length.result)

	/**
		* Transforms the given review into a ComplexProductReview
		* @param review
		* @return
		*/
	def getComplexReview(review: ProductReview): Future[ComplexProductReview] = {
		val sp = Promise[ComplexProductReview]()

		userDAO.get(review.user_id).map { user =>
			sp success new ComplexProductReview(review.id, review.product_id, review.product_reference, user, review.review, review.valoration)
		}

		sp.future
	}

	/**
		* Gets the reviews from the given product id
		* @param product_id
		* @return
		*/
	def getProductReviews(product_id: Int): Future[Seq[ComplexProductReview]] = {
		val p = Promise[Seq[ComplexProductReview]]()

		val complex_reviews: ListBuffer[ComplexProductReview] = ListBuffer()
		db.run(ProductReviews.filter(_.product_id === product_id).result).map { reviews =>
			val serialized = {
				var accum = Future{()}

				for(review <- reviews) {
					accum = accum flatMap { _ =>
						getComplexReview(review).map { complex_review =>
							complex_reviews += complex_review
						}
					}
				}

				accum
			}

			serialized.map { _ =>
				p success complex_reviews
			}
		}

		p.future
	}

	/**
		* Gets the reviews from the given product reference
		* @param product_reference
		* @return
		*/
	def getProductReviews(product_reference: String): Future[Seq[ComplexProductReview]] = {
		val p = Promise[Seq[ComplexProductReview]]()

		val complex_reviews: ListBuffer[ComplexProductReview] = ListBuffer()
		db.run(ProductReviews.filter(_.product_reference === product_reference).sortBy(_.id.desc).take(10).result).map { reviews =>
			val serialized = {
				var accum = Future{()}

				for(review <- reviews) {
					accum = accum flatMap { _ =>
						getComplexReview(review).map { complex_review =>
							complex_reviews += complex_review
						}
					}
				}

				accum
			}

			serialized.map { _ =>
				p success complex_reviews
			}
		}

		p.future
	}

	/**
		* Inserts a new review
		* @param review
		* @return
		*/
	def insertReview(review: ProductReview): Future[Unit] = db.run(ProductReviews += review).map { _ => () }

	// TODO: Rework this
	def getProperties(rawProperties: Seq[ProductProperty]): Future[Seq[ComplexProductProperty]] = {
		val p = Promise[Seq[ComplexProductProperty]]
		val f = p.future

		def iterate(l: Seq[ProductProperty], fl: Seq[ComplexProductProperty] = Seq()): Unit = {
			if (l.isEmpty) {
				p success fl
			} else {
				if (l.length == fl.length) {
					p success fl
				} else {
					val a = l(fl.length)
					getProperty(a).map { property =>
						iterate(l, fl :+ property)
					}
				}
			}
		}

		iterate(rawProperties)

		f
	}

	/**
		* Gets the property with the given id filled with its values.
		* @param property
		* @return
		*/
	def getProperty(property: ProductProperty): Future[ComplexProductProperty] = {
		val p = Promise[ComplexProductProperty]
		val f = p.future

		val q = for {
			langName <- langItemDAO.get(property.lang_name)
			// TODO: Rework product properties
			//rawValues <- db.run(ProductProperty.PropertyValuesRaw.filter(_.property_id === property.id.get).result)
			//values <- getPropertyValues(rawValues)
		} yield (Seq(), langName)

		q.map { t =>
			p success new ComplexProductProperty(property.id, property.product_id, t._2, property.input_method, property.with_images, t._1)
		}

		f
	}

	def getPropertyValues(values: Seq[PropertyValue]): Future[Seq[ComplexPropertyValue]] = {
		val p = Promise[Seq[ComplexPropertyValue]]
		val f = p.future

		def iterate(l: Seq[PropertyValue], fl: Seq[ComplexPropertyValue] = Seq()): Unit = {
			if (l.isEmpty) {
				p success fl
			} else {
				if (l.length == fl.length) {
					p success fl
				} else {
					val a = l(fl.length)
					getPropertyValue(a).map { propertyValue =>
						iterate(l, fl :+ propertyValue)
					}
				}
			}
		}

		iterate(values)

		f
	}

	/**
		* Gets the property value with its lang.
		* @param propertyValueRaw
		* @return
		*/
	def getPropertyValue(propertyValueRaw: PropertyValue): Future[ComplexPropertyValue] = {
		val p = Promise[ComplexPropertyValue]
		val f = p.future

		langItemDAO.get(propertyValueRaw.lang_value_id).map { value =>
			p success new ComplexPropertyValue(propertyValueRaw.id, propertyValueRaw.property_id, value, propertyValueRaw.image_link, propertyValueRaw.order)
		}

		f
	}

	/**
		* Gets the last product index.
		* @return
		*/
	def lastIndex: Future[Int] = db.run(Products.sortBy(_.id.desc).map(_.id).result.head)

	/**
		* Updates a product.
		* @param product
		* @return
		*/
	def update(product: Product): Future[Unit] = {
		db.run(
			Products.filter(_.id === product.id).update(product)
		).map { _ => () }
	}

	/**
		* Erase all the table data
		* @return
		*/
	def erase: Future[Unit] = {
		val p = Promise[Unit]
		val begin_time = System.currentTimeMillis()

		db.run(Products.result).map { products =>
			if (products.nonEmpty) {
				for (product <- products) {
					val i = products.indexOf(product)
					val procs = for {
						_ <- productGalleryDAO.deleteGallery(product.id.get)
						_ <- langItemDAO.delete(product.lang_name_id)
						_ <- langItemDAO.delete(product.lang_description_id)
						_ <- delete(product.id.get)
					} yield Unit

					procs.map { _ =>
						if (i == products.length-1) {
							val time_in_seconds = (System.currentTimeMillis - begin_time) / 1000
							logger.info(s"${products.length} products were erased in $time_in_seconds seconds.")
							p success Unit
						}
					}
				}
			} else {
				p success Unit
			}
		}

		p.future
	}

	/**
		* Deletes the product with the given id
		* @param id
		* @return
		*/
	def delete(id: Int): Future[Unit] = {
		db.run(
			Products.filter(_.id === id).delete
		).map { _ => () }
	}

	/**
		* Checks if a product with the given id exists
		* @param id
		* @return
		*/
	def exists(id: Int): Future[Boolean] = {
		db.run(Products.filter(_.id === id).result).map { case s => s.nonEmpty }
	}

	/**
		* Checks if a product with the given reference exists
		* @param reference
		* @return
		*/
	def exists(reference: String): Future[Boolean] = {
		db.run(Products.filter(_.reference === reference).result).map { case s => s.nonEmpty }
	}

	/**
		* Inserts a new product
		* @param product
		* @return
		*/
	def insert(product: Product): Future[Unit] = db.run(Products += product).map { _ => () }

	/**
		* Table definition for Products
		* @param tag
		*/
	class ProductsTable(tag: Tag) extends Table[Product](tag, "product") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def lang_name_id = column[String]("lang_name_id")

		def lang_description_id = column[String]("lang_description_id")

		def category_id = column[Int]("category_id")

		def vat_id = column[Int]("vat_id")

		def reference = column[String]("reference")

		def stock = column[Int]("stock")

		def in_offer = column[Boolean]("in_offer")

		def offer_use_percentage = column[Boolean]("offer_use_percentage")

		def discount = column[Double]("discount")

		def offer_date_until = column[DateTime]("offer_date_until")

		def offer_use_date = column[Boolean]("offer_use_date")

		def price = column[Double]("price")

		def * = (id.?, lang_name_id, lang_description_id, category_id, vat_id, reference, stock, in_offer, offer_use_percentage, discount, offer_date_until, offer_use_date, price) <> (Product.tupled, Product.unapply _)
	}

	/**
		* Table definition for ProductReviews
		* @param tag
		*/
	class ProductReviewsTable(tag: Tag) extends Table[ProductReview](tag, "product_reviews") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def product_id = column[Int]("product_id")

		def product_reference = column[String]("product_reference")

		def user_id = column[Int]("user_id")

		def review = column[String]("review")

		def valoration = column[Int]("valoration")

		def * = (id.?, product_id, product_reference, user_id, review, valoration) <> (ProductReview.tupled, ProductReview.unapply)
	}
}
