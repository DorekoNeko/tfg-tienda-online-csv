package dao

import javax.inject._

import com.google.inject.ImplementedBy
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.json.{Format, Json}
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
	* Created by Daniel on 18/04/16.
	*/
@ImplementedBy(classOf[VATDAO])
trait IVATDAO {
	def all: Future[Seq[VAT]]
	def list(length: Option[Int] = Some(10), from: Option[Int] = Some(0), order: String = "asc"): Future[Seq[VAT]]
	def get(id: Int): Future[VAT]
	def count: Future[Int]
	def update(vat: VAT): Future[Unit]
	def delete(id: Int): Future[Unit]
	def delete(name: String): Future[Unit]
	def exists(id: Int): Future[Boolean]
	def exists(name: String): Future[Boolean]
	def insert(vat: VAT): Future[Unit]
}

case class VAT(id: Option[Int] = None, name: String, percentage: Double)

object VAT {
	implicit val vatFormat: Format[VAT] = Json.format[VAT]
}

class VATDAO @Inject()(
	protected val dbConfigProvider: DatabaseConfigProvider
)(implicit ec: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile] with IVATDAO {
	import driver.api._

	val VATS = TableQuery[VATSTable]

	/**
		* Returns all the VATs
		* @return
		*/
	def all: Future[Seq[VAT]] = db.run(VATS.result)

	/**
		* Gets a list of products of the given length starting from the given index ordered by asc or desc.
		* @param length
		* @param from
		* @param order
		* @return
		*/
	def list(length: Option[Int] = Some(10), from: Option[Int] = Some(0), order: String = "asc"): Future[Seq[VAT]] = {
		db.run(
			VATS
				.drop(from.getOrElse(0))
				.take(length.getOrElse(10))
				.sortBy(p => if (order == "asc") p.id.asc else p.id.desc).result
		).map { vats =>
			vats
		}
	}

	/**
		* Returns the VAT with the given id
		* @param id
		* @return
		*/
	def get(id: Int): Future[VAT] = db.run(VATS.filter(_.id === id).result.head)

	/**
		* Gets the total number of VAT items.
		*/
	def count: Future[Int] = db.run(VATS.map(_.id).length.result)

	/**
		* Updates the given VAT
		* @param vat
		* @return
		*/
	def update(vat: VAT): Future[Unit] = {
		db.run(
			VATS.filter(_.id === vat.id).update(vat)
		).map { _ => () }
	}

	/**
		* Deletes the VAT with the given id
		* @param id
		* @return
		*/
	def delete(id: Int): Future[Unit] = {
		db.run(
			VATS.filter(_.id === id).delete
		).map { _ => () }
	}

	/**
		* Deletes the VAT with the given name
		* @param name
		* @return
		*/
	def delete(name: String): Future[Unit] = {
		db.run(
			VATS.filter(_.vat_name === name).delete
		).map { _ => () }
	}

	/**
		* Checks if a VAT with the given name exists
		* @param name
		* @return
		*/
	def exists(name: String): Future[Boolean] = {
		db.run(VATS.filter(_.vat_name === name).result).map { case s => s.nonEmpty }
	}

	/**
		* Checks if a VAT with the given id exists
		* @param id
		* @return
		*/
	def exists(id: Int): Future[Boolean] = {
		db.run(VATS.filter(_.id === id).result).map { case s => s.nonEmpty }
	}

	/**
		* Inserts a new VAT
		* @param vat
		* @return
		*/
	def insert(vat: VAT): Future[Unit] = db.run(VATS += vat).map { _ => () }

	// Table
	class VATSTable(tag: Tag) extends Table[VAT](tag, "vat") {

		def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

		def vat_name = column[String]("vat_name")

		def percentage = column[Double]("percentage")

		def * = (id.?, vat_name, percentage) <>((VAT.apply _).tupled, VAT.unapply _)
	}
}
