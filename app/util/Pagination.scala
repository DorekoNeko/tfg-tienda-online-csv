package util

import scala.collection.mutable.ListBuffer

/**
	* Created by Daniel on 21/04/16.
	*/
class Pagination(val actual_page: Int = 1, total_rows: Int, rows_per_page: Int = 10) {
	/**
		* Gets the previous page if able
		* @return
		*/
	def previous: Option[Int] = {
		if (actual_page-1 > 0)
			Some(actual_page-1)
		else
			None
	}

	/**
		* Gets the next page if able
		* @return
		*/
	def next: Option[Int] = {
		if (actual_page * rows_per_page < total_rows)
			Some(actual_page+1)
		else
			None
	}

	/**
		* Gets the last page
		* @return
		*/
	def last: Int = total_page_count

	/**
		* Gets the pages to show in the pagination
		* @return
		*/
	def pages: Array[Int] = {
		val lb: ListBuffer[Int] = ListBuffer()
		val total_pages = total_page_count

		// If total pages is greater than the number of pages showed...
		if (total_pages > 9) {
			// Getting the 3 pages before the actual
			for (i <- 1 to 3) {
				val n = actual_page + i
				if (n > 0)
					lb += actual_page + i
			}

			// the actual one
			lb += actual_page

			// and the 3 pages after the actual
			for (i <- 1 to 3) {
				val n = actual_page + i
				if (n <= total_pages)
					lb += actual_page + i
			}
		} else {
			for (i <- 1 to total_pages) {
				lb += i
			}
		}

		lb.toArray
	}

	/**
		* Returns the total posible pages
		* @return
		*/
	def total_page_count: Int = {
		val total_pages = total_rows.toDouble / rows_per_page.toDouble
		if (total_pages % 1 == 0)
			total_pages.toInt
		else
			total_pages.toInt + 1
	}
}
