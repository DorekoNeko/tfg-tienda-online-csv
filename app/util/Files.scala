package util

import java.io.{BufferedInputStream, File, FileInputStream}
import java.nio.ByteBuffer
import java.nio.charset.{Charset, CharsetDecoder}

/**
	* Created by Daniel on 23/11/15.
	*/
object Files {
	/**
		* Returns the extension of the given file name
		* @param filename
		* @return
		*/
	def getExtension(filename: String): String = {
		val parts = filename.split("\\.")

		if (parts.isEmpty) {
			"jpg"
		} else parts.last
	}

	/**
		* Test if the given file have the given charset
		* @param f
		* @param ch
		* @return
		*/
	def isCharset(f: File, ch: String): Option[Charset] = {
		try {
			val input: BufferedInputStream = new BufferedInputStream(new FileInputStream(f))
			val charset: Charset = Charset.forName(ch)
			//println("Charset:" + charset)
			val decoder: CharsetDecoder = charset.newDecoder()
			decoder.reset()

			val buffer: Array[Byte] = new Array[Byte](512)

			var identified = false
			while ( (input.read(buffer) != -1) && (!identified) ) {
				identified = identifyBytesCharset(buffer, decoder)
				//println("Identified: " + identified)
			}

			input.close()

			if (identified) {
				Some(charset)
			} else {
				None
			}
		} catch {
			case e: Exception => println("Exception: " + e); None
		}
	}

	/**
		* Compares bytes against the decoder to test the charset
		* @param bytes
		* @param decoder
		* @return
		*/
	def identifyBytesCharset(bytes: Array[Byte], decoder: CharsetDecoder): Boolean = {
		try {
			decoder.decode(ByteBuffer.wrap(bytes))

			true
		} catch {
			case e: Exception => println("Exception decoding: " + e); false
		}
	}
}
