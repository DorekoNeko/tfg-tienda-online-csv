package util

/**
	* Created by Daniel on 30/12/15.
	*/
object Validation extends Enumeration {
	type Validation = Value

	val Valid = Value("Valid")
	val Invalid = Value("Invalid")
}
