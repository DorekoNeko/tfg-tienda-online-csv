package util

/**
	* Created by Daniel on 12/01/16.
	*/
object CSV {
	case class ImportError(line_number: Option[Int], error_message: String, sub_line: Option[Int] = None)
}
