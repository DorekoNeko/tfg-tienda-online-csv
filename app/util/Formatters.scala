package util

import play.api.data.FormError
import play.api.data.format.Formats._
import play.api.data.format.Formatter

/**
	* Created by Daniel on 12/01/16.
	*/
object Formatters {
	/**
		* Default formatter for the `Double` type.
		*/
	implicit def doubleFormat: Formatter[Double] = new Formatter[Double] {

		override val format = Some("format.real", Nil)

		def bind(key: String, data: Map[String, String]) =
			parsing(_.toDouble, "error.real", Nil)(key, data)

		def unbind(key: String, value: Double) = Map(key -> value.toString)
	}

	/**
		* Helper for formatters binders
		* @param parse Function parsing a String value into a T value, throwing an exception in case of failure
		* @param errMsg Error to set in case of parsing failure
		* @param key Key name of the field to parse
		* @param data Field data
		*/
	private def parsing[T](parse: String => T, errMsg: String, errArgs: Seq[Any])(key: String, data: Map[String, String]): Either[Seq[FormError], T] = {
		stringFormat.bind(key, data).right.flatMap { s =>
			scala.util.control.Exception.allCatch[T]
				.either(parse(s))
				.left.map(e => Seq(FormError(key, errMsg, errArgs)))
		}
	}
}
