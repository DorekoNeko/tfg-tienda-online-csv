package util.lang

import play.api.mvc.Cookies

/**
	* Created by Daniel on 9/05/16.
	*/
object ClientLang {
	case class CurrentLang(code: String)

	/**
		* Returns the language defined by the client if any or the default one defined by the shop config.
		* @param cookies
		* @param default
		* @return
		*/
	def get(cookies: Cookies, default: String): CurrentLang = {
		val lang_cookie =cookies.get("PLAY_LANG")
		if (lang_cookie.isDefined) {
			new CurrentLang(lang_cookie.get.value)
		} else {
			new CurrentLang(default)
		}
	}
}
