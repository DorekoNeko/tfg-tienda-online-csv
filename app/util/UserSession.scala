package util

import dao.User
import org.joda.time.{DateTime, Minutes}
import play.api.Play.current
import play.api.{Logger, Play}
import util.Validation._

import scala.collection.mutable.{ListBuffer, Map}

/**
	* Created by Daniel on 29/12/15.
	*/
object UserSession {
	val logger: Logger = Logger(this.getClass)

	class Session(val token: String) {
		val timestamp = DateTime.now()
		val properties: Map[String, Any] = Map()

		/**
			* Returns the property value if exists
			* @param property_name
			* @return
			*/
		def get(property_name: String): Option[Any] = {
			if (properties.exists(p => p._1 == property_name)) {
				Some(properties.find(p => p._1 == property_name).get._2)
			} else {
				None
			}
		}

		/**
			* Adds a new property
			* @param name
			* @param value
			*/
		def add(name: String, value: Any) = properties.+=((name, value))
	}

	val sessions: ListBuffer[Session] = ListBuffer()

	/**
		* Create a new session and returns it
		* @return
		*/
	def start(): Session = {
		var token = Hashing.randomAlphaNumericString(32)
		while(sessions.exists(us => us.token == token)) token = Hashing.randomAlphaNumericString(32)

		val session = new Session(token)
		sessions += session

		session
	}

	/**
		* Create a dummy session for developer_mode and returns it
		* @return
		*/
	def startDummySession: Session = {
		val session = UserSession.start()
		session.add("user", new User(Some(1900), "Dummy", "", "testing@dummy.org", "none", "Mr.", "Dummy", 40, 53, "9999999Z", "999999999", true))

		session
	}

	/**
		* Ends a session
		* @param session
		* @return
		*/
	def end(session: Session) = sessions.remove(sessions.indexWhere(_ == session))

	/**
		* Gets an already created session
		* @param token
		* @return
		*/
	def get(token: String): Option[Session] = sessions.find(_.token == token)

	/**
		* Checks if a user session is valid or invalid
		* @param session
		* @return
		*/
	def validate(session: Session): Validation = {
		val timeNow = DateTime.now()
		val difference = Minutes.minutesBetween(session.timestamp, timeNow)
		val sessionExpiryTime = Play.configuration.getInt("session.duration").getOrElse(60)

		// If it is not expired
		if (!difference.isGreaterThan(Minutes.minutes(sessionExpiryTime))) {
			Valid
		} else {
			Invalid
		}
	}

	/**
		* Checks if a user session is valid or invalid with a play session.
		* @param session
		* @return
		*/
	def validateWithSession(session: play.api.mvc.Session): Validation = {
		if (checkSessionCookie(session)) {
			if (UserSession.get(session.get("token").get).nonEmpty) {
				val user_session = UserSession.get(session.get("token").get).get

				if (validate(user_session) == Valid) {
					Valid
				} else {
					Invalid
				}
			} else {
				Invalid
			}
		} else {
			Invalid
		}
	}

	/**
		* Checks if a token cookie exists
		* @param session
		* @return
		*/
	def checkSessionCookie(session: play.api.mvc.Session): Boolean = {
		if (session.get("token").nonEmpty) {
			true
		} else {
			false
		}
	}

	/**
		* Cleans invalid sessions
		*/
	def clean() = {
		val init: Int = sessions.length
		for(s <- sessions) {
			if (validate(s) == Invalid) {
				end(s)
			}
		}

		val cleaned = init - sessions.length
		logger.info(s"$cleaned session(s) have been cleaned.")
	}
}