package debug

import org.joda.time.DateTime
import play.api.Logger

/**
	* Created by Daniel on 15/04/16.
	*/
class AsyncTimer(instance: Any) {
	val logger = Logger(this.getClass)
	val begin = DateTime.now()

	def end = {
		val end = DateTime.now().minus(begin.getMillis).toInstant

		logger.info(s"$instance - ended in ${end.getMillis} millis.")
	}
}
