package settings

import javax.inject._

import com.google.inject.ImplementedBy
import dao.{IShopSettingDAO, ISupportedLangDAO, Setting}
import play.api.Logger

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}

/**
	* Created by Daniel on 13/01/16.
	*/

@ImplementedBy(classOf[ShopSettings])
trait IGeneralSettings {
	def initialize: Future[Unit]
	def set(name: String, value: String = "", boolean: Boolean = false): Future[Unit]
	def add(name: String, value: String = "", boolean: Boolean = false): Future[Unit]
	def get(name: String): Option[Setting]
	def getAll: Seq[Setting]
	def getObject: Future[Option[GeneralSettings]]
	def getGeneralSettings: GeneralSettings
	def refresh: Future[Unit]
}

/**
	* Simple case class to hold primary data for general_settings group.
	* @param shop_name
	* @param shop_description
	* @param company_name
	* @param address
	* @param nif_cif
	* @param products_per_page
	* @param legal_info
	* @param slider
	* @param logo
	* @param logo_minimal
	* @param shop_cover
	* @param shipment_costs
	*/
case class GeneralSettings(
	shop_name: String,
	shop_description: String,
	company_name: String,
	address: String,
	nif_cif: String,
	products_per_page: Int,
	legal_info: String,
	slider: Boolean,
	logo: String = "",
	logo_minimal: String = "",
	shop_cover: String = "",
	shipment_info: String = "",
	shipment_costs: Double = 0,
	payment_info: String = "",
	bank_entity: String = "",
	bank_account: String = ""
)

@Singleton
class ShopSettings @Inject()(
	protected val shopSettingsDAO: IShopSettingDAO,
	protected val supportedLangDAO: ISupportedLangDAO
) extends IGeneralSettings {
	val logger = Logger(this.getClass)
	@volatile private var ready = false
	@volatile private var settings: Seq[Setting] = Seq()

	/**
		* Initializing values
		*/
	def initialize: Future[Unit] = {
		val p = Promise[Unit]()

		// TODO: CHECK IF DEFAULT VALUES EXISTS AND CREATE THEM IF NOT

		if (ready) {
			// Return a void future if already loaded.
			p success Unit
		} else {
			shopSettingsDAO.get("general_settings").map { s =>
				settings = s
				logger.info("General Settings query finished.")

				supportedLangDAO.getDefault.map { defaultLang =>
					logger.info("Get default supported lang query finished.")

					val defaults = {
						var accum = Future{()}

						if (!settings.exists(_.key.equalsIgnoreCase("shop_name")))
							accum = accum flatMap(_ => add("shop_name", "My Shop!"))
						if (!settings.exists(_.key.equalsIgnoreCase("shop_description")))
							accum = accum flatMap(_ => add("shop_description", "Goodies and stuff"))
						if (!settings.exists(_.key.equalsIgnoreCase("logo")))
							accum = accum flatMap(_ => add("logo", "undefined"))
						if (!settings.exists(_.key.equalsIgnoreCase("use_slider")))
							accum = accum flatMap(_ => add("use_slider", boolean = false))
						if (!settings.exists(_.key.equalsIgnoreCase("products_per_page")))
							accum = accum flatMap(_ => add("products_per_page", "8"))

						accum
					}

					defaults.map { _ =>
						if (!settings.exists(_.key == "default_lang")) {
							add("default_lang", defaultLang.short).map { _ =>
								ready = true
								logger.info("General settings loaded.")
								p success Unit
							}
						} else {
							set("default_lang", defaultLang.short)
							ready = true
							logger.info("General settings loaded.")
							p success Unit
						}
					}
				}
			}
		}

		p.future
	}

	/**
		* Set a setting value. (or create one if not exists)
		* @param name
		* @param value
		* @param boolean
		* @return
		*/
	def set(name: String, value: String = "", boolean: Boolean = false): Future[Unit] = {
		val p = Promise[Unit]()

		if (settings.exists(s => s.key == name)) {
			val s = settings.find(s => s.key == name).get
			s.value = value
			s.value_boolean = boolean

			shopSettingsDAO.update(s).map( _ => p success Unit )
		} else {
			add(name, value, boolean).map( _ => p success Unit )
		}

		p.future
	}

	/**
		* Adds a new setting.
		* @param name
		* @param value
		* @param boolean
		* @return
		*/
	def add(name: String, value: String = "", boolean: Boolean = false): Future[Unit] = {
		val p = Promise[Unit]()

		shopSettingsDAO.insert(new Setting(None, name, value, boolean, "general_settings")).map { _ =>
			refresh.map( _ => p success Unit )
		}

		p.future
	}

	/**
		* Returns all the settings
		* @return
		*/
	def getAll = settings

	/**
		* Returns the setting with the given name
		* @param name
		* @return
		*/
	def get(name: String): Option[Setting] = settings.find(s => s.key == name)

	def getObject: Future[Option[GeneralSettings]] = {
		val p = Promise[Option[GeneralSettings]]()

		val use_slider = if (get("use_slider").isDefined) {
			get("use_slider").get.value_boolean
		} else { false }

		p success Some(
			new GeneralSettings(
				if (get("shop_name").isDefined) get("shop_name").get.value else "",
				if (get("shop_description").isDefined) get("shop_description").get.value else "",
				if (get("company_name").isDefined) get("company_name").get.value else "...",
				if (get("address").isDefined) get("address").get.value else "...",
				if (get("nif_cif").isDefined) get("nif_cif").get.value else "...",
				if (get("products_per_page").isDefined) get("products_per_page").get.value.toInt else 8,
				if (get("legal_info").isDefined) get("legal_info").get.value else "",
				if (get("use_slider").isDefined) get("use_slider").get.value_boolean else false,
				if (get("logo").isDefined) get("logo").get.value else "",
				if (get("logo_minimal").isDefined) get("logo_minimal").get.value else ""
			)
		)

		p.future
	}

	def getGeneralSettings: GeneralSettings = {
		val use_slider = if (get("use_slider").isDefined) {
			get("use_slider").get.value_boolean
		} else { false }

		new GeneralSettings(
			if (get("shop_name").isDefined) get("shop_name").get.value else "",
			if (get("shop_description").isDefined) get("shop_description").get.value else "",
			if (get("company_name").isDefined) get("company_name").get.value else "...",
			if (get("address").isDefined) get("address").get.value else "...",
			if (get("nif_cif").isDefined) get("nif_cif").get.value else "...",
			if (get("products_per_page").isDefined) get("products_per_page").get.value.toInt else 8,
			if (get("legal_info").isDefined) get("legal_info").get.value else "",
			if (get("use_slider").isDefined) get("use_slider").get.value_boolean else false,
			if (get("logo").isDefined) get("logo").get.value else "undefined",
			if (get("logo_minimal").isDefined) get("logo_minimal").get.value else "",
			if (get("shop_cover").isDefined) get("shop_cover").get.value else "",
			if (get("shipment_info").isDefined) get("shipment_info").get.value else "",
			if (get("shipment_costs").isDefined) get("shipment_costs").get.value.toDouble else 0,
			if (get("payment_info").isDefined) get("payment_info").get.value else "",
			if (get("bank_account_entity").isDefined) get("bank_account_entity").get.value else "",
			if (get("bank_account").isDefined) get("bank_account").get.value else ""
		)
	}

	/**
		* Refreshes general settings
		* @return
		*/
	def refresh: Future[Unit] = {
		ready = false
		val p = Promise[Unit]()

		shopSettingsDAO.get("general_settings").map { s =>
			logger.info("General settings refreshed.")
			settings = s

			supportedLangDAO.getDefault.map { defaultLang =>
				if (!settings.exists(_.key == "default_lang")) {
					add("default_lang", defaultLang.short).map { _ =>
						ready = true
						logger.info("General settings loaded.")
						p success Unit
					}
				} else {
					set("default_lang", defaultLang.short)
					ready = true
					logger.info("General settings loaded.")
					p success Unit
				}
			}
		}

		p.future
	}
}
