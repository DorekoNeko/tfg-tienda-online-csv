package modules

import actors._
import com.google.inject.AbstractModule
import dao.ShopSettingDAO
import play.api.Logger
import play.api.libs.concurrent.AkkaGuiceSupport
import settings.ShopSettings

/**
	* Created by Daniel
	*/
class Globals extends AbstractModule with AkkaGuiceSupport {
	val logger = Logger(this.getClass)

	def configure = {
		// Binding start class.
		logger.info("Binding application start.")
		bind(classOf[ShopSettingDAO]).asEagerSingleton()
		bind(classOf[ShopSettings]).asEagerSingleton()
		bindActor[SessionCleaner]("SessionCleaner")
		bindActor[TmpImportCleaner]("TmpImportCleaner")
		bindActor[LiveChatHandler]("LiveChatHandler")
	}
}