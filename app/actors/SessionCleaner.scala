package actors

import akka.actor._
import play.api.Logger
import util.UserSession

/**
	* Created by Daniel on 30/12/15.
	*/
object SessionCleaner {
	def props = Props[SessionCleaner]

	object Clean
}

class SessionCleaner extends Actor {
	import SessionCleaner._

	val logger: Logger = Logger(this.getClass)

	def receive = {
		case Clean =>
			logger.info("Executing session cleaning proccess...")
			UserSession.clean
	}
}
