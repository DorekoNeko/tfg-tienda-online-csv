package actors

import javax.inject._

import akka.actor._
import dao.{Conversation, IConversationDAO, Message}
import org.joda.time._
import org.joda.time.format._
import play.api.Logger
import play.api.i18n.{I18nSupport, Lang, MessagesApi}
import play.api.libs.json._

import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Promise}

/**
	* Created by Daniel on 8/04/16.
	*/
object LiveChatHandler {
	def props = Props[LiveChatHandler]

	case class addSocket(actor: LiveChatSocket)
	case class removeSocket(actor: LiveChatSocket)
	case class startConversation(actor: LiveChatSocket)
	case class endConversation(actor: LiveChatSocket, hash: String)
	case class sendMessage(actor: LiveChatSocket, message: String, hash: String)

	object Store
	object AdminExists
}

class LiveChatHandler @Inject()(
	protected val conversationDAO: IConversationDAO,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Actor with I18nSupport {
	import LiveChatHandler._

	val logger: Logger = Logger("LiveChat.Handler")
	val msgPool: ListBuffer[Message] = ListBuffer()
	val sockets: ListBuffer[LiveChatSocket] = ListBuffer()
	implicit val lang = new Lang("en")

	/**
		* Handles the actor messages
		* @return
		*/
	def receive = {
		case Store =>
			// Store messages into the database.
			val pool: ListBuffer[Message] = msgPool.clone()
			msgPool.clear()

			logger.info("Storing chat messages...")
		case addSocket(actor: LiveChatSocket) =>
			// Adds a new user socket.
			// (replaces the old socket if the user is already registered)
			sockets.exists(p => p.user.id.get == actor.user.id.get) match {
				case true =>
					sockets -= sockets.find(p => p.user.id.get == actor.user.id.get).get
					sockets += actor
					logger.info(s"Adding a socket for the user ${actor.user.username} (removing old socket)")
				case false =>
					sockets += actor
					logger.info(s"Adding a socket for the user ${actor.user.username}")
			}

			// CHECK IF IT IS AN ADMIN AND NOTIFY ITS STATUS TO ALL NON-ADMIN SOCKETS
			if (actor.user.is_admin) {
				for (socket <- sockets.filterNot(_.user.is_admin)) {
					val json: JsValue = JsObject(Seq(
						"type" -> JsString("notify-admin-online")
					))

					socket.out ! json
				}
			}
		case removeSocket(actor: LiveChatSocket) =>
			// Removes the given user socket from the list.
			sockets -= actor
			logger.info(s"Removing a user socket (${actor.user.username})")

			// Notify the disconnect to the admin
			if (actor.conversation_hash.nonEmpty) {
				val admin = sockets.find(_.user.is_admin)
				if (admin.nonEmpty) {
					val json: JsValue = JsObject(Seq(
						"type" -> JsString("notify-chat-end"),
						"conversation" -> JsObject(Seq(
							"hash" -> JsString(actor.conversation_hash)
						)),
						"username" -> JsString(actor.user.name + " " + actor.user.last_name)
					))

					admin.get.out ! json
				}

				// If the conversation is empty, delete it.
				conversationDAO.getMessagesCount(actor.conversation_hash).map { messages =>
					if (messages == 0) {
						conversationDAO.deleteConversation(actor.conversation_hash)
					}
				}
			}  else if (actor.user.is_admin) {
				// CHECK IF IT IS AN ADMIN AND NOTIFY ITS STATUS TO ALL NON-ADMIN SOCKETS
				for (socket <- sockets.filterNot(_.user.is_admin)) {
					val json: JsValue = JsObject(Seq(
						"type" -> JsString("notify-admin-offline")
					))

					socket.out ! json
				}
			}
		case AdminExists =>
			// Checks if there are admin sockets registered
			sender() ! sockets.exists(_.user.is_admin)
		case startConversation(actor: LiveChatSocket) =>
			// Try to start a conversation between a user and an admin
			val p = Promise[Option[String]]()

			if (!actor.user.is_admin && sockets.exists(_.user.is_admin)) {
				conversationDAO.generateHash.map { hash =>
					val formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss")
					val current_date = DateTime.now()
					val conversation = new Conversation(None, s"${messagesApi("chat.live_chat")} - ${current_date.toString(formatter)}", hash = hash)
					conversationDAO.insertConversation(conversation).map { _ =>
						val admin = sockets.find(_.user.is_admin).get
						val json: JsValue = JsObject(Seq(
							"type" -> JsString("notify-chat-start"),
							"conversation" -> JsObject(Seq(
								"hash" -> JsString(hash)
							)),
							"username" -> JsString(actor.user.name + " " + actor.user.last_name)
						))

						// Notify the new chat to the admin too
						admin.out ! json

						p success Some(hash)
					}
				}
			} else {
				p success None
			}

			sender() ! p.future
		case endConversation(actor: LiveChatSocket, hash: String) =>
			if (actor.user.is_admin) {
				var user = sockets.find(_.conversation_hash == hash)

				if (user.nonEmpty) {
					val json: JsValue = JsObject(Seq(
						"type" -> JsString("chat-end"),
						"conversation" -> JsObject(Seq(
							"hash" -> JsString(hash)
						)),
						"username" -> JsString(actor.user.name + " " + actor.user.last_name)
					))

					user.get.out ! json
				}
			} else {
				val json: JsValue = JsObject(Seq(
					"type" -> JsString("chat-end"),
					"result" -> JsObject(Seq(
						"error" -> JsObject(Seq(
							"message" -> JsString("Only the admin can do this."),
							"code" -> JsNumber(2000)
						))
					))
				))

				actor.out ! json
			}
		case sendMessage(actor: LiveChatSocket, message: String, hash: String) =>
			if (actor.user.is_admin) {
				val user = sockets.filterNot(_ == actor).find(_.conversation_hash == hash)

				if (user.nonEmpty) {
					val msg = new Message(None, hash, message, user_id = actor.user.id.get)
					conversationDAO.insertMessage(msg)

					val json: JsValue = JsObject(Seq(
						"type" -> JsString("message-received"),
						"conversation" -> JsObject(Seq(
							"hash" -> JsString(hash)
						)),
						"message" -> JsString(msg.message),
						"username" -> JsString(actor.user.name + " " + actor.user.last_name)
					))

					user.get.out ! json
				} else {
					val json: JsValue = JsObject(Seq(
						"type" -> JsString("message-received"),
						"result" -> JsObject(Seq(
							"error" -> JsObject(Seq(
								"message" -> JsString("Client not found. Maybe the user disconnected?"),
								"code" -> JsNumber(1998)
							))
						))
					))
				}
			} else {
				val admin = sockets.find(_.user.is_admin).get
				val msg = new Message(None, hash, message, user_id = actor.user.id.get)
				conversationDAO.insertMessage(msg)

				val json: JsValue = JsObject(Seq(
					"type" -> JsString("message-received"),
					"conversation" -> JsObject(Seq(
						"hash" -> JsString(hash)
					)),
					"message" -> JsString(msg.message),
					"username" -> JsString(actor.user.name + " " + actor.user.last_name)
				))

				// Notify the new chat to the admin too
				admin.out ! json
			}
	}

	/**
		* Gets the socket with the given user id if exists.
		* @param id
		* @return
		*/
	def getByUserId(id: Int): Option[LiveChatSocket] = sockets.find(_.user.id.get == id)
}