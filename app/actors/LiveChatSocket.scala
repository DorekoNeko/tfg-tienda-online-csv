package actors

import javax.inject._

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import dao.User
import play.api.Logger
import play.api.libs.json._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

/**
	* Created by Daniel on 8/04/16.
	*/
object LiveChatSocket {
	def props(out: ActorRef)(implicit liveChat: ActorRef, user: User) = Props(new LiveChatSocket(liveChat, user)(out))
}

class LiveChatSocket @Inject()(
	@Named("LiveChatHandler") handler: ActorRef,
	val user: User
)(val out: ActorRef) extends Actor {
	import LiveChatHandler._

	val logger: Logger = Logger("LiveChat.Socket")
	var conversation_hash: String = ""
	implicit val timeout = Timeout(10.seconds)

	// Registering this socket to the sockets controller
	handler ! new addSocket(this)

	/**
		* Handles the actor messages
		* @return
		*/
	def receive = {
		case msg: JsValue =>
			handleMessage(msg)
	}

	/**
		* Handling messages in JSON format
		* @param message
		*/
	def handleMessage(message: JsValue): Unit = {
		println(Json.prettyPrint(message))

		val jsonMsgType = (message \ "type").toOption

		if (jsonMsgType.nonEmpty) {
			// Matching the request type
			jsonMsgType.get.asOpt[String] match {
				case Some(msgType) =>
					msgType match {
						case "chat-start" =>
							(handler ? startConversation(this)).mapTo[Future[Option[String]]].map { message =>
								message.map { hash =>
									if (hash.nonEmpty) {
										this.conversation_hash = hash.get
										val json: JsValue = JsObject(Seq(
											"type" -> JsString("chat-start"),
											"result" -> JsObject(Seq(
												"conversation" -> JsObject(Seq(
													"hash" -> JsString(hash.get)
												)),
												"username" -> JsString(user.name + " " + user.last_name)
											))
										))

										out ! json
									} else {
										val json: JsValue = JsObject(Seq(
											"type" -> JsString("chat-start"),
											"result" -> JsObject(Seq(
												"error" -> JsObject(Seq(
													"message" -> JsString("Unable to create the conversation."),
													"code" -> JsNumber(1996)
												))
											))
										))

										out ! json
									}
								}
							}
						case "chat-end" =>
							val jsonHash = (message \ "hash").toOption

							if (jsonHash.nonEmpty) {
								handler ! endConversation(this, jsonHash.get.as[String])
							} else {
								val json: JsValue = JsObject(Seq(
									"type" -> JsString("chat-end"),
									"result" -> JsObject(Seq(
										"error" -> JsObject(Seq(
											"message" -> JsString("Unespecified hash."),
											"code" -> JsNumber(1999)
										))
									))
								))

								out ! json
							}
						case "send-message" =>
							val jsonMsg = (message \ "message").toOption
							val jsonHash = (message \ "hash").toOption

							if (jsonMsg.nonEmpty && jsonHash.nonEmpty) {
								handler ! sendMessage(this, jsonMsg.get.as[String], jsonHash.get.as[String])
							} else {
								val json: JsValue = JsObject(Seq(
									"type" -> JsString("send-message"),
									"result" -> JsObject(Seq(
										"error" -> JsObject(Seq(
											"message" -> JsString("Unespecified message or hash."),
											"code" -> JsNumber(1997)
										))
									))
								))

								out ! json
							}
						case "check-admin" =>
							(handler ? AdminExists).mapTo[Boolean].map { message =>
								if (message) {
									val json: JsValue = JsObject(Seq(
										"type" -> JsString("check-admin"),
										"result" -> JsBoolean(true)
									))

									out ! json
								} else {
									val json: JsValue = JsObject(Seq(
										"type" -> JsString("check-admin"),
										"result" -> JsBoolean(false)
									))

									out ! json
								}
							}
						case _ =>
							// If the type is not recognized (1993)
							val json: JsValue = JsObject(Seq(
								"type" -> JsString("undefined"),
								"result" -> JsObject(Seq(
									"error" -> JsObject(Seq(
										"message" -> JsString("Unrecognized request type."),
										"code" -> JsNumber(1993)
									))
								))
							))

							out ! json
					}
				case None =>
					// if the type is not a String (1994)
					val json: JsValue = JsObject(Seq(
						"type" -> JsString("undefined"),
						"result" -> JsObject(Seq(
							"error" -> JsObject(Seq(
								"message" -> JsString("Request type must be a string."),
								"code" -> JsNumber(1994)
							))
						))
					))

					out ! json
			}
		} else {
			// If there is no type specified (1995)0.
			val json: JsValue = JsObject(Seq(
				"type" -> JsString("undefined"),
				"result" -> JsObject(Seq(
					"error" -> JsObject(Seq(
						"message" -> JsString("Unespecified request type."),
						"code" -> JsNumber(1995)
					))
				))
			))

			out ! json
		}
	}

	/**
		* Handles the "close socket" event from the client
		*/
	override def postStop() = {
		// Removing this socket from the controller.
		handler ! new removeSocket(this)
	}
}