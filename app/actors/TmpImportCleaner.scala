package actors

import java.io.IOException

import akka.actor.{Actor, Props}
import org.apache.commons.io.FileUtils
import play.api.Logger

/**
	* Created by Daniel on 30/05/16.
	*/
object TmpImportCleaner {
	def props = Props[TmpImportCleaner]

	case class CleanProducts(hash: String)
}

class TmpImportCleaner extends Actor {
	import TmpImportCleaner._

	val logger: Logger = Logger(this.getClass)

	def receive = {
		case CleanProducts(hash: String) =>
			logger.info(s"Executing cleaning proccess for imported products with hash $hash...")
			val tmp_location = System.getProperty("java.io.tmpdir") + s"/${hash}"
			val tmp_dir = new java.io.File(tmp_location)

			if (tmp_dir.exists()) {
				try {
					FileUtils.deleteDirectory(tmp_dir)
				} catch {
					case e: IOException => logger.error(s"IOException while trying to delete $hash directory. ($tmp_location)")
				}
			}
	}
}