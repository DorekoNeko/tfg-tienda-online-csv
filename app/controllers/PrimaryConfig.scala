package controllers

import javax.inject.Inject

import dao._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import settings.IGeneralSettings

import scala.concurrent.{ExecutionContext, Promise}

/**
	* Created by Daniel on 6/05/16.
	*/
class PrimaryConfig @Inject()(
	protected val userDAO: IUserDAO,
	protected val addressDAO: IAddressDAO,
	protected val categoryDAO: ICategoryDAO,
	protected val langItemDAO: ILangItemDAO,
	protected val languageDAO: ILanguageDAO,
	protected val productDAO: IProductDAO,
	protected val productGalleryDAO: IProductGalleryDAO,
	protected val sectionDAO: ISectionDAO,
	protected val shopSettingsDAO: IShopSettingDAO,
	protected val supportedLangDAO: ISupportedLangDAO,
	protected val VATDAO: IVATDAO,
	protected val settings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {

	/**
		* Displays the restore defaults section.
		* @return
		*/
	def restore_defaults_form = Action { implicit request =>
		implicit val generalSettings = settings.getGeneralSettings
		Ok(views.html.panel.restore_defaults())
	}

	/**
		* Process the restore defaults section.
		* @return
		*/
	def restore_defaults = Action.async { implicit request =>
		val p = Promise[Result]()

		// Emptying sections
		val sectionEmpty = for {
			_ <- sectionDAO.empty
			_ <- sectionDAO.load_defaults
		} yield Unit

		sectionEmpty.map { _ =>
			p success Redirect(routes.PrimaryConfig.restore_defaults_form())
				.flashing("redirect-message" -> "Default settings restored.",
					"redirect-type" -> "alert-success")
		}

		p.future
	}

}
