package controllers.ajax

import javax.inject._

import actions.builders.Authenticated
import dao._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import se.digiplant.res.api.Res
import settings.IGeneralSettings

import scala.concurrent.{ExecutionContext, Promise}

class Account @Inject()(
	protected val userDAO: IUserDAO,
	protected val addressDAO: IAddressDAO,
	protected val sectionsDAO: ISectionDAO,
	protected val supportedLangsDAO: ISupportedLangDAO,
	protected val conversationDAO: IConversationDAO,
	protected val orderDAO: IOrderDAO,
	protected val generalSettings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	import controllers.Account._

	/**
		* Returns the portion of html that represents the user menu of the header
		* @return
		*/
	def header_user_menu = Authenticated { implicit request =>
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]
		Ok(views.html.ajax.header_user_menu(user.get))
	}

	/**
		* Returns the portion of html that represents the user cart of the header
		* @return
		*/
	def header_user_cart = Authenticated { implicit request =>
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]
		Ok(views.html.ajax.header_user_cart(user.get))
	}

	/**
		* Displays the general account settings form
		* @return
		*/
	def general_settings_form = Authenticated { implicit request =>
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]
		val accountSettings = new AccountGeneralSettings(user.get.mail, user.get.name, user.get.last_name, user.get.DNI, user.get.phone_number)

		Ok(views.html.ajax.account_settings(accountSettingsForm.fill(accountSettings), passwordChangeForm, user.get))
	}

	/**
		* Process the general account settings form
		* @return
		*/
	def general_settings = Authenticated.async(parse.multipartFormData) { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		accountSettingsForm.bindFromRequest.fold(
			errors => {
				val accountSettings = new AccountGeneralSettings(user.get.mail, user.get.name, user.get.last_name, user.get.DNI, user.get.phone_number)
				p success Ok(views.html.ajax.account_settings(errors, passwordChangeForm, user.get))
			},
			(data: AccountGeneralSettings) => {
				// Updating user props
				user.get.name = data.name
				user.get.last_name = data.last_name
				user.get.DNI = data.dni
				user.get.phone_number = data.phone_number
				user.get.mail = data.mail

				// If remove picture request
				if (request.body.dataParts.exists(_._1 == "remove-picture")) {
					if (user.get.profile_picture.nonEmpty) {
						Res.delete(user.get.profile_picture, "default", Seq())
					}

					user.get.profile_picture = ""
				} else if (request.body.file("profile-picture").nonEmpty) {
					// If picture update request
					user.get.profile_picture = Res.put(request.body.file("profile-picture").get, "default", Seq())
				}

				userDAO.update(user.get).map { _ =>
					p success Redirect(controllers.ajax.routes.Account.general_settings_form())
						.flashing("redirect-message" -> messagesApi("account.settings.save_success"),
							"redirect-type" -> "alert-success")
				}
			}
		)

		p.future
	}

	/**
		* Process the password change request
		* @return
		*/
	def password_change = Authenticated.async(parse.multipartFormData) { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		passwordChangeForm.bindFromRequest.fold(
			errors => {
				val accountSettings = new AccountGeneralSettings(user.get.mail, user.get.name, user.get.last_name, user.get.DNI, user.get.phone_number)
				p success Ok(views.html.ajax.account_settings(accountSettingsForm.fill(accountSettings), errors, user.get))
			},
			(data: AccountChangePassword) => {
				// Check if the old password is correct
				if (user.get.password == util.Hashing.md5(data.old_password)) {
					// Update the password
					user.get.password = util.Hashing.md5(data.password)
					userDAO.update(user.get).map { _ =>
						p success Redirect(controllers.ajax.routes.Account.general_settings_form())
							.flashing("redirect-message" -> messagesApi("account.settings.password_change_success"),
								"redirect-type" -> "alert-success")
					}
				} else {
					p success Redirect(controllers.ajax.routes.Account.general_settings_form())
						.flashing("redirect-message" -> messagesApi("account.settings.password_change_bad_auth"),
							"redirect-type" -> "alert-danger")
				}
			}
		)

		p.future
	}

	/**
		* Display the address management page
		* @return
		*/
	def address_management_list = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		addressDAO.getUserAddresses(user.get.id.get).map { addresses =>
			p success Ok(views.html.ajax.account_address_list(user.get, addresses))
		}

		p.future
	}

	/**
		* Display the add address form
		* @return
		*/
	def address_management_add_form = Authenticated { implicit request =>
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		Ok(views.html.ajax.account_address_add(addressAddForm, user.get))
	}

	/**
		* Process the add address form
		* @return
		*/
	def address_management_add = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		addressAddForm.bindFromRequest.fold(
			errors => {
				p success Ok(views.html.ajax.account_address_add(errors, user.get))
			},
			(data: AddressAddForm) => {
				val address = new Address(None, user.get.id.get, data.name, data.address, data.postal_code, data.province, data.population)

				addressDAO.exists(data.name).map { exists =>
					if (exists) {
						p success Redirect(controllers.ajax.routes.Account.address_management_add_form())
							.flashing("redirect-message" -> messagesApi("account.address_management.address_already_exists", data.name),
								"redirect-type" -> "alert-danger")
					} else {
						addressDAO.insert(address).map { _ =>
							p success Redirect(controllers.ajax.routes.Account.address_management_add_form())
								.flashing("redirect-message" -> messagesApi("account.address_management.add_success"),
									"redirect-type" -> "alert-success")
						}
					}
				}
			}
		)

		p.future
	}

	/**
		* Display the address edit form
		* @return
		*/
	def address_management_edit_form(id: Int) = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		addressDAO.exists(id).map { exists =>
			if (exists) {
				addressDAO.get(id).map { address =>
					val data = new AddressAddForm(address.name, address.address, address.population, address.province, address.postal_code)
					p success Ok(views.html.ajax.account_address_edit(addressAddForm.fill(data), user.get, id))
				}
			} else {
				p success Redirect(controllers.ajax.routes.Account.address_management_list())
					.flashing("redirect-message" -> messagesApi("account.address_management.address_does_not_exists"),
						"redirect-type" -> "alert-danger")
			}
		}

		p.future
	}

	/**
		* Process the address edit form
		* @param id
		* @return
		*/
	def address_management_edit(id: Int) = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		addressDAO.exists(id).map { exists =>
			if (exists) {
				addressAddForm.bindFromRequest.fold(
					errors => {
						addressDAO.get(id).map { address =>
							p success Ok(views.html.ajax.account_address_edit(errors, user.get, id))
						}
					},
					(data: AddressAddForm) => {
						addressDAO.get(id).map { address =>
							if (data.name != address.name) {
								addressDAO.exists(data.name).map { nameExists =>
									if (nameExists) {
										p success Redirect(controllers.ajax.routes.Account.address_management_edit_form(id))
											.flashing("redirect-message" -> messagesApi("account.address_management.address_already_exists", data.name),
												"redirect-type" -> "alert-danger")
									} else {
										address.name = data.name
										address.address = data.address
										address.population = data.population
										address.province = data.province
										address.postal_code = data.postal_code

										addressDAO.update(address).map { _ =>
											p success Redirect(controllers.ajax.routes.Account.address_management_edit_form(id))
												.flashing("redirect-message" -> messagesApi("account.address_management.edit_success"),
													"redirect-type" -> "alert-success")
										}
									}
								}
							} else {
								address.address = data.address
								address.population = data.population
								address.province = data.province
								address.postal_code = data.postal_code

								addressDAO.update(address).map { _ =>
									p success Redirect(controllers.ajax.routes.Account.address_management_edit_form(id))
										.flashing("redirect-message" -> messagesApi("account.address_management.edit_success"),
											"redirect-type" -> "alert-success")
								}
							}
						}
					}
				)
			} else {
				p success Redirect(controllers.ajax.routes.Account.address_management_list())
					.flashing("redirect-message" -> messagesApi("account.address_management.address_does_not_exists"),
						"redirect-type" -> "alert-danger")
			}
		}

		p.future
	}

	def address_management_delete(id: Int) = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		addressDAO.exists(id).map { exists =>
			if (exists) {
				addressDAO.delete(id).map { _ =>
					p success Redirect(controllers.ajax.routes.Account.address_management_list())
						.flashing("redirect-message" -> messagesApi("account.address_management.delete_success"),
							"redirect-type" -> "alert-success")
				}
			} else {
				p success Redirect(controllers.ajax.routes.Account.address_management_list())
					.flashing("redirect-message" -> messagesApi("account.address_management.address_does_not_exists"),
						"redirect-type" -> "alert-danger")
			}
		}

		p.future
	}

	/**
		* Display the conversation management page
		* @return
		*/
	def conversation_list = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		conversationDAO.getUserConversations(user.get.id.get).map { conversations =>
			p success Ok(views.html.ajax.account_conversation_list(user.get, conversations))
		}

		p.future
	}

	/**
		* Display the conversation with the given hash
		* @param hash
		* @return
		*/
	def conversation_view(hash: String) = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		conversationDAO.getComplexConversation(hash).map { conversation =>
			p success Ok(views.html.ajax.account_conversation_view(addMessageForm, user.get, conversation))
		}

		p.future
	}

	/**
		* Adds a new message to the conversation
		* @param hash
		* @return
		*/
	def conversation_add_message(hash: String) = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		addMessageForm.bindFromRequest.fold(
			errors => {
				conversationDAO.getComplexConversation(hash).map { conversation =>
					p success Ok(views.html.ajax.account_conversation_view(errors, user.get, conversation))
				}
			},
			data => {
				conversationDAO.insertMessage(new Message(None, hash, data.message, user_id = user.get.id.get)).map { _ =>
					p success Redirect(controllers.ajax.routes.Account.conversation_view(hash))
				}
			}
		)

		p.future
	}

	/**
		* Displays the new conversation form
		* @return
		*/
	def conversation_new_form = Authenticated { implicit request =>
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]
		Ok(views.html.ajax.account_conversation_new(newConversationForm, user.get))
	}

	/**
		* Process the new conversation form
		* @return
		*/
	def conversation_new = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		newConversationForm.bindFromRequest.fold(
			errors => {
				p success Ok(views.html.ajax.account_conversation_new(errors, user.get))
			},
			data => {
				conversationDAO.generateHash.map { hash =>
					conversationDAO.insertConversation(new Conversation(None, data.title, hash = hash)).map { _ =>
						conversationDAO.insertMessage(new Message(None, hash, data.message, user_id = user.get.id.get)).map { _ =>
							p success Redirect(controllers.ajax.routes.Account.conversation_new_form())
								.flashing("redirect-message" -> messagesApi("account.conversations.creation_success"),
									"redirect-type" -> "alert-success")
						}
					}
				}
			}
		)

		p.future
	}

	/**
		* Displays the order management view
		* @return
		*/
	def order_management = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		orderDAO.getOrders(user.get.id.get).map { orders =>
			p success Ok(views.html.ajax.account_order_list(user.get, orders))
		}

		p.future
	}

	/**
		* Process the cancellation request of the given order
		* @param hash
		* @return
		*/
	def order_cancel(hash: String) = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		orderDAO.exists(hash).map { exists =>
			if (exists) {
				orderDAO.getOrder(hash).map { order =>
					order.state = "canceled"

					orderDAO.updateOrder(order).map { _ =>
						p success Redirect(controllers.ajax.routes.Account.order_management())
							.flashing("redirect-message" -> messagesApi("account.orders.cancel_success"),
								"redirect-type" -> "alert-success")
					}
				}
			} else {
				p success Redirect(controllers.ajax.routes.Account.order_management())
					.flashing("redirect-message" -> messagesApi("account.orders.order_does_not_exists"),
						"redirect-type" -> "alert-danger")
			}
		}

		p.future

	}
}
