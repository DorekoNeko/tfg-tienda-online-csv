package controllers.ajax

import javax.inject.Inject

import actions.builders.Authenticated
import controllers.ajax
import dao._
import org.joda.time.DateTime
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Controller, _}
import settings.IGeneralSettings

import scala.concurrent.{ExecutionContext, Future, Promise}

/**
	* Created by Daniel on 24/05/16.
	*/
object Cart {
	implicit val setAddressForm = Form(
		single(
			"shipping-address" -> number
		)
	)
}

class Cart @Inject()(
	protected val cartDAO: ICartDAO,
	protected val orderDAO: IOrderDAO,
	protected val productDAO: IProductDAO,
	protected val addressDAO: IAddressDAO,
	protected val generalSettings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	import Cart._

	/**
		* Displays the checkout form in two steps
		* @param step
		* @return
		*/
	def checkout_form(step: Int) = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		if (step == 1) {
			cartDAO.get(user.get.id.get).map { cart =>
				p success Ok(views.html.ajax.checkout_cart_list(cart))
			}
		} else if (step == 2) {
			addressDAO.getUserAddresses(user.get.id.get).map { addresses =>
				p success Ok(views.html.ajax.checkout_cart_step_2(addresses))
			}
		} else {
			p success NotFound
		}

		p.future
	}

	/**
		* Process the address definition of the second step
		* @return
		*/
	def checkout_define_address = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		setAddressForm.bindFromRequest.fold(
			errors => {
				p success Redirect(ajax.routes.Cart.checkout_form(2))
			},
			data => {
				addressDAO.exists(data).map { exists =>
					if (exists) {
						addressDAO.get(data).map { address =>
							cartDAO.get(user.get.id.get).map { cart =>
								orderDAO.generateHash.map { hash =>
									val newOrder = new Order(
										None,
										user.get.id.get,
										DateTime.now(),
										"preparing",
										hash,
										settings.shipment_costs,
										cart.items.map(p => p.product.calculatedPrice * p.quantity).sum + settings.shipment_costs,
										user.get.name + " " + user.get.last_name,
										address.address,
										address.postal_code,
										address.province,
										address.population
									)

									orderDAO.insertOrder(newOrder).map { _ =>
										orderDAO.getOrder(hash).map { order =>
											val serialized = {
												var accum = Future {
													()
												}

												for (item <- cart.items) {
													accum = accum flatMap { _ =>
														orderDAO.insertOrderItem(new OrderItem(None, order.id.get, item.quantity, order.hash, item.product.name.toString, item.product.priceWithDiscountsNoVAT, item.product.vat.percentage)).map { _ =>
															Unit
														}
													}
												}

												accum
											}

											serialized.map { _ =>
												cartDAO.empty(user.get.id.get).map { _ =>
													p success Redirect(ajax.routes.Cart.checkout_complete())
												}
											}
										}
									}
								}
							}
						}
					} else {
						p success Redirect(ajax.routes.Cart.checkout_form(2))
							.flashing("redirect-message" -> messagesApi("checkout.shipping_information.error_address_does_not_exists"),
								"redirect-type" -> "alert-danger")
					}
				}
			}
		)

		p.future
	}

	/**
		* Display the checkout complete view
		* @return
		*/
	def checkout_complete = Authenticated { implicit request =>
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]
		Ok(views.html.ajax.checkout_complete())
	}

	/**
		* Adds the given product to the current user session
		* @return
		*/
	def add_product(hash: String, quantity: Int) = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		productDAO.exists(hash).map { exists =>
			if (exists) {
				productDAO.get(hash).map { product =>
					val q = if (quantity < 1) 1 else quantity
					cartDAO.getItems(user.get.id.get).map { items =>
						if (items.exists(_.product_id == product.id.get)) {
							val item = items.find(_.product_id == product.id.get).get
							item.quantity += q
							cartDAO.updateItem(item).map { _ =>
								p success Ok("Product added.")
							}
						} else {
							cartDAO.insert(new CartItem(None, user.get.id.get, product.id.get, q)).map { _ =>
								p success Ok("Product added.")
							}
						}
					}
				}
			} else {
				p success BadRequest("The product reference does not exists.")
			}
		}

		p.future
	}

	/**
		* Removes a product from the cart of the current user session
		* @param hash
		* @return
		*/
	def remove_product(hash: String) = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		productDAO.exists(hash).map { exists =>
			if (exists) {
				productDAO.get(hash).map { product =>
					cartDAO.deleteUserItem(user.get.id.get, product.id.get).map { _ =>
						p success Ok("Deleted.")
					}
				}
			} else {
				p success BadRequest("The product reference does not exists.")
			}
		}

		p.future
	}

	/**
		* Empty the cart of the current user session
		* @return
		*/
	def empty = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		cartDAO.empty(user.get.id.get).map { _ =>
			p success Ok("Empty.")
		}

		p.future
	}

}
