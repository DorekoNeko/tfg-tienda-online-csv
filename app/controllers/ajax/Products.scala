package controllers.ajax

import javax.inject.Inject

import actions.builders.Authenticated
import dao.{ICategoryDAO, IProductDAO, ProductReview, User}
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import settings.IGeneralSettings
import util.UserSession

import scala.concurrent.{ExecutionContext, Promise}

/**
	* Created by Daniel on 18/05/16.
	*/
object Products {
	case class AddReviewForm(review: String, valoration: Int)

	val addReviewForm = Form(
		mapping(
			"body" -> nonEmptyText,
			"valoration" -> number(min = 1)
		)(AddReviewForm.apply)(AddReviewForm.unapply)
	)
}

class Products @Inject()(
	protected val productDAO: IProductDAO,
	protected val categoryDAO: ICategoryDAO,
	protected val generalSettings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	import Products._

	/**
		* Displays the product reviews view
		* @param ref
		* @return
		*/
	def reviews(ref: String) = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings

		if (UserSession.validateWithSession(request.session) == util.Validation.Valid) {
			val user = UserSession.get(request.session.get("token").get).get.get("user").asInstanceOf[Option[User]]
			productDAO.getProductReviews(ref).map { reviews =>
				p success Ok(views.html.ajax.customer_reviews(ref, reviews, user))
			}
		} else {
			productDAO.getProductReviews(ref).map { reviews =>
				p success Ok(views.html.ajax.customer_reviews(ref, reviews))
			}
		}

		p.future
	}

	/**
		* Adds a review to the given product
		* @param ref
		* @return
		*/
	def review_add(ref: String) = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").get.asInstanceOf[User]

		addReviewForm.bindFromRequest.fold(
			errors => {
				p success Redirect(controllers.ajax.routes.Products.details(ref))
			},
			data => {
				productDAO.get(ref).map { product =>
					val review = new ProductReview(None, product.id.get, product.reference, user.id.get, data.review, data.valoration)
					productDAO.insertReview(review).map { _ =>
						p success Redirect(controllers.ajax.routes.Products.details(ref))
					}
				}
			}
		)

		p.future
	}

	/**
		* Display the product details view
		* @param ref
		* @return
		*/
	def details(ref: String) = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings

		productDAO.exists(ref).map { exists =>
			if (exists) {
				productDAO.getComplex(ref).map { product =>
					categoryDAO.getParents(product.category.id.get).map { category_map =>
						p success Ok(views.html.ajax.product_details(product, category_map))
					}
				}
			} else {
				p success Ok(messagesApi("products.product_ref_does_not_exists"))
			}
		}

		p.future
	}

}
