package controllers.ajax

import java.net.URLDecoder
import javax.inject._

import actors.SessionCleaner._
import akka.actor._
import dao._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.Json
import play.api.mvc._
import settings.IGeneralSettings
import util.Pagination

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Promise}

class Application @Inject()(
	protected val productDAO: IProductDAO,
	protected val sectionsDAO: ISectionDAO,
	protected val categoryDAO: ICategoryDAO,
	protected val conversationDAO: IConversationDAO,
	protected val generalSettings: IGeneralSettings,
	val messagesApi: MessagesApi,
	protected val system: ActorSystem,
	@Named("SessionCleaner") actorCleaner: ActorRef
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	val actorCleanerSchedule = system.scheduler.schedule(
		0.microseconds, 1.hours, actorCleaner, Clean)

	/**
		* Displays the home page
		* @return
		*/
  def index = Action.async { implicit request =>
    val p = Promise[Result]
		implicit val settings = generalSettings.getGeneralSettings

		val q = for {
			products <- productDAO.newest()
			count <- productDAO.count
		} yield (products, count)

		q.map { t =>
			val products = t._1
			val pagination = new Pagination(total_rows = t._2)

			p success Ok(views.html.ajax.index(products, pagination))
		}

		p.future
	}

	/**
		* Displays a the section with the given name
		* @param name
		* @return
		*/
	def section(name: String) = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings

		val decoded_name = URLDecoder.decode(name, "utf-8")
		sectionsDAO.exists(decoded_name).map { exists =>
			if (exists) {
				sectionsDAO.get(decoded_name).map { section =>
					p success Ok(views.html.ajax.section(section))
				}
			} else {
				p success Redirect(routes.Application.index())
					.flashing("redirect-message" -> "The section doesn't exists!")
			}
		}

		p.future
	}

	def search(query: String) = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings

		productDAO.search(query).map { results =>
			p success Ok(Json.prettyPrint(Json.toJson(results)))
		}

		p.future
	}

	/**
		* Displays the products within the given category
		* @param normalized_name
		* @return
		*/
	def category_show(normalized_name: String) = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings

		categoryDAO.exists(normalized_name).map { exists =>
			if (exists) {
				categoryDAO.get(normalized_name).map { category =>
					productDAO.countCategoryProducts(category.id.get).map { products_length =>
						if (products_length > 0) {
							productDAO.listFromCategory(category.id.get).map { products =>
								val pagination = new Pagination(
									total_rows = products_length,
									rows_per_page = settings.products_per_page)
								p success Ok(views.html.ajax.category_show(category, products, pagination))
							}
						} else {
							p success Ok(views.html.ajax.category_empty())
						}
					}
				}
			} else {
				p success Ok(messagesApi("categories.category_does_not_exists"))
			}
		}

		p.future
	}

	/**
		* Displays the products within the given category with a pager
		* @param normalized_name
		* @param page
		* @param order
		* @return
		*/
	def category_show_pager(normalized_name: String, page: Int, order: String) = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings

		val take = if (page-1 <= 0) 0 else (page-1) * settings.products_per_page
		categoryDAO.exists(normalized_name).map { exists =>
			if (exists) {
				categoryDAO.get(normalized_name).map { category =>
					productDAO.countCategoryProducts(category.id.get).map { products_length =>
						if (products_length > 0) {
							productDAO.listFromCategory(category.id.get, Some(settings.products_per_page), Some(take)).map { products =>
								val pagination = new Pagination(page, products_length, settings.products_per_page)
								p success Ok(views.html.ajax.category_show(category, products, pagination))
							}
						} else {
							p success Ok(views.html.ajax.category_empty())
						}
					}
				}
			} else {
				p success Ok(messagesApi("categories.category_does_not_exists"))
			}
		}

		p.future
	}

	/**
		* Displays the base category menu
		*/
	def category_menu_base = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings

		categoryDAO.allBase.map { categories =>
			p success Ok(views.html.ajax.category_menu(categories))
		}

		p.future
	}

	/**
		* Displays the sub category menu for the given category
		* @param normalized_name
		* @return
		*/
	def category_menu(normalized_name: String) = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings

		categoryDAO.exists(normalized_name).map { exists =>
			if (exists) {
				categoryDAO.get(normalized_name).map { category =>
					categoryDAO.getComplexChildren(category.id.get).map { children =>
						p success Ok(views.html.ajax.category_menu(children, Some(category)))
					}
				}
			} else {
				p success Ok(messagesApi("categories.category_does_not_exists"))
			}
		}

		p.future
	}

	/**
		* Displays the unauthorized view
		* @return
		*/
	def unauthorized = Action { implicit request =>
		Ok(views.html.unauthorized())
	}
}
