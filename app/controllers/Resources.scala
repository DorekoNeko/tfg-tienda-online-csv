package controllers

import javax.inject._

import play.api.Play
import play.api.Play.current
import play.api.mvc.{Action, Controller, Result}
import se.digiplant.res.api.Res
import se.digiplant.scalr.api.{Resizer, Scalr}

import scala.concurrent.Promise

/**
	* Created by Daniel on 18/04/16.
	*/
class Resources @Inject() extends Controller {

	/**
		* Resizes a resource or get the cached version if exists.
		* @param fileuuid
		* @param width
		* @param height
		* @param source
		* @return
		*/
	def resizeWithMode(fileuuid: String, width: Int, height: Int, source: String = "default", mode: String = "AUTOMATIC") = Action.async {
		val p = Promise[Result]
		val f = p.future

		val resizerMode = if (mode.equalsIgnoreCase("automatic")) {
			Resizer.Mode.AUTOMATIC
		} else if (mode.equalsIgnoreCase("width")) {
			Resizer.Mode.FIT_TO_WIDTH
		} else if (mode.equalsIgnoreCase("height")) {
			Resizer.Mode.FIT_TO_HEIGHT
		} else if (mode.equalsIgnoreCase("exact")) {
			Resizer.Mode.FIT_EXACT
		} else if (mode.equalsIgnoreCase("crop")) {
			Resizer.Mode.CROP
		} else {
			Resizer.Mode.AUTOMATIC
		}

		//val notFoundFile = Play.application.getFile(s"public/images/no_image_available.png")
		val file = Scalr.getRes(fileuuid, source, width, height, resizerMode)
		if (file.isDefined) {
			//resource type such as image+png, image+jpg
			val resourceType: String = "image+"+fileuuid.substring(fileuuid.length()-3)
			val source = scala.io.Source.fromFile(file.get)(scala.io.Codec.ISO8859)
			val byteArray = source.map(_.toByte).toArray
			source.close()

			p success Ok(byteArray).as(resourceType)
		} else {
			/*
			val resourceType: String = "image+png"
			val source = scala.io.Source.fromFile(notFoundFile)(scala.io.Codec.ISO8859)
			val byteArray = source.map(_.toByte).toArray
			source.close()

			p success Ok(byteArray).as(resourceType)
			*/

			p success Redirect(routes.Assets.versioned("images/no_image_available.png"))
		}

		f
	}

	def resize(fileuuid: String, width: Int, height: Int, source: String = "default") = resizeWithMode(fileuuid, width, height, source, "automatic")

	def get(fileuuid: String, source: String = "") = Action.async {
		val p = Promise[Result]
		val f = p.future

		val notFoundFile = Play.application.getFile(s"public/images/no_image_available.png")
		val file = Res.get(fileuuid, source, Seq())

		if (file.isDefined) {
			//resource type such as image+png, image+jpg
			val resourceType: String = "image+"+fileuuid.substring(fileuuid.length()-3)
			val source = scala.io.Source.fromFile(file.get)(scala.io.Codec.ISO8859)
			val byteArray = source.map(_.toByte).toArray
			source.close()

			p success Ok(byteArray).as(resourceType)
		} else {
			/*
			val resourceType: String = "image+png"
			val source = scala.io.Source.fromFile(notFoundFile)(scala.io.Codec.ISO8859)
			val byteArray = source.map(_.toByte).toArray
			source.close()

			p success Ok(byteArray).as(resourceType)
			*/

			p success Redirect(routes.Assets.versioned("images/no_image_available.png"))
		}

		f
	}
}
