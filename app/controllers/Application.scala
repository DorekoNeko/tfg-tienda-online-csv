package controllers

import javax.inject._

import actors.SessionCleaner._
import akka.actor._
import controllers.Login._
import dao._
import play.api.Logger
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation._
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.mvc._
import se.digiplant.res.api.Res
import settings.IGeneralSettings
import util.UserSession

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Promise}

object Application {
	import constraints.Password._
	import play.api.Play.current
	import play.api.i18n.Messages.Implicits._

	case class UserRegistration(username: String, password: String, repassword: String, mail: String, name: String, last_name: String, dni: String, phone_number: String)

	val registerPasswordMatchingConstraint: Constraint[UserRegistration] = Constraint("constraints.passwordsmatch")({
		registerData =>
			if (registerData.password == registerData.repassword) {
				play.api.data.validation.Valid
			} else {
				play.api.data.validation.Invalid(Seq(ValidationError(Messages("register.password.donotmatch"))))
			}
	})

	implicit val registerForm = Form(
		mapping(
			"username" -> nonEmptyText,
			"password" -> nonEmptyText.verifying(passwordCheckConstraint),
			"repassword" -> nonEmptyText,
			"mail" -> email,
			"name" -> nonEmptyText,
			"last-name" -> nonEmptyText,
			"dni" -> nonEmptyText,
			"phone-number" -> text
		)(UserRegistration.apply)(UserRegistration.unapply) verifying registerPasswordMatchingConstraint
	)
}

class Application @Inject()(
	protected val userDAO: IUserDAO,
	protected val productDAO: IProductDAO,
	protected val sectionsDAO: ISectionDAO,
	protected val supportedLangsDAO: ISupportedLangDAO,
	protected val generalSettings: IGeneralSettings,
	val messagesApi: MessagesApi,
	protected val system: ActorSystem,
	@Named("SessionCleaner") actorCleaner: ActorRef
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	import Application._

	val actorCleanerSchedule = system.scheduler.schedule(
		0.microseconds, 1.hours, actorCleaner, Clean)

	/**
		* Displays the home page
		* @return
		*/
  def index = Action.async { implicit request =>
    val p = Promise[Result]
		implicit val settings = generalSettings.getGeneralSettings
		implicit val currentLang = util.lang.ClientLang.get(request.cookies, generalSettings.get("default_lang").get.value)

		sectionsDAO.all.map { sections =>
			val supportedLangs = request.acceptLanguages

			if (UserSession.validateWithSession(request.session) == util.Validation.Valid) {
				val user = UserSession.get(request.session.get("token").get).get.get("user").asInstanceOf[Option[User]]
				p success Ok(views.html.index(userLoginForm, sections, supportedLangs, user))
			} else {
				p success Ok(views.html.index(userLoginForm, sections, supportedLangs))
			}
		}

		p.future
	}

	/**
		* Displays the sign up form
		* @return
		*/
	def sign_up_form = Action { implicit request =>
		implicit val settings = generalSettings.getGeneralSettings
		Ok(views.html.sign_up(registerForm))
	}

	/**
		* Process the sign up form
		* @return
		*/
	def sign_up = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings

		registerForm.bindFromRequest.fold(
			errors => {
				p success Ok(views.html.sign_up(errors))
			},
			data => {
				val user = new User(None, data.username, util.Hashing.md5(data.password), data.mail, "", data.name, data.last_name, 0, 0, data.dni, data.phone_number, false)

				userDAO.isUsernameTaken(data.username).map { taken =>
					if (taken) {
						p success Redirect(routes.Application.sign_up_form())
							.flashing("redirect-message" -> messagesApi("sign_up.errors.username_taken"),
								"redirect-type" -> "alert-danger")
					} else {
						userDAO.insert(user).map { _ =>
							// Once inserted, login with the new user
							userDAO.get(data.username).map { user =>
								val session = UserSession.start()
								session.add("user", user)

								p success Redirect(routes.Application.index())
									.withSession("token" -> session.token)
							}
						}
					}
				}
			}
		)

		p.future
	}

	/**
		* Displays the unauthorized view
		* @return
		*/
	def unauthorized = Action { implicit request =>
		Unauthorized(views.html.unauthorized())
	}

	/**
		* Uploads an image.
		* @return
		*/
	def image_upload = Action(parse.multipartFormData) { implicit request =>
		request.body.file("file").map { picture =>
			val asset = Res.put(picture, "default", Seq())
			Ok(asset)
		}.getOrElse {
			Logger.error("Can't upload image. \n" + request.body.files)
			InternalServerError("Seems like the file is missing.")
		}
	}
}
