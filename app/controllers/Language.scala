package controllers

import javax.inject._

import play.api.mvc._

/**
	* Created by Daniel on 26/05/16.
	*/
class Language @Inject()() extends Controller {

	/**
		* Sets the language cookie and redirects to the index.
		* @param lang_code
		* @return
		*/
	def set(lang_code: String) = Action { implicit request =>
		val acceptedCodes = request.acceptLanguages.map(_.code)
		if (acceptedCodes.exists(_.toLowerCase == lang_code.toLowerCase)) {
			Redirect(routes.Application.index()).withCookies(new Cookie("PLAY_LANG", lang_code))
		} else {
			Redirect(routes.Application.index())
		}
	}

}
