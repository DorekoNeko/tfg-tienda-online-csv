package controllers

import javax.inject.Inject

import actions.builders.AuthenticatedWithAdmin
import dao.User
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import settings.IGeneralSettings

import scala.concurrent.ExecutionContext

/**
	* Created by Daniel on 18/04/16.
	*/
class Panel @Inject()(
	protected val settings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {

	/**
		* Displays the view of the panel index (overview)
		* @return
		*/
  def index = AuthenticatedWithAdmin { implicit request =>
		Redirect(panel.routes.Orders.orders())
  }

	/**
		* Displays the user view for the live chat session
		* @return
		*/
	def admin_chat = AuthenticatedWithAdmin { implicit request =>
		implicit val generalSettings = settings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		Ok(views.html.panel.admin_chat(user.get))
	}
}
