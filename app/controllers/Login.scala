package controllers

import javax.inject._

import dao.IUserDAO
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Action, Controller, Result}
import settings.IGeneralSettings
import util.UserSession

import scala.concurrent.{ExecutionContext, Promise}

/**
	* Created by Daniel on 18/04/16.
	*/
object Login {
	case class UserLogin(username: String, password: String)

	implicit val userLoginForm = Form(
		mapping(
			"username" -> nonEmptyText,
			"password" -> nonEmptyText
		)(UserLogin.apply)(UserLogin.unapply)
	)
}

class Login @Inject()(
	protected val userDAO: IUserDAO,
	protected val generalSettings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	import controllers.Login._

	/**
		* Displays the sign in form
		* @return
		*/
	def sign_in_form = Action { implicit request =>
		implicit val settings = generalSettings.getGeneralSettings
		Ok(views.html.sign_in(userLoginForm))
	}

	/**
		* Process the sign in form
		* @return
		*/
	def sign_in = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings

		userLoginForm.bindFromRequest.fold(
			errors => {
				p success Ok(views.html.sign_in(errors))
			},
			data => {
				userDAO.authenticate(data.username, util.Hashing.md5(data.password)).map { result =>
					if (result) {
						userDAO.get(data.username).map { user =>
							val session = UserSession.start()
							session.add("user", user)

							p success Redirect(routes.Application.index())
								.withSession("token" -> session.token)
						}
					} else {
						p success Redirect(routes.Login.sign_in_form())
							.flashing("redirect-message" -> messagesApi("login.bad_credentials"),
								"redirect-type" -> "alert-danger")
					}
				}
			}
		)

		p.future
	}

	/**
		* Process the sign out request
		* @return
		*/
	def sign_out = Action { implicit request =>
		if (request.session.get("token").nonEmpty) {
			UserSession.end(UserSession.get(request.session.get("token").get).get)
		}

		Redirect(routes.Application.index()).withNewSession
	}
}
