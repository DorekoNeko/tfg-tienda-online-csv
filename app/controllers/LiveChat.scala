package controllers

import javax.inject._

import _root_.util.UserSession
import _root_.util.Validation._
import actors.LiveChatSocket
import akka.actor._
import dao.User
import org.joda.time.DateTime
import play.api.Play.current
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json._
import play.api.mvc._
import play.api.{Logger, Play}
import settings.IGeneralSettings

import scala.concurrent.{ExecutionContext, Promise}

object LiveChat {
	case class ChatMessage(id: Option[Int], from_id: Int, to_id: Int, message: String, timestamp: DateTime = DateTime.now())
}

@Singleton
class LiveChat @Inject() (
	system: ActorSystem,
	@Named("LiveChatHandler") implicit val liveChat: ActorRef,
	protected val settings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	val logger = Logger("LiveChat.Controller")

	/**
		* Lifts the request to a websocket request
		* and handles it with a LiveChatSocket actor
		* @return
		*/
	def chatSocket = WebSocket.tryAcceptWithActor[JsValue, JsValue] { implicit request =>
		val p = Promise[Either[Result, WebSocket.HandlerProps]]()

		if (Play.configuration.getBoolean("developer_mode").getOrElse(false)) {
			if (UserSession.validateWithSession(request.session) == Valid) {
				p success(UserSession.validateWithSession(request.session) match {
					case Valid =>
						implicit val user: User = UserSession.get(request.session.get("token").get).get.get("user").get.asInstanceOf[User]
						logger.info("Session validated.")
						Right(LiveChatSocket.props)
					case _ =>
						logger.info("Session invalidated.")
						Left(Forbidden)
				})
			} else {
				val dummy = UserSession.startDummySession
				implicit val user: User = dummy.get("user").get.asInstanceOf[User]
				logger.info("Session validated.")
				p success Right(LiveChatSocket.props)
			}
		} else {
			p success(UserSession.validateWithSession(request.session) match {
				case Valid =>
					implicit val user: User = UserSession.get(request.session.get("token").get).get.get("user").get.asInstanceOf[User]
					logger.info("Session validated.")
					Right(LiveChatSocket.props)
				case _ =>
					logger.info("Session invalidated.")
					Left(Forbidden)
			})
		}

		p.future
	}

	/**
		* Displays the view for the chat
		* @return
		*/
	def chatView = Action { implicit request =>
		implicit val generalSettings = settings.getGeneralSettings
		Ok(views.html.ajax.chat_view())
	}

}