package controllers.templates

import javax.inject.Inject

import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._

class Panel @Inject()(
	val messagesApi: MessagesApi
) extends Controller with I18nSupport {

	/**
		* Display the file upload template
		* @return
		*/
	def file_upload = Action {
		Ok(views.html.templates.panel.file_upload())
	}

}
