package controllers.json

import javax.inject.Inject

import actions.builders.Authenticated
import dao.{ICartDAO, User}
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.Json
import play.api.mvc.Controller
import settings.IGeneralSettings

import scala.concurrent.ExecutionContext

/**
	* Created by Daniel on 24/05/16.
	*/
class Cart @Inject()(
	protected val cartDAO: ICartDAO,
	protected val generalSettings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {

	/**
		* Returns the cart items of the given user
		* @return
		*/
	def user_cart = Authenticated.async { implicit request =>
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		cartDAO.get(user.get.id.get).map { cart =>
			Ok(Json.prettyPrint(Json.toJson(cart.items)))
		}
	}

}
