package controllers

import javax.inject._

import actions.builders.Authenticated
import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import dao.{IOrderDAO, IUserDAO, User}
import play.api.data.validation.{Constraint, ValidationError}
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.mvc.{Controller, Result}
import settings.IGeneralSettings

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Promise}

object Account {
	import constraints.Password._
	import play.api.Play.current
	import play.api.data.Form
	import play.api.data.Forms._
	import play.api.i18n.Messages.Implicits._

	case class AccountGeneralSettings(mail: String, name: String, last_name: String, dni: String, phone_number: String)
	implicit val accountSettingsForm = Form(
		mapping(
			"mail" -> email,
			"name" -> nonEmptyText,
			"last-name" -> nonEmptyText,
			"dni" -> nonEmptyText,
			"phone-number" -> text
		)(AccountGeneralSettings.apply)(AccountGeneralSettings.unapply)
	)

	case class AccountChangePassword(old_password: String, password: String, repassword: String)

	val registerPasswordMatchingConstraint: Constraint[AccountChangePassword] = Constraint("constraints.passwordsmatch")({
		data =>
			if (data.password == data.repassword) {
				play.api.data.validation.Valid
			} else {
				play.api.data.validation.Invalid(Seq(ValidationError(Messages("register.password.donotmatch"))))
			}
	})

	implicit val passwordChangeForm = Form(
		mapping(
			"old-password" -> nonEmptyText,
			"new-password" -> nonEmptyText.verifying(passwordCheckConstraint),
			"new-password-repeat" -> nonEmptyText
		)(AccountChangePassword.apply)(AccountChangePassword.unapply) verifying registerPasswordMatchingConstraint
	)

	case class AddressAddForm(name: String, address: String, population: String, province: String, postal_code: String)
	implicit val addressAddForm = Form(
		mapping(
			"name" -> nonEmptyText,
			"address" -> nonEmptyText,
			"population" -> nonEmptyText,
			"province" -> nonEmptyText,
			"postal-code" -> nonEmptyText
		)(AddressAddForm.apply)(AddressAddForm.unapply)
	)

	case class AddMessageForm(message: String)
	val addMessageForm = Form(
		mapping(
			"message" -> nonEmptyText
		)(AddMessageForm.apply)(AddMessageForm.unapply)
	)

	case class NewConversationForm(title: String, message: String)
	val newConversationForm = Form(
		mapping(
			"title" -> nonEmptyText,
			"message" -> nonEmptyText
		)(NewConversationForm.apply)(NewConversationForm.unapply)
	)
}

class Account @Inject()(
	protected val userDAO: IUserDAO,
	protected val orderDAO: IOrderDAO,
	protected val generalSettings: IGeneralSettings,
	val messagesApi: MessagesApi,
	system: ActorSystem,
	@Named("LiveChatHandler") implicit val chatHandler: ActorRef
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	import actors.LiveChatHandler._

	/**
		* Displays the proforma invoice for the given order
		* @param order_hash
		* @return
		*/
	def proforma_invoice(order_hash: String) = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val settings = generalSettings.getGeneralSettings

		orderDAO.exists(order_hash).map { exists =>
			if (exists) {
				orderDAO.getOrder(order_hash).map { order =>
					orderDAO.getItems(order_hash).map { items =>
						p success Ok(views.html.proforma_invoice(order, items))
					}
				}
			} else {
				p success Redirect(routes.Application.index())
			}
		}

		p.future
	}

	/**
		* Displays the user view for the live chat session
		* @return
		*/
	def user_chat = Authenticated.async { implicit request =>
		implicit val timeout = Timeout(5.seconds)
		implicit val settings = generalSettings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		(chatHandler ? AdminExists).mapTo[Boolean].map { message =>
			Ok(views.html.user_chat(message))
		}
	}
}
