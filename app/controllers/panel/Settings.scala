package controllers.panel

import javax.inject._

import actions.builders.AuthenticatedWithAdmin
import dao.User
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Controller, Result}
import se.digiplant.res.api.Res
import settings.{GeneralSettings, IGeneralSettings}
import util.Formatters._

import scala.concurrent.{ExecutionContext, Future, Promise}

/**
	* Created by Daniel on 27/04/16.
	*/
object Settings {
	implicit val generalSettingsForm = Form(
		mapping(
			"shop-name" -> nonEmptyText,
			"shop-description" -> text,
			"company-name" -> nonEmptyText,
			"address" -> nonEmptyText,
			"nif-cif" -> nonEmptyText,
			"products-per-page" -> number(min = 8),
			"activate-slider" -> boolean,
			"legal-info" -> text
		)(
			(shopName, shopDescription, companyName, address, nifCif, productsPerPage, activateSlider, legalInfo) =>
				new GeneralSettings(shopName, shopDescription, companyName, address, nifCif, productsPerPage, legalInfo, activateSlider)
		)(
			g =>
				Some((g.shop_name, g.shop_description, g.company_name, g.address, g.nif_cif, g.products_per_page, g.slider, g.legal_info))
		)
	)

	case class PaymentInfoForm(info: String, bank_entity: String, bank_account: String)
	val paymentInfoForm = Form(
		mapping(
			"info" -> text,
			"bank-entity" -> nonEmptyText,
			"bank-account" -> nonEmptyText
		)(PaymentInfoForm.apply)(PaymentInfoForm.unapply)
	)

	case class ShipmentInfoForm(info: String, price: Double)
	val shipmentInfoForm = Form(
		mapping(
			"shipment-info" -> text,
			"shipment-price" -> of(doubleFormat)
		)(ShipmentInfoForm.apply)(ShipmentInfoForm.unapply)
	)
}

class Settings @Inject()(
	protected val settings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	import Settings._

	/**
		* Display the general settings
		* @return
		*/
	def general_settings = AuthenticatedWithAdmin.async { implicit request =>
		implicit val generalSettings = settings.getGeneralSettings
		settings.getObject.map { generals =>
			Ok(views.html.panel.general_settings(generalSettingsForm.fill(generalSettings)))
		}
	}

	/**
		* Process the general settings form
		* @return
		*/
	def general_settings_save = AuthenticatedWithAdmin.async { implicit request =>
		val p = Promise[Result]()
		implicit val generalSettings = settings.getGeneralSettings

		generalSettingsForm.bindFromRequest.fold(
			error => {
				p success Ok(views.html.panel.general_settings(error))
			},
			data => {
				val multipart = request.body.asMultipartFormData
				val logo = Future {
					val inception = Promise[Unit]()
					if (multipart.isDefined) {
						val image = multipart.get.file("logo-image")
						if (image.isDefined) {
							settings.set("logo", value = Res.put(image.get, "default", Seq())).map(_ => inception success Unit)
						} else {
							inception success Unit
						}
					} else {
						inception success Unit
					}

					inception.future
				}
				val minimal_logo = {
					val inception = Promise[Unit]()
					if (multipart.isDefined) {
						val image = multipart.get.file("logo-minimal-image")
						if (image.isDefined) {
							settings.set("logo_minimal", value = Res.put(image.get, "default", Seq())).map(_ => inception success Unit)
						} else {
							inception success Unit
						}
					} else {
						inception success Unit
					}

					inception.future
				}
				val shop_cover = Future {
					val inception = Promise[Unit]()
					if (multipart.isDefined) {
						val image = multipart.get.file("shop-cover")
						if (image.isDefined) {
							val previous = settings.get("shop_cover")
							if (previous.isDefined) {
								Res.delete(previous.get.value, "default", Seq())
							}

							settings.set("shop_cover", value = Res.put(image.get, "default", Seq())).map(_ => inception success Unit)
						} else {
							inception success Unit
						}
					} else {
						inception success Unit
					}

					inception.future
				}

				// Updating and refreshing
				val procs = for {
					_ <- settings.set("shop_name", data.shop_name)
					_ <- settings.set("shop_description", data.shop_description)
					_ <- settings.set("company_name", data.company_name)
					_ <- settings.set("address", data.address)
					_ <- settings.set("nif_cif", data.nif_cif)
					_ <- settings.set("legal_info", data.legal_info)
					_ <- settings.set("products_per_page", data.products_per_page.toString)
					_ <- settings.set("use_slider", boolean = data.slider)
					_ <- logo
					_ <- minimal_logo
					_ <- shop_cover
					_ <- settings.refresh
				} yield Unit

				procs.map { _ =>
					p success Redirect(routes.Settings.general_settings())
						.flashing("redirect-message" -> "Settings saved!",
							"redirect-type" -> "alert-success")
				}
			}
		)

		p.future
	}

	/**
		* Display the shipment settings
		* @return
		*/
	def shipment_settings = AuthenticatedWithAdmin.async { implicit request =>
		val p = Promise[Result]()
		implicit val generalSettings = settings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		if (settings.get("shipment_info").isEmpty || settings.get("shipment_costs").isEmpty) {
			if (settings.get("shipment_info").isEmpty)
				settings.set("shipment_info", "")
			if (settings.get("shipment_costs").isEmpty)
				settings.set("shipment_costs", "0.00")

			p success Ok(views.html.panel.shipment_settings(shipmentInfoForm))
		} else {
			val info = settings.get("shipment_info").get
			val price = settings.get("shipment_costs").get
			p success Ok(views.html.panel.shipment_settings(shipmentInfoForm.fill(new ShipmentInfoForm(info.value, price.value.toDouble))))
		}

		p.future
	}

	/**
		* Process the shipment info form
		* @return
		*/
	def shipment_settings_save = AuthenticatedWithAdmin.async { implicit request =>
		val p = Promise[Result]()
		implicit val generalSettings = settings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		shipmentInfoForm.bindFromRequest.fold(
			errors => {
				p success Ok(views.html.panel.shipment_settings(errors))
			},
			data => {
				val sets = for {
					_ <- settings.set("shipment_info", data.info)
					_ <- settings.set("shipment_costs", data.price.toString)
				} yield Unit

				sets.map { _ =>
					p success Redirect(controllers.panel.routes.Settings.shipment_settings())
						.flashing("redirect-message" -> "Settings saved!",
							"redirect-type" -> "alert-success")
				}
			}
		)

		p.future
	}

	/**
		* Display the payment settings
		* @return
		*/
	def payment_settings = AuthenticatedWithAdmin.async { implicit request =>
		val p = Promise[Result]()
		implicit val generalSettings = settings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		if (settings.get("payment_info").isEmpty || settings.get("bank_account_entity").isEmpty || settings.get("bank_account").isEmpty) {
			if (settings.get("payment_info").isEmpty)
				settings.set("payment_info", "")
			if (settings.get("bank_account_entity").isEmpty)
				settings.set("bank_account_entity", "")
			if (settings.get("bank_account").isEmpty)
				settings.set("bank_account", "")

			p success Ok(views.html.panel.payment_settings(paymentInfoForm))
		} else {
			val info = settings.get("payment_info").get
			val entity = settings.get("bank_account_entity").get
			val account = settings.get("bank_account").get
			p success Ok(views.html.panel.payment_settings(paymentInfoForm.fill(new PaymentInfoForm(info.value, entity.value, account.value))))
		}

		p.future
	}

	/**
		* Process the payment settings form
		* @return
		*/
	def payment_settings_save = AuthenticatedWithAdmin.async { implicit request =>
		val p = Promise[Result]()
		implicit val generalSettings = settings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		paymentInfoForm.bindFromRequest.fold(
			errors => {
				p success Ok(views.html.panel.payment_settings(errors))
			},
			data => {
				val sets = for {
					_ <- settings.set("payment_info", data.info)
					_ <- settings.set("bank_account_entity", data.bank_entity)
					_ <- settings.set("bank_account", data.bank_account)
				} yield Unit

				sets.map { _ =>
					p success Redirect(controllers.panel.routes.Settings.payment_settings())
						.flashing("redirect-message" -> "Settings saved!",
							"redirect-type" -> "alert-success")
				}
			}
		)

		p.future
	}
}
