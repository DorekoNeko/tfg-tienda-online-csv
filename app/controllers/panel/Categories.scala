package controllers.panel

import java.net.URLEncoder
import javax.inject._

import dao._
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Action, Controller, Result}
import settings.IGeneralSettings

import scala.concurrent.{ExecutionContext, Promise}

/**
	* Created by Daniel on 27/04/16.
	*/
object Categories {
	case class AddCategoryForm(name: String, parent: Int, order: Int, normalized: String)
	implicit val addCategoryForm = Form(
		mapping (
			"name" -> nonEmptyText,
			"parent"-> number,
			"order" -> number,
			"normalized" -> nonEmptyText
		)(AddCategoryForm.apply)(AddCategoryForm.unapply)
	)
}

class Categories @Inject()(
	protected val supportedLangDAO: ISupportedLangDAO,
	protected val langItemDAO: ILangItemDAO,
	protected val categoryDAO: ICategoryDAO,
	protected val settings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	import Categories._

	def categories = Action.async { implicit request =>
		val p = Promise[Result]
		val f = p.future

		categoryDAO.all.map { categories =>
			p.success(Ok(views.html.panel.categories(addCategoryForm, categories)))
		}

		f
	}

	def category_delete(id: Int) = Action.async { implicit request =>
		val p = Promise[Result]
		val f = p.future

		categoryDAO.exists(id).map { resultado =>
			if (resultado) {
				categoryDAO.delete(id).map { valor =>
					p success Redirect(routes.Categories.categories()).
						flashing("redirect-message" -> "Category successfully deleted.",
							"redirect-type" -> "alert-success")
				}
			} else {
				p success Redirect(routes.Categories.categories()).
					flashing("redirect-message" -> "Category id does not exists.",
						"redirect-type" -> "alert-danger")
			}
		}


		f
	}

	def category_add = Action.async { implicit request =>
		val p = Promise[Result]
		val f = p.future

		addCategoryForm.bindFromRequest.fold(
			errors => {
				categoryDAO.all.map { categories =>
					p success Ok(views.html.panel.categories(errors, categories))
				}
			},
			data => {
				val procs = for {
					default <- supportedLangDAO.getDefault
					hash <- langItemDAO.generateHash
					_ <- langItemDAO.insert(new LangItem(None, hash, default.short, data.name))
					_ <- categoryDAO.insert(new Category(None, hash, data.order, data.parent, URLEncoder.encode(data.normalized, "utf-8")))
				} yield Unit

				procs.map { _ =>
					p success Redirect(routes.Categories.categories()).
						flashing("redirect-message" -> "Category successfully added.",
							"redirect-type" -> "alert-success")
				}
			}
		)
		f
	}

}
