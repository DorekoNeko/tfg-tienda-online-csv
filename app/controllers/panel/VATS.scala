package controllers.panel

import javax.inject._

import dao.{IVATDAO, VAT}
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Action, Controller, Result}
import settings.IGeneralSettings
import util.Pagination

import scala.concurrent.{ExecutionContext, Promise}

/**
	* Created by Daniel on 27/04/16.
	*/
object VATS {
	import util.Formatters._
	case class VATForm(name: String, percentage: Double)
	implicit val addVATForm = Form(
		mapping(
			"name" -> nonEmptyText,
			"percent" -> of(doubleFormat)
		)(VATForm.apply)(VATForm.unapply)
	)
}

class VATS @Inject()(
	protected val vatDAO: IVATDAO,
	protected val settings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	import VATS._

	/**
		* Displays the VAT management section
		* @return
		*/
	def vats = Action.async { implicit request =>
		val p = Promise[Result]()

		vatDAO.list(Some(4)).map { vats =>
			vatDAO.count.map { total_vats =>
				val pagination = new Pagination(1, total_vats, 4)
				p success Ok(views.html.panel.vats(addVATForm, vats, pagination))
			}
		}

		p.future
	}

	/**
		* Displays the VAT management section with pager
		* @param page
		* @param order
		* @return
		*/
	def vats_pager(page: Int, order: String) = Action.async { implicit request =>
		val p = Promise[Result]()

		val take = if (page-1 <= 0) 0 else (page-1) * 4
		vatDAO.list(Some(4), Some(take), order).map { vats =>
			vatDAO.count.map { total_vats =>
				val pagination = new Pagination(page, total_vats, 4)
				p success Ok(views.html.panel.vats(addVATForm, vats, pagination))
			}
		}

		p.future
	}

	/**
		* Adds a new VAT
		* @return
		*/
	def vats_add = Action.async { implicit request =>
		val p = Promise[Result]()

		addVATForm.bindFromRequest.fold(
			errors => {
				vatDAO.all.map { vats =>
					vatDAO.count.map { total_vats =>
						val pagination = new Pagination(1, total_vats, 4)
						p success Ok(views.html.panel.vats(errors, vats, pagination))
					}
				}
			}, data => {
				vatDAO.exists(data.name).map { exists =>
					if (exists) {
						p success Redirect(routes.VATS.vats())
							.flashing("redirect-message" -> messagesApi("vats.errors.vat_already_exists"),
								"redirect-type" -> "alert-danger")
					} else {
						vatDAO.insert(new VAT(None, data.name, data.percentage)).map { _ =>
							p success Redirect(routes.VATS.vats())
								.flashing("redirect-message" -> messagesApi("vats.messages.add_success"),
									"redirect-type" -> "alert-success")
						}
					}
				}
			}
		)

		p.future
	}

	/**
		* Deletes a VAT
		* @param id
		* @return
		*/
	def vats_delete(id: Int) = Action.async { implicit request =>
		val p = Promise[Result]()

		vatDAO.exists(id).map { exists =>
			if (exists) {
				vatDAO.delete(id).map { _ =>
					p success Redirect(routes.VATS.vats())
						.flashing("redirect-message" -> messagesApi("vats.messages.delete_success"),
							"redirect-type" -> "alert-success")
				}
			} else {
				p success Redirect(routes.VATS.vats())
					.flashing("redirect-message" -> messagesApi("vats.errors.vat_does_not_exists"),
						"redirect-type" -> "alert-danger")
			}
		}

		p.future
	}

}
