package controllers.panel

import javax.inject.Inject

import actions.builders.AuthenticatedWithAdmin
import dao.IOrderDAO
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Controller, _}
import settings.IGeneralSettings
import util.Pagination

import scala.concurrent.{ExecutionContext, Promise}

/**
	* Created by Daniel on 26/05/16.
	*/
object Orders {

}

class Orders @Inject()(
	protected val orderDAO: IOrderDAO,
	protected val settings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {

	/**
		* Display the orders
		* @return
		*/
	def orders = AuthenticatedWithAdmin.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		orderDAO.list().map { orders =>
			orderDAO.count.map { total =>
				val pagination = new Pagination(total_rows = total-1)
				p success Ok(views.html.panel.orders(orders, pagination))
			}
		}

		p.future
	}

	/**
		* Displays the orders with pager
		* @param page
		* @param order
		* @return
		*/
	def pager(page: Int, order: String) = AuthenticatedWithAdmin.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		val take = if (page-1 <= 0) 0 else (page-1) * 10
		orderDAO.list(Some(10), Some(take), order).map { orders =>
			orderDAO.count.map { total =>
				val pagination = new Pagination(page, total-1)
				p success Ok(views.html.panel.orders(orders, pagination))
			}
		}

		p.future
	}

	/**
		* Changes the state of the given order
		* @param hash
		* @param state
		* @return
		*/
	def set_state(hash: String, state: String) = AuthenticatedWithAdmin.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		orderDAO.exists(hash).map { exists =>
			if (exists) {
				orderDAO.getOrder(hash).map { order =>
					order.state = state
					orderDAO.updateOrder(order).map { _ =>
						p success Ok(messagesApi("panel.orders.order_state_updated"))
					}
				}
			} else {
				p success BadRequest(messagesApi("panel.orders.order_does_not_exists"))
			}
		}

		p.future
	}
}
