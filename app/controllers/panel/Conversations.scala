package controllers.panel

import javax.inject.Inject

import actions.builders.Authenticated
import dao.{IConversationDAO, Message, User}
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Action, Controller, Result}
import settings.IGeneralSettings
import util.Pagination

import scala.concurrent.{ExecutionContext, Promise}

/**
	* Created by Daniel on 24/05/16.
	*/
object Conversations {
	case class AddMessageForm(message: String)
	val addMessageForm = Form(
		mapping(
			"message" -> nonEmptyText
		)(AddMessageForm.apply)(AddMessageForm.unapply)
	)
}

class Conversations @Inject()(
	protected val conversationsDAO: IConversationDAO,
	protected val settings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	import Conversations._

	/**
		* Displays the conversations management
		* @return
		*/
	def conversations = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		conversationsDAO.list().map { conversations =>
			conversationsDAO.count.map { total =>
				val pagination = new Pagination(total_rows = total)
				p success Ok(views.html.panel.conversations(conversations, pagination))
			}
		}

		p.future
	}

	/**
		* Displays the conversations with pager
		* @param page
		* @param order
		* @return
		*/
	def pager(page: Int, order: String) = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		val take = if (page-1 <= 0) 0 else (page-1) * 10
		conversationsDAO.list(Some(10), Some(take), order).map { conversations =>
			conversationsDAO.count.map { total =>
				val pagination = new Pagination(page, total-1)
				p success Ok(views.html.panel.conversations(conversations, pagination))
			}
		}

		p.future
	}

	/**
		* Display the conversation view
		* @param hash
		* @return
		*/
	def view(hash: String) = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		conversationsDAO.getComplexConversation(hash).map { conversation =>
			p success Ok(views.html.panel.conversations_view(addMessageForm, conversation))
		}

		p.future
	}

	/**
		* Process the add message form
		* @return
		*/
	def add_message(hash: String) = Authenticated.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings
		val user = request.user_session.get("user").asInstanceOf[Option[User]]

		addMessageForm.bindFromRequest.fold(
			errors => {
				conversationsDAO.getComplexConversation(hash).map { conversation =>
					p success Ok(views.html.panel.conversations_view(errors, conversation))
				}
			},
			data => {
				conversationsDAO.insertMessage(new Message(None, hash, data.message, user_id = user.get.id.get)).map { _ =>
					p success Redirect(controllers.panel.routes.Conversations.view(hash))
						.flashing("redirect-message" -> messagesApi("panel.conversations.message_added_success"),
							"redirect-type" -> "alert-success")
				}
			}
		)

		p.future
	}

}
