package controllers.panel

import java.io._
import java.nio.charset.StandardCharsets
import java.util.zip.{ZipEntry, ZipInputStream, ZipOutputStream}
import javax.inject._

import _root_.util.CSV.ImportError
import _root_.util.Formatters._
import _root_.util.{Files, Hashing, Pagination}
import actions.builders.AuthenticatedWithAdmin
import actors.TmpImportCleaner._
import akka.actor.{ActorRef, ActorSystem}
import dao._
import models.{ProductPropertyDefaultForm, ProductPropertyOptionForm, ViewProductForm}
import org.apache.commons.io.FileUtils
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.Logger
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, Messages, MessagesApi}
import play.api.libs.iteratee.Enumerator
import play.api.libs.json._
import play.api.mvc.{Action, Controller, Result}
import se.digiplant.res.api.Res
import settings.IGeneralSettings

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.io.Source

/**
	* Created by Daniel on 27/04/16.
	*/
object Products {
	case class AddProduct(name: String, description: String, category_id: Int, vat_id: Int, stock: Int, price: Double)
	implicit val productAddForm = Form(
		mapping(
			"name" -> nonEmptyText,
			"description" -> text,
			"category-id" -> number,
			"vat-id" -> number,
			"stock" -> number,
			"price" -> of(doubleFormat)
		)(AddProduct.apply)(AddProduct.unapply)
	)

	val productAddPropertyDefault = Form(
		mapping(
			"name" -> nonEmptyText,
			"with-images" -> boolean,
			"method" -> nonEmptyText,
			"options" -> seq(
				mapping(
					"id" -> nonEmptyText,
					"name" -> nonEmptyText,
					"order" -> number,
					"image-link" -> text,
					"lang-short" -> nonEmptyText
				)(ProductPropertyOptionForm.apply)(ProductPropertyOptionForm.unapply)
			)
		)(ProductPropertyDefaultForm.apply)(ProductPropertyDefaultForm.unapply)
	)

	implicit val productViewForm = Form(
		mapping(
			"name" -> nonEmptyText,
			"description" -> text,
			"category-id" -> number,
			"vat-id" -> number,
			"stock" -> number,
			"price" -> of(doubleFormat),
			"offer-discount" -> of(doubleFormat),
			"offer-discount-percentage" -> boolean,
			"activate-offer" -> boolean,
			"offer-date-until" -> jodaDate("dd/MM/yyyy"),
			"offer-use-date" -> boolean
		)(ViewProductForm.apply)(ViewProductForm.unapply)
	)

	object CSVImport {
		val logger = Logger("products_import")
		val totalValues = 12 // number of values
		case class CSVProduct(reference: String, name: String, description: String, category_id: Int, vat_id: Int, stock: Int, in_offer: Boolean, offer_use_percentage: Boolean, discount: Double, offer_date_until: Option[DateTime], offer_use_date: Boolean, price: Double)
		val CSVForm = Form(
			mapping(
				"reference" -> nonEmptyText,
				"name" -> nonEmptyText,
				"description" -> text,
				"category" -> number,
				"vat" -> number,
				"stock" -> number,
				"in-offer" -> boolean,
				"use-percentage" -> boolean,
				"discount" -> of(doubleFormat),
				"offer-date-until" -> optional(jodaDate("dd-MM-yyyy")),
				"offer-use-date" -> boolean,
				"price" -> of(doubleFormat)
			)(CSVProduct.apply)(CSVProduct.unapply)
		)

		/**
			* Checks the proper format of the lines
			* @param stream
			* @return
			*/
		def check(stream: Stream[String])(implicit messages: Messages): Seq[ImportError] = {
			val products_errors: ListBuffer[ImportError] = ListBuffer()
			for (line <- stream) {
				if (!line.trim.startsWith("#")) { // If not a comment
					val comma_values = line.trim.split(",")
					val semicolon_values = line.trim.split(";")
					val values = if (comma_values.length == totalValues) {
						comma_values.map( str => str.trim )
					} else if (semicolon_values.length == totalValues) {
						semicolon_values.map( str => str.trim )
					} else {
						Array[String]() // Empty
					}

					if (values.isEmpty) {
						products_errors += new ImportError(Some(stream.indexOf(line)), messages("panel.import_export.products.import.errors.incorrect_arguments_size", totalValues))
					} else {
						val jsValues = Json.obj(
							"reference" -> values(0),
							"name" -> values(1),
							"description" -> values(2),
							"category" -> values(3),
							"vat" -> values(4),
							"stock" -> values(5),
							"in-offer" -> values(6),
							"use-percentage" -> values(7),
							"discount" -> values(8),
							"offer-date-until" -> values(9),
							"offer-use-date" -> values(10),
							"price" -> values(11)
						)

						CSVForm.bind(jsValues).fold(
							err => {
								val errorsList: ListBuffer[String] = ListBuffer()
								for (error <- err.errors) {
									val argument_type = error.message.substring(6)
									errorsList += messages("panel.import_export.products.import.errors.argument_type_error", error.key, argument_type)
								}

								products_errors += new ImportError(Some(stream.indexOf(line)), errorsList.mkString(", "))
							},
							data => ()
						)
					}
				}
			}

			products_errors.toSeq
		}

		/**
			* Transforms the given line to a CSVProduct object
			* @param line
			* @return
			*/
		def line_format(line: String) = {
			val comma_values = line.trim.split(",")
			val semicolon_values = line.trim.split(";")
			val values = if (comma_values.length == totalValues) {
				comma_values.map( str => str.trim )
			} else if (semicolon_values.length == totalValues) {
				semicolon_values.map( str => str.trim )
			} else {
				Array[String]() // Empty
			}
			val dtf = DateTimeFormat.forPattern("dd-MM-yyyy")

			new CSVProduct(
				values(0),
				values(1),
				values(2),
				values(3).toInt,
				values(4).toInt,
				values(5).toInt,
				values(6).toBoolean,
				values(7).toBoolean,
				values(8).toDouble,
				if (values(9).nonEmpty) Some(DateTime.parse(values(9), dtf)) else Some(DateTime.now()),
				values(10).toBoolean,
				values(11).toDouble
			)
		}

		/**
			* Unzips the gallery
			* @param zipFile
			* @param outputDir
			*/
		def unzip(zipFile: String, outputDir: String): Unit = {
			val _buffer = new Array[Byte](1024)

			try {
				// Creating the output directory in case it doesn't exists
				val output_dir = new File(outputDir)
				if (!output_dir.exists()) output_dir.mkdirs()

				val galleries_file = new File(zipFile)
				val fis: FileInputStream = new FileInputStream(galleries_file)
				val zis: ZipInputStream = new ZipInputStream(new BufferedInputStream(fis))
				var entry: ZipEntry = zis.getNextEntry

				while ( entry != null ) {
					val filename = entry.getName
					val newFile = new File(outputDir + File.separator + filename)

					// Create new dirs
					if (entry.isDirectory) {
						newFile.mkdirs()
					} else {
						newFile.getParentFile.mkdirs()

						val fos: FileOutputStream = new FileOutputStream(newFile)

						Stream.continually(zis.read(_buffer))
							.takeWhile(_ != -1)
							.foreach(fos.write(_buffer, 0, _))

						fos.close()
					}

					entry = zis.getNextEntry
				}

				zis.closeEntry()
				zis.close()
			} catch {
				case e: Throwable => logger.error("Error unziping the galleries file: " + e)
			}
		}

	}
}

class Products @Inject()(
	protected val vatDAO: IVATDAO,
	protected val categoryDAO: ICategoryDAO,
	protected val productDAO: IProductDAO,
	protected val langItemDAO: ILangItemDAO,
	protected val productGalleryDAO: IProductGalleryDAO,
	protected val productPropertyDAO: IProductPropertyDAO,
	protected val supportedLangDAO: ISupportedLangDAO,
	protected val settings: IGeneralSettings,
	val messagesApi: MessagesApi,
	protected val system: ActorSystem,
	@Named("TmpImportCleaner") actorTmpImportCleaner: ActorRef
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	import Products._

	val CSVImportLogger = Logger("products_import")

	/**
		* Displays the view of the products.
		* @return
		*/
	def products = AuthenticatedWithAdmin.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		productDAO.list(order = "desc").map { products =>
			productDAO.count.map { total =>
				val pagination = new Pagination(total_rows = total-1)
				p success Ok(views.html.panel.products(products, pagination))
			}
		}

		p.future
	}

	/**
		* Displays the products with pager
		* @param page
		* @param order
		* @return
		*/
	def pager(page: Int, order: String) = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		val take = if (page-1 <= 0) 0 else (page-1) * 10
		productDAO.list(Some(10), Some(take), order).map { products =>
			productDAO.count.map { total =>
				val pagination = new Pagination(page, total)
				p success Ok(views.html.panel.products(products, pagination))
			}
		}

		p.future
	}

	/**
		* Displays the add form
		* @return
		*/
	def add_form = AuthenticatedWithAdmin.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		val things = for {
			categories <- categoryDAO.all
			vats <- vatDAO.all
		} yield (categories, vats)

		things.map { things =>
			val categories = things._1
			val vats = things._2

			p success Ok(views.html.panel.products_add(productAddForm, categories, vats))
		}

		p.future
	}

	/**
		* Creates a new product
		* @return
		*/
	def add = AuthenticatedWithAdmin.async { implicit request =>
		val p = Promise[Result]
		val f = p.future
		implicit val shopSettings = settings.getGeneralSettings

		productAddForm.bindFromRequest.fold(
			errors => {
				val things = for {
					categories <- categoryDAO.all
					vats <- vatDAO.all
				} yield (categories, vats)

				things.map { things =>
					val categories = things._1
					val vats = things._2

					p success Ok(views.html.panel.products_add(errors, categories, vats))
				}
			},
			data => {
				val q = for {
					langHash <- langItemDAO.generateHash
					_ <- langItemDAO.insert(new LangItem(None, langHash, "en", data.name))
					langName <- langItemDAO.get(langHash)
					langHash <- langItemDAO.generateHash
					_ <- langItemDAO.insert(new LangItem(None, langHash, "en", data.description))
					langDesc <- langItemDAO.get(langHash)
				} yield (langName, langDesc)

				q.map { tup =>
					val multipart = request.body.asMultipartFormData
					if (multipart.get.file("file").isEmpty) {
						p success Redirect(routes.Products.products()).
							flashing("redirect-message" -> "Please upload the default image!",
								"redirect-type" -> "alert-warning")
					} else {
						val hash = Hashing.randomAlphaNumericString(12)
						val newProduct = new Product(None, tup._1.lang_id, tup._2.lang_id, data.category_id, data.vat_id, hash, data.stock, price = data.price)
						val aq = for {
							_ <- productDAO.insert(newProduct)
							id <- productDAO.lastIndex
							_ <- productGalleryDAO.insert(new GalleryItem(
								None, id, Res.put(multipart.get.file("file").get, "products", Seq()), true
							))
						} yield Unit

						aq.map { _ =>
							p success Redirect(routes.Products.product_upload_gallery(hash))
								.flashing("redirect-message" -> "New product added successfully.",
									"redirect-type" -> "alert-success")
						}
					}
				}
			}
		)

		f
	}

	/**
		* Display the gallery upload for a product after the creation
		* @param hash
		* @return
		*/
	def product_upload_gallery(hash: String) = Action { implicit request =>
		Ok(views.html.panel.product_upload_gallery(hash))
	}

	/**
		* Upload gallery images to a product.
		* @param product_ref
		* @return
		*/
	def product_gallery_upload(product_ref: String) = Action.async(parse.multipartFormData) { implicit request =>
		val p = Promise[Result]
		val f = p.future

		request.body.file("file").map { picture =>
			try {
				val asset = Res.put(picture, "products", Seq())
				val comp = for {
					product <- productDAO.getComplex(product_ref)
					_ <- productGalleryDAO.insert(new GalleryItem(None, product.id.get, asset, false))
				} yield Unit

				comp.map(_ => p success Ok("File uploaded."))
			} catch {
				case e: IllegalArgumentException =>
					p success InternalServerError("There was a problem while trying to upload the file.")
			}
		}.getOrElse {
			Logger.error("Can't upload image to the gallery. \n" + request.body.files)
			p success InternalServerError("Seems like the file is missing.")
		}

		f
	}

	def gallery_delete_image(ref: String, fileuuid: String) = Action.async { implicit request =>
		productGalleryDAO.delete(fileuuid).map { _ =>
			Redirect(routes.Products.product_view(ref))
		}
	}

	/**
		* Displays the view to add product properties.
		* @param hash
		* @return
		*/
	def product_add_properties(hash: String) = Action.async { implicit request =>
		val p = Promise[Result]
		val f = p.future

		supportedLangDAO.all.map { supportedLangs =>
			p success Ok(views.html.panel.product_add_properties(hash, supportedLangs))
		}

		f
	}

	/**
		* Adds a property to a product.
		* @param hash
		* @return
		*/
	def product_add_property(hash: String) = Action.async { implicit request =>
		val p = Promise[Result]
		val f = p.future
		val encoded = request.body.asMultipartFormData

		// If is defined
		if (encoded.isDefined) {
			supportedLangDAO.all.map { supportedLangs =>
				val form = encoded.get
				val dataParts = form.dataParts

				// Getting method
				val method = dataParts.getOrElse("method", Seq("SELECT")).head
				// Getting with images boolean
				val withImages = dataParts.get("with-images").isDefined

				supportedLangDAO.all.map { supportedLangs =>
					val defaultLang = supportedLangs.find(_.is_default).get
					val defaultParts = dataParts.filterKeys(_.startsWith(defaultLang.short + "-")).map( m =>
						m._1.substring(3, m._1.length) -> m._2.head
					)
					val defaultOptions = defaultParts.filterKeys(_.startsWith("option-"))
					val optionsIds: ListBuffer[String] = ListBuffer()

					defaultOptions.foreach(
						option =>
							if (!optionsIds.contains(option._1.split("-").last))
								optionsIds.+=(option._1.split("-").last)
					)

					val defaultOptionsJs: ListBuffer[JsValue] = ListBuffer()
					for (option_id <- optionsIds) {
						val option_values = defaultOptions.filterKeys(_.endsWith(option_id)).map( m =>
							m._1.substring(0, m._1.length - option_id.length - 1) -> m._2
						)

						defaultOptionsJs.+=(
							JsObject(Seq(
								"id" -> JsString(option_id),
								"name" -> JsString(option_values.get("option-name").get),
								"order" -> JsNumber(option_values.get("option-order").get.toInt),
								"image-link" -> JsString(option_values.get("option-image-link").get),
								"lang-short" -> JsString(defaultLang.short)
							))
						)
					}

					productAddPropertyDefault.bind(
						Json.obj(
							"name" -> defaultParts.get("name").get,
							"with-images" -> withImages,
							"method" -> method,
							"options" -> defaultOptionsJs
						)
					).fold(
						formErrors => {
							p success Redirect(routes.Products.product_add_properties(hash))
								.flashing("redirect-message" -> "There are some errors. Be sure to fill all the fields.",
									"redirect-type" -> "alert-danger")
						},
						data => {
							val mainProcs = for {
								product <- productDAO.getComplex(hash)
								hash <- langItemDAO.generateHash
								_ <- langItemDAO.insert(new LangItem(None, hash, defaultLang.short, data.name))
								lang <- langItemDAO.get(hash)
							} yield (product, lang)

							mainProcs.map { t =>
								val product = t._1
								val langName = t._2

								// Process the default language...
								val productProperty = new ProductProperty(None, product.id.get, langName.lang_id, data.method, data.with_images)

								// Process other languages
								val namesMap: scala.collection.mutable.Map[String, String] = scala.collection.mutable.Map()
								val optionsList: ListBuffer[(String, String, String)] = ListBuffer()
								for (lang <- supportedLangs.filterNot(_.is_default)) {
									val parts = dataParts.filterKeys(_.startsWith(lang.short + "-")).map( m =>
										m._1.substring(3, m._1.length) -> m._2.head
									)
									val options = parts.filterKeys(_.startsWith("option-"))

									// Adding name
									namesMap.+=(lang.short -> parts.get("name").get)

									// Adding options
									options.foreach(
										opt =>
											optionsList.+=((opt._1.split("-").last, lang.short, opt._2))
									)
								}

								// Updating langName items
								namesMap.foreach(
									name =>
										langItemDAO.insert(new LangItem(None, langName.lang_id, name._1, name._2))
								)

								val propertyProcs = for {
									_ <- productPropertyDAO.insertProperty(productProperty)
									i <- productPropertyDAO.lastIndex
								} yield i

								propertyProcs.map { propertyId =>
									val serialized = {
										var accum = Future{()}

										for (option <- data.options) {
											accum = accum flatMap { _ =>
												val optionLangProc = for {
													hash <- langItemDAO.generateHash
													_ <- langItemDAO.insert(new LangItem(None, hash, option.lang_short, option.name))
													_ <- productPropertyDAO.insertValue(new PropertyValue(None, propertyId, hash, option.image_link, option.order.toByte))
												} yield hash

												optionLangProc.map { langId =>
													optionsList.filter(_._1 == option.id).foreach(
														translation =>
															langItemDAO.insert(new LangItem(None, langId, translation._2, translation._3))
													)
												}
											}
										}

										accum
									}

									serialized.map { _ =>
										p success Redirect(routes.Products.product_add_properties(hash))
											.flashing("redirect-message" -> "The property has been added.",
												"redirect-type" -> "alert-success")
									}
								}
							}
						}
					)

				}
			}
		} else {
			p success Redirect(routes.Products.product_add_properties(hash))
				.flashing("redirect-message" -> "There are some errors.",
					"redirect-type" -> "alert-danger")
		}

		f
	}

	/**
		* Deletes the product with the given id.
		* @param id
		* @return
		*/
	def delete(id: Int) = Action.async { implicit request =>
		val p = Promise[Result]
		val f = p.future

		productDAO.exists(id).map { yes =>
			if (yes) {
				val secs = for {
					product <- productDAO.getComplex(id)
					gallery <- productGalleryDAO.getGallery(id)
					_ <- productGalleryDAO.deleteGallery(id)
					_ <- langItemDAO.delete(product.name.lang_id)
					_ <- langItemDAO.delete(product.description.lang_id)
					_ <- productPropertyDAO.deleteAll(product.id.get)
					_ <- productDAO.delete(id)
				} yield Unit

				secs.map { _ =>
					p success Redirect(routes.Products.products()).
						flashing("redirect-message" -> "Product deleted successfully.",
							"redirect-type" -> "alert-success")
				}
			} else {
				p success Redirect(routes.Products.products()).
					flashing("redirect-message" -> "Oops! Seems like this product is not in the database.",
						"redirect-type" -> "alert-danger")
			}
		}

		f
	}

	/**
		* Product view
		* @return
		*/
	def product_view(product_ref: String) = Action.async { implicit request =>
		val p = Promise[Result]
		val f = p.future

		productDAO.exists(product_ref).map { yes =>
			if (yes) {
				val q = for {
					product <- productDAO.getComplex(product_ref)
					vats <- vatDAO.all
					cats <- categoryDAO.all
				} yield (product, vats, cats)

				q.map { t =>
					val product = t._1
					val data = new ViewProductForm(
						product.name.toString,
						product.description.toString,
						product.category.id.get,
						product.vat.id.get,
						product.stock,
						product.price,
						product.discount,
						product.offer_use_percentage,
						product.in_offer,
						product.offer_date_until,
						product.offer_use_date
					)

					p success Ok(views.html.panel.view_product(productViewForm.fill(data), t._1, t._3, t._2))
				}
			} else {
				p success Redirect(routes.Products.products()).
					flashing("redirect-message" -> "Oops! Seems like this product is not in the database.",
						"redirect-type" -> "alert-danger")
			}
		}

		f
	}

	def product_view_save(product_ref: String) = Action.async { implicit request =>
		val p = Promise[Result]
		val f = p.future

		productDAO.exists(product_ref).map { yes =>
			if (yes) {
				productViewForm.bindFromRequest.fold(
					error => {
						val q = for {
							product <- productDAO.getComplex(product_ref)
							vats <- vatDAO.all
							cats <- categoryDAO.all
						} yield (product, vats, cats)

						q.map { t =>
							val product = t._1
							val data = new ViewProductForm(
								product.name.toString,
								product.description.toString,
								product.category.id.get,
								product.vat.id.get,
								product.stock,
								product.price,
								product.discount,
								product.offer_use_percentage,
								product.in_offer,
								product.offer_date_until,
								product.offer_use_date
							)

							p success Ok(views.html.panel.view_product(error.fill(data), t._1, t._3, t._2))
						}
					},
					(data: ViewProductForm) => {
						productDAO.get(product_ref).map { product =>
							product.category_id = data.category_id
							product.vat_id = data.vat_id
							product.stock = data.stock
							product.in_offer = data.activate_offer
							product.discount = data.offer_discount
							product.offer_use_percentage = data.discount_percentage
							product.offer_date_until = data.offer_date_until
							product.offer_use_date = data.use_date
							product.price = data.price

							val procs = for {
								_ <- productDAO.update(product)
								gallery <- productGalleryDAO.getGallery(product.id.get)
							} yield gallery

							procs.map { gallery =>
								if (request.body.asMultipartFormData.isDefined) {
									val multipart = request.body.asMultipartFormData.get

									val serializedProcs = {
										var fAccum = Future{()}

										if (multipart.file("default-image").isDefined) {
											val file = multipart.file("default-image").get
											fAccum = fAccum flatMap{ _ =>
												if (gallery.exists(_.is_default)) {
													productGalleryDAO.delete(gallery.find(_.is_default).get.fileuuid)
												}

												productGalleryDAO.insert(new GalleryItem(None, product.id.get, Res.put(file, "products", Seq()), is_default = true))
											}
										}

										if (multipart.file("slider-image").isDefined) {
											val file = multipart.file("slider-image").get
											fAccum = fAccum flatMap{ _ =>
												if (gallery.exists(_.is_slider)) {
													productGalleryDAO.delete(gallery.find(_.is_slider).get.fileuuid)
												}

												productGalleryDAO.insert(new GalleryItem(None, product.id.get, Res.put(file, "products", Seq()), is_slider = true))
											}
										}

										fAccum
									}

									serializedProcs.map { _ =>
										p success Redirect(routes.Products.product_view(product_ref)).
											flashing("redirect-message" -> "Changes saved.",
												"redirect-type" -> "alert-success")
									}
								} else {
									p success Redirect(routes.Products.product_view(product_ref)).
										flashing("redirect-message" -> "Changes saved.",
											"redirect-type" -> "alert-success")
								}
							}
						}
					}
				)
			} else {
				p success Redirect(routes.Products.products()).
					flashing("redirect-message" -> "Oops! Seems like this product is not in the database.",
						"redirect-type" -> "alert-danger")
			}
		}

		f
	}

	/**
		* Display the import / export view
		* @return
		*/
	def import_export_form = AuthenticatedWithAdmin { implicit request =>
		implicit val shopSettings = settings.getGeneralSettings
		Ok(views.html.panel.products_import_export())
	}

	/**
		* Process submitted data before the import
		* @return
		*/
	def import_products = AuthenticatedWithAdmin.async(parse.multipartFormData) { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		val errors: scala.collection.mutable.Map[String, Seq[ImportError]] = scala.collection.mutable.Map()
		val hash = Hashing.randomAlphaNumericString(8)
		val tmp_location = System.getProperty("java.io.tmpdir") + s"/${hash}"
		val tmp_dir = new java.io.File(tmp_location)

		// Creating folders
		tmp_dir.mkdirs()

		// Schedule the cleaning process
		system.scheduler.scheduleOnce(2.hours, actorTmpImportCleaner, new CleanProducts(hash))

		// Processing the products file
		if (request.body.file("products").nonEmpty) {
			val products_file = new java.io.File(tmp_location + "/products.csv")
			request.body.file("products").get.ref.moveTo(products_file)

			// Checking charset
			if (Files.isCharset(products_file, "UTF-8").isDefined) {
				errors.+=( ("products" -> CSVImport.check(Source.fromFile(products_file).getLines().toStream)) )
			} else {
				errors.+=( ("products" -> Seq(ImportError(None, messagesApi("panel.import_export.products.import.errors.bad_file_encoding")))) )
			}

			if (request.body.file("galleries").nonEmpty) {
				val galleries_file = new java.io.File(tmp_location + "/galleries.zip")
				request.body.file("galleries").get.ref.moveTo(galleries_file)
				CSVImport.unzip(galleries_file.getPath, galleries_file.getParent + File.separator + "galleries")
			}

			if (request.body.dataParts.contains("erase-before")) {
				p success Ok(views.html.panel.products_import_check(errors.toMap, hash, true))
			} else {
				p success Ok(views.html.panel.products_import_check(errors.toMap, hash))
			}
		} else {
			p success Redirect(controllers.panel.routes.Products.import_export_form())
				.flashing("redirect-message" -> messagesApi("panel.import_export.products.import.errors.no_products_file_found"),
					"redirect-type" -> "alert-danger")
		}

		p.future
	}

	/**
		* Import submitted data or cancel the import
		* @param step
		* @param ref
		* @param erase_before
		* @return
		*/
	def import_products_step(step: Int, ref: String, erase_before: Boolean = false) = AuthenticatedWithAdmin.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		if (step == 2) {
			if (ref.nonEmpty) {
				val begin_time = System.currentTimeMillis()
				val import_count: ListBuffer[String] = ListBuffer()
				val tmp_location = System.getProperty("java.io.tmpdir") + s"/${ref}"
				val tmp_dir = new java.io.File(tmp_location)

				// Erase products if needed
				val erase = {
					var thing = Future{()}

					if (erase_before) {
						CSVImportLogger.info(s"[$ref] Erasing products before the import...")
						thing = thing flatMap(_ => productDAO.erase)
					}

					thing
				}

				erase.map { _ =>
					if (tmp_dir.exists()) {
						val tmp_files = tmp_dir.listFiles

						if (tmp_files.exists(_.getName.toLowerCase == "products.csv")) {
							CSVImportLogger.info(s"[$ref] Importing products...")
							val products_file = tmp_files.find(_.getName.toLowerCase == "products.csv").get
							val products_stream = Source.fromFile(products_file).getLines().toStream
							val products_errors = CSVImport.check(products_stream)

							val import_products = {
								var inception = Promise[Unit]()

								val futures: ListBuffer[Future[Unit]] = ListBuffer()
								val processed_lines: ListBuffer[String] = ListBuffer()
								if (products_stream.nonEmpty) {
									for (line <- products_stream) {
										if (!line.trim.startsWith("#")) {
											if (!products_errors.exists(err => err.line_number.getOrElse(-1) == products_stream.indexOf(line))) {
												futures += {
													val deception = Promise[Unit]()
													val csv_product = CSVImport.line_format(line)
													val checks = for {
														category_exists <- categoryDAO.exists(csv_product.category_id)
														vat_exists <- vatDAO.exists(csv_product.vat_id)
													} yield (category_exists, vat_exists)

													checks.map { checks_tuple =>
														val category_exists = checks_tuple._1
														val vat_exists = checks_tuple._2

														if (category_exists && vat_exists) {
															productDAO.exists(csv_product.reference).map { exists =>
																if (exists) {
																	productDAO.get(csv_product.reference).map { product =>
																		val langs = for {
																			name <- langItemDAO.get(product.lang_name_id)
																			description <- langItemDAO.get(product.lang_description_id)
																		} yield (name, description)

																		langs.map { langs_tuple =>
																			val name = langs_tuple._1
																			val description = langs_tuple._2

																			// Updating name and description
																			name.default.value = csv_product.name
																			description.default.value = csv_product.description

																			// Updating product
																			product.category_id = csv_product.category_id
																			product.vat_id = csv_product.vat_id
																			product.stock = csv_product.stock
																			product.in_offer = csv_product.in_offer
																			product.offer_use_percentage = csv_product.offer_use_percentage
																			product.discount = csv_product.discount
																			product.offer_date_until = csv_product.offer_date_until.getOrElse(DateTime.now())
																			product.offer_use_date = csv_product.offer_use_date
																			product.price = csv_product.price

																			val update = for {
																				_ <- langItemDAO.update(name.default)
																				_ <- langItemDAO.update(description.default)
																				_ <- productDAO.update(product)
																			} yield Unit

																			update.map { _ =>
																				import_count += line
																				CSVImportLogger.info(s"[$ref] Product ${product.reference} imported successfully.")
																				deception success Unit
																			}
																		}
																	}
																} else {
																	val langs = for {
																		nameHash <- langItemDAO.insertAndGetHash(new LangItem(None, "", "es", csv_product.name))
																		descHash <- langItemDAO.insertAndGetHash(new LangItem(None, "", "es", csv_product.description))
																	} yield (nameHash, descHash)

																	langs.map { langs =>
																		val new_product = new Product(None, langs._1, langs._2, csv_product.category_id, csv_product.vat_id, csv_product.reference, csv_product.stock, csv_product.in_offer, csv_product.offer_use_percentage, csv_product.discount, csv_product.offer_date_until.getOrElse(DateTime.now()), csv_product.offer_use_date, csv_product.price)

																		productDAO.insert(new_product).map { _ =>
																			import_count += line
																			CSVImportLogger.info(s"[$ref] Product ${csv_product.reference} imported successfully as new.")
																			deception success Unit
																		}
																	}
																}
															}
														} else {
															CSVImportLogger.info(s"[$ref] Skipping error in product ${csv_product.reference}. Category id or VAT id don't exist.")
															deception success Unit
														}
													}

													deception.future
												}
											} else {
												CSVImportLogger.info(s"[$ref] Skipping error in line ${products_stream.indexOf(line)}.")
											}
										} else {
											CSVImportLogger.info(s"[$ref] Skipping comment in line ${products_stream.indexOf(line)}.")
										}
									}
								} else {
									CSVImportLogger.info(s"[$ref] Products file is empty.")
								}

								inception.future.onComplete(
									_ => CSVImportLogger.info(s"[$ref] Products imported: ${import_count.length}")
								)

								// Waiting for the futures to complete...
								Future.sequence(futures).map( _ => inception success Unit )

								inception.future
							}

							val serialized_galleries = {
								var accum = Future{()}

								if (tmp_files.exists(_.getName.toLowerCase == "galleries.zip")) {
									CSVImportLogger.info(s"[$ref] Importing products galleries...")
									val galleries_file = tmp_files.find(f => f.getName.toLowerCase == "galleries.zip").get
									val galleries_dir = new File(galleries_file.getParent + File.separator + "galleries")

									if (galleries_dir.exists()) {
										val galleries_dirs = galleries_dir.listFiles().filter(_.isDirectory)

										for (dir <- galleries_dirs) {
											val name = dir.getName
											val images = dir.listFiles().filter(!_.isDirectory)

											images.foreach( image =>
												accum = accum flatMap { _ =>
													val inception = Promise[Unit]()

													productDAO.exists(name).map { exists =>
														if (exists) {
															productDAO.getComplex(name).map { product =>
																if (image.getName.startsWith("default.")) {
																	if (product.gallery.exists(_.is_default)) {
																		val old = product.gallery.find(_.is_default).get
																		old.is_default = false

																		productGalleryDAO.update(old)
																	}

																	inception success productGalleryDAO.insert(new GalleryItem(None, product.id.get, Res.put(image, "products"), is_default = true))
																} else if (image.getName.startsWith("slider.")) {
																	if (product.gallery.exists(_.is_slider)) {
																		val old = product.gallery.find(_.is_slider).get
																		old.is_slider = false

																		productGalleryDAO.update(old)
																	}

																	inception success productGalleryDAO.insert(new GalleryItem(None, product.id.get, Res.put(image, "products"), is_slider = true))
																} else {
																	inception success productGalleryDAO.insert(new GalleryItem(None, product.id.get, Res.put(image, "products")))
																}
															}
														} else {
															CSVImportLogger.warn(s"[$ref] galleries - Skipping nonexistent product reference. (${name})")
															inception success Unit
														}
													}

													inception.future
												}
											)
										}
									} else {
										CSVImportLogger.error(s"[$ref] galleries - galleries.zip exists but it is not uncompressed. Permissions problem maybe?")
									}
								}

								accum
							}

							val serialized_import = for {
								_ <- import_products
								_ <- serialized_galleries
							} yield Unit

							serialized_import.map { _ =>
								try {
									// Cleaning after the mess
									FileUtils.deleteDirectory(tmp_dir)
								} catch {
									case e: IOException =>
										CSVImportLogger.error(s"IOException while trying to delete ${tmp_dir.getAbsolutePath} directory.")

								}

								val time_in_seconds = (System.currentTimeMillis - begin_time) / 1000

								p success Redirect(controllers.panel.routes.Products.import_export_form())
									.flashing("redirect-message" -> messagesApi("panel.import_export.products.import.successful_import", time_in_seconds, import_count.length),
										"redirect-type" -> "alert-success")
							}
						} else {
							p success Redirect(controllers.panel.routes.Products.import_export_form())
								.flashing("redirect-message" -> messagesApi("panel.import_export.products.import.errors.temporal_files_do_not_exist"),
									"redirect-type" -> "alert-danger")
						}
					} else {
						p success Redirect(controllers.panel.routes.Products.import_export_form())
							.flashing("redirect-message" -> messagesApi("panel.import_export.products.import.errors.temporal_files_do_not_exist"),
								"redirect-type" -> "alert-danger")
					}
				}
			} else {
				p success Redirect(controllers.panel.routes.Products.import_export_form())
					.flashing("redirect-message" -> messagesApi("panel.import_export.products.import.errors.ref_does_not_exists"),
						"redirect-type" -> "alert-danger")
			}
		} else if (step == 3) {
			if (ref.nonEmpty) {
				val tmp_location = System.getProperty("java.io.tmpdir") + s"/${ref}"
				val tmp_dir = new java.io.File(tmp_location)

				if (tmp_dir.exists()) {
					try {
						FileUtils.deleteDirectory(tmp_dir)
					} catch {
						case e: IOException =>
							CSVImportLogger.error(s"IOException while trying to delete ${tmp_dir.getAbsolutePath} directory.")
					}
				}

				p success Redirect(controllers.panel.routes.Products.import_export_form())
					.flashing("redirect-message" -> messagesApi("panel.import_export.products.import.successful_cancellation"),
						"redirect-type" -> "alert-success")
			} else {
				p success Redirect(controllers.panel.routes.Products.import_export_form())
					.flashing("redirect-message" -> messagesApi("panel.import_export.products.import.errors.ref_does_not_exists"),
						"redirect-type" -> "alert-danger")
			}
		} else {
			p success Redirect(controllers.panel.routes.Products.import_export_form())
		}

		p.future
	}

	/**
		* Generates a file with all the products in CSV format.
		* @return
		*/
	def export_products = AuthenticatedWithAdmin.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		val dtf = DateTimeFormat.forPattern("dd-MM-yyyy")
		val lines_to_export: ListBuffer[String] = ListBuffer()
		val baos: ByteArrayOutputStream = new ByteArrayOutputStream()
		try {
			val zos: ZipOutputStream = new ZipOutputStream(baos)

			productDAO.all.map { products =>
				val serial = {
					var accum = Future{()}

					for (product <- products) {
						accum = accum flatMap { _ =>
							val inception = Promise[Unit]()

							val things = for {
								name <- langItemDAO.get(product.lang_name_id)
								description <- langItemDAO.get(product.lang_description_id)
								gallery <- productGalleryDAO.getGallery(product.id.get)
							} yield (name, description, gallery)

							things.map { things_tuple =>
								val name = things_tuple._1
								val description = things_tuple._2
								val gallery = things_tuple._3

								// Adding the line to the list
								lines_to_export += s"${product.reference},${name.toString},${description.toString},${product.category_id},${product.vat_id},${product.stock},${product.in_offer},${product.offer_use_percentage},${product.discount},${product.offer_date_until.toString(dtf)},${product.offer_use_date},${product.price}"

								// Adding galleries to the zip
								for (image <- gallery) {
									if (Res.get(image.fileuuid, "products").isDefined) {
										val _buffer = new Array[Byte](1024)
										val ext = Files.getExtension(image.fileuuid)
										val entry: ZipEntry = if (image.is_default || image.is_slider) {
											if (image.is_default) {
												new ZipEntry(s"galleries/${product.reference}/default.$ext")
											} else {
												new ZipEntry(s"galleries/${product.reference}/slider.$ext")
											}
										} else { new ZipEntry(s"galleries/${product.reference}/${image.fileuuid}") }

										// Adding the entry
										zos.putNextEntry(entry)

										// Filling the entry
										val bis: BufferedInputStream = new BufferedInputStream(new FileInputStream(Res.get(image.fileuuid, "products").get))

										Stream.continually(bis.read(_buffer))
											.takeWhile(_ != -1)
											.foreach(zos.write(_buffer, 0, _))

										// Closing streams
										bis.close()
										zos.closeEntry()
									}
								}

								inception success Unit
							}

							inception.future
						}
					}

					accum
				}

				// when all the lines are added to the list and all the galleries are added to the zip...
				serial.map { _ =>
					val _buffer = new Array[Byte](1024)
					val entry: ZipEntry = new ZipEntry("products.csv")
					val data: String = lines_to_export.toList.mkString("\r\n")

					// Adding the entry
					zos.putNextEntry(entry)

					// Filling the entry
					val bis: BufferedInputStream = new BufferedInputStream(new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8)))

					Stream.continually(bis.read(_buffer))
						.takeWhile(_ != -1)
						.foreach(zos.write(_buffer, 0, _))

					// Closing all the streams
					bis.close()
					zos.closeEntry()
					zos.close()


					// And we serve the chunked file
					p success Ok.chunked(
						Enumerator(baos.toByteArray).andThen(Enumerator.eof)
					).as("application/zip").withHeaders(CONTENT_DISPOSITION -> s"attachment; filename=products_export_${DateTime.now().toString(DateTimeFormat.forPattern("dd-MM-yyyy_HH-mm-ss"))}.zip")
				}
			}
		} catch {
			case e: IOException =>
				CSVImportLogger.error("There was an IOException error while trying to create te galleries ZIP while exporting.\n" + e.getMessage)
				p failure new Throwable("IOException while creating the galleries ZIP.")
			case e: Exception =>
				CSVImportLogger.error("There was an Exception error while trying to export.\n" + e.getMessage)
				p failure new Throwable("Exception while creating the galleries ZIP.")
		}

		p.future
	}

}
