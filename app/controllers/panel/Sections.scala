package controllers.panel

import javax.inject.Inject

import dao.{ISectionDAO, Section}
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import play.twirl.api.Html
import settings.IGeneralSettings
import util.Pagination

import scala.concurrent.{ExecutionContext, Future, Promise}

/**
	* Created by Daniel on 3/05/16.
	*/
object Sections {
	case class SectionForm(name: String, in_header: Boolean, html: String)
	implicit val addSectionForm = Form(
		mapping(
			"name" -> nonEmptyText,
			"in-header" -> boolean,
			"html" -> text
		)(SectionForm.apply)(SectionForm.unapply)
	)

	case class SectionSortForm(ids: Seq[Int], orders: Seq[Int])
	implicit val sortSectionForm = Form(
		mapping(
			"id" -> seq(number),
			"order" -> seq(number)
		)(SectionSortForm.apply)(SectionSortForm.unapply)
	)
}

class Sections @Inject()(
	protected val sectionsDAO: ISectionDAO,
	protected val settings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	import Sections._

	/**
		* Displays the sections management
		* @return
		*/
	def sections = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		sectionsDAO.list().map { sections =>
			sectionsDAO.count.map { total =>
				val pagination = new Pagination(total_rows = total-1)
				p success Ok(views.html.panel.sections(sections, pagination))
			}
		}

		p.future
	}

	/**
		* Displays the sections management with pager
		* @param page
		* @param order
		* @return
		*/
	def pager(page: Int, order: String) = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		val take = if (page-1 <= 0) 0 else (page-1) * 10
		sectionsDAO.list(Some(10), Some(take), order).map { sections =>
			sectionsDAO.count.map { total =>
				val pagination = new Pagination(page, total-1)
				p success Ok(views.html.panel.sections(sections, pagination))
			}
		}

		p.future
	}

	/**
		* Displays the add section form
		* @return
		*/
	def add_form = Action { implicit request =>
		implicit val shopSettings = settings.getGeneralSettings

		Ok(views.html.panel.sections_add(addSectionForm))
	}

	/**
		* Process the add section form
		* @return
		*/
	def add = Action.async(parse.multipartFormData) { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		addSectionForm.bindFromRequest.fold(
			errors => {
				p success Ok(views.html.panel.sections_add(errors))
			},
			data => {
				sectionsDAO.insert(new Section(None, data.name, Html(data.html), in_header = data.in_header)).map { _ =>
					p success Redirect(routes.Sections.add_form())
						.flashing("redirect-message" -> messagesApi("panel.sections.messages.add_success"),
							"redirect-type" -> "alert-success")
				}
			}
		)

		p.future
	}

	/**
		* Displays the sort form
		* @return
		*/
	def sort_form = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		sectionsDAO.all.map { sections =>
			p success Ok(views.html.panel.sections_sort(sortSectionForm, sections))
		}

		p.future
	}

	/**
		* Process the sort form
		* @return
		*/
	def sort = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		sortSectionForm.bindFromRequest.fold(
			errors => {
				sectionsDAO.all.map { sections =>
					p success Ok(views.html.panel.sections_sort(errors, sections))
				}
			},
			data => {
				val serialized = {
					var accum = Future {()}

					for (id <- data.ids) {
						accum = accum flatMap { _ =>
							sectionsDAO.updateOrder(id, data.orders(data.ids.indexOf(id)))
						}
					}

					accum
				}

				serialized.map { _ =>
					p success Redirect(routes.Sections.sort_form())
						.flashing("redirect-message" -> messagesApi("panel.sections.messages.sort_saved"),
							"redirect-type" -> "alert-success")
				}
			}
		)

		p.future
	}

	def edit_form(id: Int) = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		sectionsDAO.exists(id).map { exists =>
			if (exists) {
				sectionsDAO.get(id).map { section =>
					val sectionForm = new SectionForm(section.name, section.in_header, section.html.toString)
					p success Ok(views.html.panel.sections_edit(addSectionForm.fill(sectionForm), section.id.get))
				}
			} else {
				p success Redirect(routes.Sections.sections())
					.flashing("redirect-message" -> messagesApi("panel.messages.id_doesnt_exists"),
						"redirect-type" -> "alert-danger")
			}
		}

		p.future
	}

	def edit(id: Int) = Action.async { implicit request =>
		val p = Promise[Result]()
		implicit val shopSettings = settings.getGeneralSettings

		sectionsDAO.exists(id).map { exists =>
			if (exists) {
				sectionsDAO.get(id).map { section =>
					addSectionForm.bindFromRequest.fold(
						errors => {
							val sectionForm = new SectionForm(section.name, section.in_header, section.html.toString)
							p success Ok(views.html.panel.sections_edit(errors.fill(sectionForm), section.id.get))
						},
						data => {
							section.name = data.name
							section.html = Html(data.html)
							section.in_header = data.in_header

							sectionsDAO.update(section).map { _ =>
								val sectionForm = new SectionForm(section.name, section.in_header, section.html.toString)
								p success Redirect(routes.Sections.edit_form(section.id.get))
									.flashing("redirect-message" -> messagesApi("panel.sections.messages.edit_saved"),
										"redirect-type" -> "alert-success")
							}
						}
					)
				}
			} else {
				p success Redirect(routes.Sections.sections())
					.flashing("redirect-message" -> messagesApi("panel.messages.id_doesnt_exists"),
						"redirect-type" -> "alert-danger")
			}
		}

		p.future
	}

	/**
		* Process the delete request of a section
		* @return
		*/
	def delete(id: Int) = Action.async {
		val p = Promise[Result]()

		sectionsDAO.exists(id).map { exists =>
			if (exists) {
				sectionsDAO.delete(id).map { _ =>
					p success Redirect(routes.Sections.sections())
						.flashing("redirect-message" -> messagesApi("panel.sections.messages.delete_success"),
							"redirect-type" -> "alert-success")
				}
			} else {
				p success Redirect(routes.Sections.sections())
					.flashing("redirect-message" -> messagesApi("panel.sections.messages.delete_not_exists"),
						"redirect-type" -> "alert-danger")
			}
		}

		p.future
	}

}
