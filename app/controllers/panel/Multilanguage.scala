package controllers.panel

import javax.inject._

import dao.{ILanguageDAO, ISupportedLangDAO, SupportedLang}
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Action, Controller, Result}
import settings.IGeneralSettings

import scala.concurrent.{ExecutionContext, Promise}

/**
	* Created by Daniel on 27/04/16.
	*/
object Multilanguage {
	case class MultilanguageAddForm(id: Int)
	implicit val multilanguageAddForm = Form(
		mapping(
			"lang-id" -> number
		)(MultilanguageAddForm.apply)(MultilanguageAddForm.unapply)
	)
}

class Multilanguage @Inject()(
	protected val languageDAO: ILanguageDAO,
	protected val supportedLangDAO: ISupportedLangDAO,
	protected val settings: IGeneralSettings,
	val messagesApi: MessagesApi
)(implicit ec: ExecutionContext) extends Controller with I18nSupport {
	import Multilanguage._

	/**
		* Displays the view of the supported languages.
		* @return
		*/
	def multilanguage = Action.async { implicit request =>
		val p = Promise[Result]
		val f = p.future

		val q = for {
			allLangsList <- languageDAO.all
			shopSupportedList <- supportedLangDAO.all
		} yield (shopSupportedList, allLangsList)

		q.map { t =>
			val langs = t._2
			val supportedLangs = t._1

			if (supportedLangs.isEmpty) {
				val esLang = langs.find(_.short.equalsIgnoreCase("es"))
				val theLang = if (esLang.isEmpty) langs.head else esLang.get
				val esSupportedLang = new SupportedLang(None, theLang.id.get, theLang.short, theLang.name, true)

				val e = for {
					_ <- supportedLangDAO.insert(esSupportedLang)
					s <- supportedLangDAO.all
				} yield s

				e.map { supportedLangs =>
					p success Ok(views.html.panel.multilanguage(multilanguageAddForm, langs, supportedLangs))
				}
			} else {
				p success Ok(views.html.panel.multilanguage(multilanguageAddForm, langs, supportedLangs))
			}
		}

		f
	}

	/**
		* Add support for a new language.
		* @return
		*/
	def multilanguage_add_new = Action.async { implicit request =>
		val p = Promise[Result]
		val f = p.future

		multilanguageAddForm.bindFromRequest.fold(
			errors => {
				val q = for {
					langs <- languageDAO.all
				} yield langs

				q.map { langs =>
					p success Ok(views.html.panel.multilanguage(errors, langs))
				}
			},
			data => {
				languageDAO.exists(data.id).map { langExists =>
					if (langExists) {
						val q = for {
							lang <- languageDAO.get(data.id)
							exists <- supportedLangDAO.exists(lang.id.get)
						} yield (lang, exists)

						q.map { t =>
							val supportedLangExists = t._2
							val lang = t._1
							if (!supportedLangExists) {
								supportedLangDAO.insert( new SupportedLang(None, lang.id.get, lang.short, lang.name) ).map { _ =>
									p success Redirect(routes.Multilanguage.multilanguage()).
										flashing("redirect-message" -> "Language support added successfully.",
											"redirect-type" -> "alert-success")
								}
							} else {
								p success Redirect(routes.Multilanguage.multilanguage()).
									flashing("redirect-message" -> "This language is already supported.",
										"redirect-type" -> "alert-warning")
							}
						}
					} else {
						p success Redirect(routes.Multilanguage.multilanguage()).
							flashing("redirect-message" -> "Seems like the server is not responding. Try again later.",
								"redirect-type" -> "alert-danger")
					}
				}
			}
		)

		f
	}

	/**
		* Deletes the language with the given id.
		* @param id
		* @return
		*/
	def multilanguage_delete(id: Int) = Action.async { implicit request =>
		val p = Promise[Result]
		val f = p.future

		supportedLangDAO.exists(id).map { exists =>
			if (exists) {
				supportedLangDAO.get(id).map { item =>
					if (item.is_default) {
						p success Redirect(routes.Multilanguage.multilanguage()).
							flashing("redirect-message" -> "Can't delete the default language of this shop.",
								"redirect-type" -> "alert-warning")
					} else {
						supportedLangDAO.delete(item.id.get).map { _ =>
							p success Redirect(routes.Multilanguage.multilanguage()).
								flashing("redirect-message" -> s"${item.name} support deleted.",
									"redirect-type" -> "alert-success")
						}
					}
				}
			} else {
				p success Redirect(routes.Multilanguage.multilanguage()).
					flashing("redirect-message" -> "Oops! Seems like this language is not in the database.",
						"redirect-type" -> "alert-danger")
			}
		}

		f
	}

	/**
		* Makes a language the default language.
		* @param id
		* @return
		*/
	def multilanguage_default(id: Int) = Action.async { implicit request =>
		val p = Promise[Result]
		val f = p.future

		supportedLangDAO.exists(id).map { exists =>
			if (exists) {
				val q = for {
					default <- supportedLangDAO.getDefault
					actual <- supportedLangDAO.get(id)
				} yield (default, actual)

				q.map { tup =>
					val oldDefault = new SupportedLang(tup._1.id, tup._1.language_id, tup._1.short, tup._1.name, false)
					val newDefault = new SupportedLang(tup._2.id, tup._2.language_id, tup._2.short, tup._2.name, true)

					val q2 = for {
						a <- supportedLangDAO.update(oldDefault)
						b <- supportedLangDAO.update(newDefault)
					} yield (a, b)

					q2.map { _ =>
						p success Redirect(routes.Multilanguage.multilanguage()).
							flashing("redirect-message" -> "Default language changed succesfully.",
								"redirect-type" -> "alert-success")
					}
				}
			} else {
				p success Redirect(routes.Multilanguage.multilanguage()).
					flashing("redirect-message" -> "Oops! Seems like this language is not in the database.",
						"redirect-type" -> "alert-danger")
			}
		}

		f
	}

}
