package models

import org.joda.time.DateTime

/**
	* Created by Daniel on 21/12/15.
	*/
// Update product view model
case class ViewProductForm(
														name: String,
														description: String,
														category_id: Int,
														vat_id: Int,
														stock: Int,
														price: Double,
														offer_discount: Double,
														discount_percentage: Boolean,
														activate_offer: Boolean,
														offer_date_until: DateTime,
														use_date: Boolean)

// Product add property multilang
case class ProductPropertyOptionForm(id: String, name: String, order: Int, image_link: String, lang_short: String)
case class ProductPropertyDefaultForm(name: String, with_images: Boolean, method: String, options: Seq[ProductPropertyOptionForm])
case class ProductPropertyForm(name: String, options: Seq[ProductPropertyOptionForm])