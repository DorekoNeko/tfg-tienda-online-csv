name := """tfg-daniel"""

version := "2.1-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(SbtWeb, PlayScala)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  specs2 % Test,
  "com.typesafe.play" %% "play-slick" % "1.0.0",
  "mysql" % "mysql-connector-java" % "5.1.36",
	"org.webjars" % "bootstrap" % "3.3.5",
	"org.webjars" % "requirejs" % "2.2.0",
  "com.adrianhurt" %% "play-bootstrap3" % "0.4.4-P24",
	"commons-io" % "commons-io" % "2.4",
	"se.digiplant" %% "play-scalr" % "1.1.2",
	"se.digiplant" %% "play-res" % "1.1.1"
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

// Less config
includeFilter in (Assets, LessKeys.less) := "*.less"

// Compress css files
//LessKeys.compress in Assets := true

// RequireJS config
//pipelineStages := Seq(rjs)

// To simplify the reverse routing we can import the digiPlant namespace
TwirlKeys.templateImports += "se.digiplant._"